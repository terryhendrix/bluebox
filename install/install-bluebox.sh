#!/bin/bash

run() {
    ansible-playbook --inventory-file=$INVENTORY_FILE --user=$USER $PLAYBOOK
}


if [ -z $1 ]; then
    PLAYBOOK=full.yml
else
    PLAYBOOK="${1}.yml"
fi;

INVENTORY_FILE=$( ./gen-inventory-file.scala )
USER=bluebox

echo "Running ${PLAYBOOK} generated inventory file $INVENTORY_FILE"

run