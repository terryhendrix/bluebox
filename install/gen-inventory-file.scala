#!/bin/sh
exec scala -deprecation "$0" "$@"
!#
import java.nio.file.{Paths, Files}
import java.util.UUID
import scala.sys.process._

case class Host(ip:String)

def createInventoryFile(hosts:Seq[Host]) = {
  Files.createDirectories(Paths.get("target/inventory"))

  val hostsPart = hosts.zipWithIndex.foldLeft(""){ case (a,b) ⇒
    a +
      s"""
        |[box${b._2}]
        |${b._1.ip}
      """.stripMargin
  }

  val childrenPart = hosts.zipWithIndex.map(_._2).foldLeft("") { case (a,b) ⇒
    a + s"box${b}\n"
  }

  val target = "target/inventory/"+UUID.randomUUID+".inv"
  Files.write(Paths.get(target),
    s"""# file: Auto generated
      |$hostsPart
      |[bluebox:children]
      |$childrenPart
    """.stripMargin.getBytes)
  target
}

val hosts = Seq("nmap", "-sP", "10.0.0.0/24").!!.mkString.split("\n").toSeq
  .map(_.trim)
  .filter(!_.isEmpty)
  .filter(s => !s.toLowerCase.contains("latency"))
  .map(_.split("for").last.split("\\(").head.trim)
  .filter(s ⇒ !s.toLowerCase.contains("nmap"))
  .map(Host)


val exitCodesBlueBox = hosts.map { host ⇒
  System.err.print(s"Attempting connection to ${host.ip} as bluebox... ")
  val exit = Seq("ssh",
    "-o", "BatchMode=yes",
    "-o", "ConnectTimeout=3",
    "-o", "PasswordAuthentication=no",
    "-o", "StrictHostKeyChecking=no",
    "-o", "UserKnownHostsFile=/dev/null",
    "-q", s"bluebox@${host.ip}",
    "exit").!
  if(exit == 0)
    System.err.println("OK")
  else
    System.err.println("Fail")
  (host, exit);
}

val inventoryFile = createInventoryFile(exitCodesBlueBox.filter(_._2 == 0).map(_._1))

println(inventoryFile)


