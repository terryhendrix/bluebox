#!/bin/bash
# FIXME: The mounts are currently made by fstab, this will be done by the bluebox software soon enough. Then this line can be removed
umount -a -t nfs

dhclient -r eth0                                        && \
ifconfig eth0 down                                      && \
macchanger -pm {{ stored_mac_address.stdout }} eth0;       \
ifconfig eth0 up && echo 'Done setting mac'             &

sleep 10
echo 'Refreshing dhcp using the new mac'
dhclient -v eth0

# FIXME: The mounts are currently made by fstab, this will be done by the bluebox software soon enough. Then this line can be removed
mount -a -t nfs
