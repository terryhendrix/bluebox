package nl.bluesoft.bluebox.version.server

import akka.actor.ActorSystem


object Boot extends App
{
  val system = ActorSystem("BlueBoxUpgrade")
  val rest = UpgradeRest(system)
  rest.initializeRestApi(system)
}
