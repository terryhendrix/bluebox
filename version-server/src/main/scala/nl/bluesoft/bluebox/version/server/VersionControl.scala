package nl.bluesoft.bluebox.version.server

import java.nio.file.{Paths, Files, Path}
import java.text.SimpleDateFormat
import java.util.Date

import akka.actor.Actor
import nl.bluesoft.peanuts.json.{JsonMarshalling, PeanutsJsonProtocol}

import scala.collection.JavaConversions._
import scala.io.Source

object VersionControl extends PeanutsJsonProtocol
{
  case class Release(releaseDate:String)
  case class Version(version:String, released:Date)
  case class AddVersion(version:String)
  case class GetVersion(version:String)
  case object GetVersions

  implicit val releaseFormat = jsonFormat1(Release)
}

class VersionControl(path:Path) extends Actor with JsonMarshalling
{
  import VersionControl._

  def sdf = new SimpleDateFormat("dd-MM-yyyy")

  override def preStart() = {
    Files.createDirectories(path)
  }

  override def receive: Receive = {
    case GetVersions ⇒
      sender ! Files.newDirectoryStream(path).iterator().toSeq
        .filter(Files.isDirectory(_))
        .map(versionFromPath)

    case AddVersion(version) if !version.contains("..") ⇒
      val packagePath = Paths.get(path.toAbsolutePath + "/" + version)
      if(!Files.exists(packagePath)) {
        Files.createDirectories(packagePath)
        val release = Release(sdf.format(new Date()))
        Files.write(Paths.get(packagePath.toAbsolutePath + "/release.json"), release.asJson.getBytes)
        sender ! packagePath
      }

    case GetVersion(version) ⇒
      val packagePath = Paths.get(path.toAbsolutePath + "/" + version)
      if(Files.exists(packagePath)) {
        sender ! packagePath
      }
  }

  def versionFromPath(dir:Path) = {
    val release = Source.fromFile(dir.toAbsolutePath + "/release.json").mkString.asObject[Release]
    Version(
      version  = dir.getFileName.toString,
      released = sdf.parse(release.releaseDate)
    )
  }
}
