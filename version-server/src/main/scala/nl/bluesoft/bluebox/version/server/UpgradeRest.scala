package nl.bluesoft.bluebox.version.server

import java.io.File
import java.nio.channels.FileChannel
import java.nio.file.{Path, Paths, StandardOpenOption}
import java.nio.{ByteBuffer, MappedByteBuffer}
import java.util.Date

import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.HttpEntity.ChunkStreamPart
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server.Route
import akka.pattern._
import akka.stream.io.SynchronousFileSink
import akka.stream.scaladsl.Source
import akka.stream.{ActorMaterializer, Materializer}
import akka.util.{ByteString, Timeout}
import com.typesafe.config.Config
import UpgradeRest.PingResponse
import nl.bluesoft.bluebox.version.server.UpgradeRest.PingResponse
import nl.bluesoft.peanuts.http.RestApi
import nl.bluesoft.peanuts.json.{JsonMarshalling, PeanutsJsonProtocol}

import scala.concurrent.Await
import scala.concurrent.duration._

class ByteBufferIterator(buffer:ByteBuffer, chunkSize:Int) extends Iterator[ByteString] {
  require(buffer.isReadOnly)
  require(chunkSize > 0)

  override def hasNext = buffer.hasRemaining

  override def next(): ByteString = {
    val size = chunkSize min buffer.remaining()
    val temp = buffer.slice()
    temp.limit(size)
    buffer.position(buffer.position() + size)
    ByteString(temp)
  }
}

object UpgradeRest
{
  val UpgradeServerID = "BlueBox Version Server"
  val UpgradeServerVersion = "v1"

  case class PingResponse(id:String, version:String, timestamp:Date)

  def apply(system:ActorSystem) = new UpgradeRest()(system)
}

trait UpgradeJsonProtocol extends PeanutsJsonProtocol with JsonMarshalling with SprayJsonSupport
{
  implicit val pingResponseFormat = jsonFormat3(PingResponse)
  implicit val versionFormat = jsonFormat2(VersionControl.Version)
}

class UpgradeRest(implicit override val system: ActorSystem) extends RestApi with UpgradeJsonProtocol
{
  import UpgradeRest._
  implicit val ec = system.dispatcher
  override implicit def mat: Materializer = ActorMaterializer()
  implicit val timeout = Timeout(15 seconds)
  override def config: Config = system.settings.config.getConfig("bluebox.versions")
  val versionControl = system.actorOf(Props(new VersionControl(Paths.get(config.getString("storage-path")))), "Versions")

  def read(path: Path) : MappedByteBuffer = {
    val channel = FileChannel.open(path, StandardOpenOption.READ)
    val result = channel.map(FileChannel.MapMode.READ_ONLY, 0L, channel.size())
    channel.close()
    result
  }

  override def route: Route = pathPrefix("api" / UpgradeServerVersion) {
    path("ping") {
      complete {
        PingResponse(
          id = UpgradeServerID,
          version = UpgradeServerVersion,
          timestamp = new Date()
        ).asJson
      }
    } ~
    path("versions") {
      get {
        complete {
          (versionControl ? VersionControl.GetVersions).mapTo[Seq[VersionControl.Version]]
        }
      }
    } ~
    path("versions" / Segment) { version ⇒
      get {
        respondWithHeader(RawHeader(s"Content-Disposition", s"""inline; filename="bluebox-$version.zip" """)) {
          complete {
            val path = Await.result(
              versionControl ? VersionControl.GetVersion(version), timeout.duration) match {
              case p: Path ⇒
                system.log.info(s"Reading from $p")
                Paths.get(p.toAbsolutePath.toString + s"/bluebox.zip")
              case _ ⇒ throw new Exception(s"Could not get version '$version'")
            }

            val mappedByteBuffer = read(path)
            val iterator = new ByteBufferIterator(mappedByteBuffer, 4096)
            val chunks = Source(iterator.toStream).map { x =>
              system.log.info("Send chunk of size " + x.size)
              ChunkStreamPart(x)
            }

            HttpResponse(
              entity = HttpEntity.Chunked(MediaTypes.`application/octet-stream`, chunks)
            )
          }
        }
      } ~
      post {
        extractRequest { request =>
          val path = Await.result(
            versionControl ? VersionControl.AddVersion(version), timeout.duration) match {
            case p:Path ⇒ p
            case _ ⇒ throw new Exception(s"Could not create version '$version'")
          }

          system.log.info(s"Creating version $version at $path of size ${request.entity.contentLengthOption.getOrElse("")}")
          val outFile = new File(path.toAbsolutePath.toString + s"/bluebox.zip")
          val sink = SynchronousFileSink.create(outFile)

          request.entity.dataBytes.map { s ⇒
            system.log.info("Received chunk of size " + s.size)
            s
          }.to(sink).run()

          complete(StatusCodes.OK)
        }
      }
    }
  }

}
