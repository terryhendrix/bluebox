package nl.bluesoft.bluebox.version.server
import java.nio.file.{Files, Paths}
import akka.actor.ActorSystem
import scala.sys.process._
import org.scalatest.concurrent.{Futures, ScalaFutures}
import org.scalatest._

class TestSuite extends FeatureSpec with GivenWhenThen with Matchers with ScalaFutures with Futures with BeforeAndAfter with BeforeAndAfterEach
{
  val system = ActorSystem("System")
  val zipFile = "src/test/resources/test.zip"
  val storagePath = system.settings.config.getString("bluebox.versions.storage-path")
  
  require(Files.exists(Paths.get(zipFile)), "There must be a test.zip in the resources directory")

  override def beforeEach() = {
    Seq("rm", "-rf", storagePath+"/*").!!
  }

  def uploadVersion(version:String) = {
    Seq("curl", "-v", "-i", "-X", "POST", "-H", "Content-Type: multipart/form-data",
      "--data-binary", s"@$zipFile", s"http://localhost:25050/api/v1/versions/$version").!!
  }
  
  def downloadVersion(version:String) = {
    Seq("curl", s"http://localhost:25050/api/v1/versions/$version")
  }
}
