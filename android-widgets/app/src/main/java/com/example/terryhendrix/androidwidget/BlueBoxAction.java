package com.example.terryhendrix.androidwidget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import java.util.logging.Logger;

/**
 * Implementation of App Widget functionality.
 * App Widget Configuration implemented in {@link BlueBoxActionConfigureActivity BlueBoxActionConfigureActivity}
 */
public class BlueBoxAction extends AppWidgetProvider
{
    private static Logger log = Logger.getLogger("BlueBoxAction");

    public static void update(Context context, int appWidgetId, AppWidgetManager appWidgetManager) {
        CharSequence ip = BlueBoxActionConfigureActivity.loadPref(context, appWidgetId, "ip");
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.blue_box_action);

        Intent resumeIntent = new Intent(context, BlueBoxAction.class);
        resumeIntent.setAction("bluebox.widget.action"); // assign intent action
        resumeIntent.putExtra("action", "resume");
        resumeIntent.putExtra("ip", ip);
        PendingIntent pendingResumeIntent = PendingIntent.getBroadcast(context, 0, resumeIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        views.setOnClickPendingIntent(R.id.resume, pendingResumeIntent);

        Intent pauseIntent = new Intent(context, BlueBoxAction.class);
        pauseIntent.setAction("bluebox.widget.action"); // assign intent action
        pauseIntent.putExtra("action", "pause");
        pauseIntent.putExtra("ip", ip);
        PendingIntent pendingPauseIntent =PendingIntent.getBroadcast(context, 1, pauseIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        views.setOnClickPendingIntent(R.id.pause, pendingPauseIntent);


        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        log.info("onUpdate");
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            log.info("Updating ID "+appWidgetId);
            update(context, appWidgetId, appWidgetManager);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        log.info("Received Intent");
        if (intent.getAction().equals("bluebox.widget.action")) {
            log.info("Handling Intent");
            new CallBlueBoxTask().execute(intent.getStringExtra("ip"), intent.getStringExtra("action"));
        }
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        log.info("onDeleted");
        // When the user deletes the widget, delete the preference associated with it.
        for (int appWidgetId : appWidgetIds) {
            BlueBoxActionConfigureActivity.deletePref(context, appWidgetId, "action");
            BlueBoxActionConfigureActivity.deletePref(context, appWidgetId, "ip");
        }
    }

    @Override
    public void onEnabled(Context context) {
        log.info("onEnabled");
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        log.info("onDisabled");
        // Enter relevant functionality for when the last widget is disabled
    }
}

