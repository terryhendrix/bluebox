package com.example.terryhendrix.androidwidget;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.logging.Logger;

/**
 * The configuration screen for the {@link BlueBoxAction BlueBoxAction} AppWidget.
 */
public class BlueBoxActionConfigureActivity extends Activity
{
    private static Logger log = Logger.getLogger("BlueBoxActionConfigure");
    private static final String PREFS_NAME = "com.example.terryhendrix.androidwidget.BlueBoxAction";
    private static final String PREF_PREFIX_KEY = "bluebox_";
//    int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
    int mAppWidgetId = 1 + (int)(Math.random() * 10000000);     // random large number
    EditText mConfigureIpText;

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            final Context context = BlueBoxActionConfigureActivity.this;

            // When the button is clicked, store the string locally
            String ipAddress = mConfigureIpText.getText().toString();
            savePref(context, mAppWidgetId, "ip", ipAddress);

            // It is the responsibility of the configuration activity to update the app widget
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            BlueBoxAction.update(context, mAppWidgetId, appWidgetManager);

            // Make sure we pass back the original appWidgetId
            Intent resultValue = new Intent();
            resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
            setResult(RESULT_OK, resultValue);
            finish();
        }
    };

    public BlueBoxActionConfigureActivity() {
        super();
    }

    // Write the prefix to the SharedPreferences object for this widget
    static void savePref(Context context, int appWidgetId, String key, String text) {
        SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();
        prefs.putString(PREF_PREFIX_KEY + "_" + key + "_" + appWidgetId, text);
        prefs.apply();
    }

    // Read the prefix from the SharedPreferences object for this widget.
    // If there is no preference saved, get the default from a resource
    static String loadPref(Context context, int appWidgetId, String key) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        String value = prefs.getString(PREF_PREFIX_KEY + "_" + key + "_" + appWidgetId, null);
        if (value != null) {
            return value;
        } else {
            switch (key) {
                case "ip":
                    return context.getString(R.string.ip);
                case "action":
                    return context.getString(R.string.resume);
                default:
                    return "Could not load.";
            }
        }
    }

    static void deletePref(Context context, int appWidgetId, String key) {
        SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();
        prefs.remove(PREF_PREFIX_KEY + "_" + key + "_" + appWidgetId);
        prefs.apply();
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        log.info("Creating ID "+mAppWidgetId);
        // Set the result to CANCELED.  This will cause the widget host to cancel
        // out of the widget placement if the user presses the back button.
        setResult(RESULT_CANCELED);

        setContentView(R.layout.blue_box_action_configure);
        mConfigureIpText = (EditText) findViewById(R.id.configureIp);
        findViewById(R.id.add_button).setOnClickListener(mOnClickListener);

        // Find the widget id from the intent.
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mAppWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        // If this activity was started with an intent without an app widget ID, finish with an error.
        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
            return;
        }

        mConfigureIpText.setText(loadPref(BlueBoxActionConfigureActivity.this,  mAppWidgetId, "ip"));
    }
}

