package com.example.terryhendrix.androidwidget;

import android.os.AsyncTask;

import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by terryhendrix on 30/03/16.
 */
class CallBlueBoxTask extends AsyncTask<String, Void, String[]> {

    private static Logger log = Logger.getLogger("CallBlueBoxTask");
    private Exception exception;

    @Override
    protected String[] doInBackground(String... data) {
        try {
            JSONObject json = new JSONObject();
            json.put("action", data[1]);
            makeRequest("http://"+data[0]+":25080/api/v1/audio/actions", json);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return data;
    }

    @Override
    protected void onPostExecute(String[] requestedActions) {
        // TODO: check this.exception
        // TODO: do something with the feed
    }


    public static void makeRequest(String path, JSONObject holder) throws Exception
    {
        log.info("Calling path: "+path+" with "+holder);
        DefaultHttpClient httpclient = new DefaultHttpClient();
        HttpPut request = new HttpPut(path);

        //passes the results to a string builder/entity
        StringEntity se = new StringEntity(holder.toString());

        //sets the post request as the resulting string
        request.setEntity(se);
        //sets a request header so the page receving the request
        //will know what to do with it
        request.setHeader("Accept", "application/json");
        request.setHeader("Content-type", "application/json");

        //Handles what is returned from the page
        ResponseHandler responseHandler = new BasicResponseHandler();
        httpclient.execute(request, responseHandler);
    }

    private static JSONObject getJsonObjectFromMap(Map params) throws JSONException {

        //all the passed parameters from the post request
        //iterator used to loop through all the parameters
        //passed in the post request
        Iterator iter = params.entrySet().iterator();

        //Stores JSON
        JSONObject holder = new JSONObject();

        //using the earlier example your first entry would get email
        //and the inner while would get the value which would be 'foo@bar.com'
        //{ fan: { email : 'foo@bar.com' } }

        //While there is another entry
        while (iter.hasNext())
        {
            //gets an entry in the params
            Map.Entry pairs = (Map.Entry)iter.next();

            //creates a key for Map
            String key = (String)pairs.getKey();

            //Create a new map
            Map m = (Map)pairs.getValue();

            //object for storing Json
            JSONObject data = new JSONObject();

            //gets the value
            Iterator iter2 = m.entrySet().iterator();
            while (iter2.hasNext())
            {
                Map.Entry pairs2 = (Map.Entry)iter2.next();
                data.put((String)pairs2.getKey(), (String)pairs2.getValue());
            }

            //puts email and 'foo@bar.com'  together in map
            holder.put(key, data);
        }
        return holder;
    }
}