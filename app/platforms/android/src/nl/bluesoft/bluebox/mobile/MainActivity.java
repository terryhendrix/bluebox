package nl.bluesoft.bluebox.mobile;
import android.os.Bundle;
import android.view.KeyEvent;
import org.apache.cordova.*;

public class MainActivity extends CordovaActivity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        loadUrl(launchUrl);         // Set by <content src="index.html" /> in config.xml
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //If volume down key
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            this.loadUrl("javascript:cordova.fireDocumentEvent('volumedownbutton');");
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            this.loadUrl("javascript:cordova.fireDocumentEvent('volumeupbutton');");
            return true;
        } else {
            //return super.onKeyDown(keyCode, event);
        }
        //return super.onKeyDown(keyCode, event);
        return true;
    }
}
