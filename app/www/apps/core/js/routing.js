/**
 * Created by terryhendrix on 05/11/15.
 */

if(typeof box === "undefined") {
    console.error("routing.js must be after default.js");
    exit();
}

box.config(function($routeProvider) {
    $routeProvider
        .when('/welcome', {
            templateUrl: '/apps/core/welcome.html'
        })
        .when('/apps/audio', {
            templateUrl: '/apps/audio/audio.html',
            controller: 'AudioController'
        })
        .otherwise({
            redirectTo: '/apps/audio'
        });
});