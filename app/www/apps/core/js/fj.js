/**
 * Created by terryhendrix on 19/10/15.
 */

Array.prototype.last = function()
{
    return this[this.length-1]
};

Array.prototype.head = function()
{
    return this[0];
};

Array.prototype.reverse = function()
{
    var acc = [];
    var i, l = this.length;
    for (i = 0; i < l; i++) {
        acc.push(this[this.length - i - 1])
    }
    return acc;
};

Array.prototype.tail = function()
{
    var acc = [];
    var i, l = this.length;
    for (i = 1; i < l; i++) {
        acc.push(this[i])
    }
    return acc;
};

Array.prototype.fold = function(acc, f)
{
    var i, l = this.length;
    for (i = 0; i < l; i++) {
        acc = f(acc, this[i])
    }
    return acc;
};

Array.prototype.findFirst = function(condition)
{
    var i, l = this.length;
    for (i = 0; i < l; i++) {
        if(condition(this[i]))
            return this[i];
    }
};

Array.prototype.exists = function(condition)
{
    var i, l = this.length;
    for (i = 0; i < l; i++) {
        if(condition(this[i]))
            return true;
    }
    return false;
};

Array.prototype.replace = function(condition, entry)
{
    var i, l = this.length;
    for (i = 0; i < l; i++) {
        if(condition(this[i]))
            this[i] = entry;
    }
    return this;
};
