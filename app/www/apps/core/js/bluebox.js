var box = angular.module('bluebox', ['audio', 'asyncHttp', 'ngRoute']);

box.run(function($rootScope, $timeout, $location,blueboxService) {
    $rootScope.isMobile = function() {
        var a = navigator.userAgent.toLocaleLowerCase();
        return a.indexOf("android") !== -1
            || a.indexOf("iphone") !== -1
            || a.indexOf("ipad") !== -1;
    };

    $rootScope.isCordovaLoaded = function() {
        return blueboxService.isCordovaReady();
    };

    $rootScope.alertDeviceInfo = function() {
        if($rootScope.isCordovaLoaded()) {
            var deviceInfo = ('Device Platform: ' + device.platform + '\n'
            + 'Device Version: ' + device.version + '\n' + 'Device Model: '
            + device.model + '\n' + 'Device UUID: ' + device.uuid + '\n');
            navigator.notification.alert(deviceInfo);
        }
    };

    $rootScope.alertGeoLocation = function() {
        if($rootScope.isCordovaLoaded()) {
            var onSuccess = function(position) {
                alert('Latitude: ' + position.coords.latitude + '\n'
                    + 'Longitude: ' + position.coords.longitude + '\n'
                    + 'Altitude: ' + position.coords.altitude + '\n'
                    + 'Accuracy: ' + position.coords.accuracy + '\n'
                    + 'Altitude Accuracy: ' + position.coords.altitudeAccuracy
                    + '\n' + 'Heading: ' + position.coords.heading + '\n'
                    + 'Timestamp: ' + position.timestamp + '\n'); };
            navigator.geolocation.getCurrentPosition(onSuccess);
        }
    };

    $rootScope.beepNotify = function() {
        if($rootScope.isCordovaLoaded()) {
            navigator.notification.beep(1);
        }
    };

    $rootScope.vibrate = function(millis) {
        if($rootScope.isCordovaLoaded()) {
            navigator.vibrate(millis);
        }
    };

    $rootScope.fullscreen = false;

    $rootScope.openApp = function(app) {
        $location.path(app);
        $rootScope.enableFullscreen()
    };

    $rootScope.toggleFullScreen = function() {
        if (!(document.fullscreen || document.mozFullScreen || document.webkitIsFullScreen)) { // current working methods
            $rootScope.enableFullscreen();
        } else {
            $rootScope.disableFullscreen();
        }
    };

    $rootScope.countProperties = function(obj) {
        var count = 0;

        for(var prop in obj) {
            if(obj.hasOwnProperty(prop))
                ++count;
        }

        return count;
    };

    $rootScope.enableFullscreen = function() {
        console.info("Enter fullscreen");
        $rootScope.fullscreen = true;
        if (document.documentElement.requestFullScreen) {
            document.documentElement.requestFullScreen();
        } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
        } else if (document.documentElement.webkitRequestFullScreen) {
            document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        }
    };


    $rootScope.disableFullscreen = function() {
        console.info("Exit fullscreen");
        $rootScope.fullscreen = false;
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        }
    };

    $rootScope.changeFullScreen = function () {
        $timeout(function() {
            $rootScope.fullscreen = document.fullscreen || document.mozFullScreen || document.webkitIsFullScreen
            console.info("Change fullscreen "+ $rootScope.fullscreen);
        }, 5)
    };

    document.addEventListener("fullscreenchange",$rootScope.changeFullScreen , false);
    document.addEventListener("mozfullscreenchange", $rootScope.changeFullScreen, false);
    document.addEventListener("webkitfullscreenchange", $rootScope.changeFullScreen, false);
});

box.service('blueboxService', function (asyncHttp, $q) {
    var FORCE_NO_WEBSOCKETS = false;

    var server = window.location.hostname;
    var online = false;
    var websocketSupport = false;
    var cordovaReady = false;
    var portNumber = 25080; // default port number

    //if(typeof window !== "undefined" && typeof window.location !== "undefined" && typeof window.location.port !== "undefined") {
    //    portNumber = window.location.port;
    //    console.log("Using port from url: "+portNumber);
    //}

    document.addEventListener("deviceready", function() {
        console.log("The device is ready");
        cordovaReady = true;
        StatusBar.hide();
    }, false);

    if ("WebSocket" in window && !FORCE_NO_WEBSOCKETS) {
        websocketSupport = true;
        console.log("WebSockets supported!\r\n\r\nBrowser: " + navigator.userAgent);
    }
    else {
        websocketSupport = false;
        if(FORCE_NO_WEBSOCKETS)
            console.log("Forcing no websockets");
        console.error("WebSockets NOT supported!\r\n\r\nBrowser: " + navigator.userAgent);
    }

    if(typeof window.location.hostname !== "undefined" && window.location.hostname.trim() == "") {
        server = window.localStorage.getItem("server")
    }
    else {
        window.localStorage.setItem("server", server);
    }


    var self = {
        hasWebsocketSupport: function() {
            //try {
            //    var test = new WebSocket(self.getWebsocketUrl());
            //    test.onclose = function (event) {
            //        console.log("Websocket test succeeded")
            //    };
            //    test.close();
            //    websocketSupport = true;
            //}
            //catch(ex) {
            //    console.error(ex);
            //    websocketSupport = false;
            //}

            return websocketSupport;
        },
        isCordovaReady: function() {
            return cordovaReady;
        },
        isOnline: function() {
            return online;
        },
        setOnline: function() {
            online = true;
        },
        setOffline: function() {
            online = false;
        },
        validateServer: function(serverAndPort) {
            return asyncHttp.get("http://"+serverAndPort+"/api/v1/box/ping")
        },
        getServer: function() {
            return server;
        },
        getServerAndPort: function () {
            return server + ":" + portNumber;
        },
        getWebsocketUrl: function() {
            return "ws://" + self.getServerAndPort() + "/api/v1/audio/ws";
        },
        setServerAddress:function(_server) {
            return $q(function(success, failure) {
                self.validateServer(_server + ":" + portNumber).then(function(result){
                    if(result.id == "BlueBox") {
                        server = _server;
                        console.log("Setting address to "+_server);
                        window.localStorage.setItem("server", _server);
                        success(result);
                    }
                    else {
                        failure("Could not connect to "+_server);
                    }
                }, function(error) {
                    console.error("Could not connect to "+_server);
                    failure("Could not connect to "+_server);
                });
            });
        }
    };

    return self;
});

box.directive('configureServer', function() {
    return {
        controller: 'ConfigureServerController',
        templateUrl: 'apps/core/configure-server.html'
    };
});

box.controller("ConfigureServerController", function($scope, blueboxService) {
    console.log("Loading ConfigureServerController");
    if(blueboxService.getServer() == undefined) {
        blueboxService.setOffline();
        $("#chooseServerModal").modal("show");
    }
    else {
        blueboxService.validateServer(blueboxService.getServerAndPort()).then(function(result) {
            if(typeof result !== "undefined" && typeof result.id !== "undefined" && result.id == "BlueBox") {
                blueboxService.setOnline();
                console.log("Validated server " + blueboxService.getServerAndPort());
            }
            else {
                blueboxService.setOffline();
                $("#chooseServerModal").modal("show");
            }
        },function() {
            blueboxService.setOffline();
            $("#chooseServerModal").modal("show");
            console.error("Server "+blueboxService.getServerAndPort() + " is offline");
        })
    }

    $scope.$watch(blueboxService.getServer,function(address) {
        $scope.original = address;
        $scope.address = address;
    });

    $scope.configureServer = function(ipAddress) {
        blueboxService.setServerAddress(ipAddress).then(function(identifier) {
            blueboxService.setOnline();
            console.log(identifier.id + " "+identifier.version+" identified at "+identifier.timestamp);
            $("#chooseServerModal").modal("hide");
        }, function(errorMessage) {
            blueboxService.setOffline();
            $scope.error = errorMessage;
        });
    };
});

box.directive('ngRightClick', function($parse) {
    return function(scope, element, attrs) {
        var fn = $parse(attrs.ngRightClick);
        element.bind('contextmenu', function(event) {
            scope.$apply(function() {
                event.preventDefault();
                fn(scope, {$event:event});
            });
        });
    };
});

box.directive('onLongPress', function($timeout) {
    return {
        restrict: 'A',
        link: function ($scope, $elm, $attrs) {
            document.addEventListener("deviceready", function() {
                $scope.$apply(function () {
                    $scope.$eval($attrs.onLongPress)
                });
            }, false);
        }
    };
});
