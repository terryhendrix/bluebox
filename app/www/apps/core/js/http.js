/**
 * Created by terryhendrix on 07/07/15.
 */

var http = angular.module('asyncHttp', []);

http.service('asyncHttp', function($http,  $q) {
    var self = {
        get: function(url, params) {
            if(typeof params === "undefined")
                params = {};

            return $q(function(success, failure) {
                $http.get(url, {params:params}).success(function(result) {
                    console.log("GET "+url+" params: "+JSON.stringify(params));
                    console.log(result);
                    success(result);
                }).error(function(reason) {
                    console.error("GET "+url+" "+JSON.stringify(reason));
                    failure(reason)
                });
            });
        },
        put: function(url, body) {
            return $q(function(success, failure) {
                $http.put(url, body).success(function(result) {
                    console.log("PUT "+url+" "+JSON.stringify(body));
                    console.log(result);
                    success(result);
                }).error(function(reason) {
                    console.error("GET "+url+" "+JSON.stringify(reason));
                    failure(reason)
                });
            });
        },
        post: function(url, body) {
            return $q(function(success, failure) {
                $http.post(url, body).success(function(result) {
                    console.log("POST "+url+" "+JSON.stringify(body));
                    console.log(result);
                    success(result);
                }).error(function(reason) {
                    console.error("POST "+url+" "+JSON.stringify(reason));
                    failure(reason)
                });
            });
        },
        delete: function(url, params) {
            if(typeof params === "undefined")
                params = {};

            return $q(function(success, failure) {
                $http.delete(url, {params:params}).success(function(result) {
                    console.log("DELETE "+url);
                    console.log(result);
                    success(result);
                }).error(function(reason) {
                    console.error("DELETE "+url);
                    failure(reason)
                });
            });
        }
    };

    return self;
});

// TODO: Enable once we need to intercept failed calls
//http.factory('invalidRequestInterceptor', function($timeout)
//{
//    return {
//        'responseError': function(rejection) {
//            return rejection
//        },
//        'response': function(response) {
//            return response
//        }
//    };
//});
//
//http.config(function($httpProvider) {
//    $httpProvider.interceptors.push('invalidRequestInterceptor');
//});


