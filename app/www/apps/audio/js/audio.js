/**
 * Created by terryhendrix on 11/10/15.
 */
var audio = angular.module("audio", ['asyncHttp']);

http.service('audioService', function (asyncHttp, $q, blueboxService) {
    var self = {
        search: function (query) {
            if(typeof query !== "undefined" && query.trim() != "")
                return asyncHttp.get("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/search?q="+query);
            else {
                return $q(function(success, failure) {
                    console.error("Invalid query '"+query+"'");
                    success([]);
                });
            }
        },
        mute: function () {
            return asyncHttp.put("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/actions", {action: "mute"});
        },
        setMode: function (mode) {
            return asyncHttp.put("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/actions", {action: "mode", mode: mode});
        },
        unmute: function () {
            return asyncHttp.put("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/actions", {action: "unmute"});
        },
        play: function () {
            return asyncHttp.put("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/actions", {action: "play"});
        },
        skipTo: function (song) {
            return asyncHttp.put("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/actions", {action: "goto", song:song});
        },
        pause: function () {
            return asyncHttp.put("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/actions", {action: "pause"});
        },
        resume: function () {
            return asyncHttp.put("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/actions", {action: "resume"});
        },
        next: function () {
            return asyncHttp.put("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/actions", {action: "next"});
        },
        previous: function () {
            return asyncHttp.put("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/actions", {action: "previous"});
        },
        getVolume: function () {
            return asyncHttp.get("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/volume");
        },
        setVolume: function(volume) {
            return asyncHttp.put("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/volume", {volume: parseInt(volume)})
        },
        getSongProgress: function() {
            return asyncHttp.get("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/progress")
        },
        getCurrentlyPlaying: function() {
            return asyncHttp.get("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/songs/active")
        },
        loadSongs: function (rootDir) {
            if(typeof rootDir === "undefined")
                return asyncHttp.get("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/songs");
            else
                return asyncHttp.get("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/songs", {path: rootDir});
        },
        moveSong: function(from, to) {
            return asyncHttp.put("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/songs", {from: from, to:to, copy: false});
        },
        copySong: function(from, to) {
            return asyncHttp.put("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/songs", {from: from, to:to, copy: true});
        },
        deleteSong: function(dir, force) {
            return asyncHttp.delete("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/songs", {path: dir});
        },
        getDirectories: function (rootDir) {
            if(typeof rootDir === "undefined")
                return asyncHttp.get("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/directories");
            else
                return asyncHttp.get("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/directories", {path: rootDir});
        },
        createDirectory: function(dir, asPartOf) {
            if(typeof asPartOf !== "undefined")
                return asyncHttp.post("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/directories", {name: dir, asPartOf: asPartOf});
            else
                return asyncHttp.post("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/directories", {name: dir});
        },
        moveDirectory: function(from, to) {
            return asyncHttp.put("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/directories", {from: from, to:to, copy: false});
        },
        copyDirectory: function(from, to) {
            return asyncHttp.put("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/directories", {from: from, to:to, copy: true});
        },
        deleteDirectory: function(dir, force) {
            return asyncHttp.delete("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/directories", {path: dir, force: force});
        },
        getPlaylists: function () {
            return asyncHttp.get("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/playlists")
        },
        getPlaylist: function (name) {
            if(typeof name === "undefined")
                return asyncHttp.get("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/playlists/audio-manager-playlist");
            else
                return asyncHttp.get("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/playlists");
        },
        deletePlaylist: function (name) {
            if(typeof name === "undefined")
                return asyncHttp.delete("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/playlists/audio-manager-playlist");
            else
                return asyncHttp.delete("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/playlists");
        },
        addSongToPlaylist: function(hash, after, playlist) {
            if(typeof playlist === "undefined") {
                return asyncHttp.post("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/playlists/audio-manager-playlist/songs", {hash: hash, after:after});
            }
            else
                return asyncHttp.post("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/playlists/"+playlist+"/songs", {hash: hash, after:after});
        },
        removeSongFromPlaylist: function(hash, playlist) {
            if(typeof playlist === "undefined")
                return asyncHttp.delete("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/playlists/audio-manager-playlist/songs/"+hash);
            else
                return asyncHttp.post("http://" + blueboxService.getServerAndPort() + "/api/v1/audio/playlists/"+playlist+"/songs/"+ hash);
        }
    };

    return self;
});

audio.controller("AudioController", function ($scope, audioService, $interval, $timeout, blueboxService)
{
    $scope.query = "";
    $scope.pollingIntervalMillis = 3000;
    $scope.loading = false;
    $scope.playing = false;
    $scope.playModeSimple = "simple";
    $scope.playModeShuffle = "shuffle";
    $scope.playMode = $scope.playModeSimple;
    $scope.modePlayer = "player";
    $scope.modeCopy = "copy";
    $scope.modeSearch = "search";
    $scope.modeMove = "move";
    $scope.mode = $scope.modePlayer;
    $scope.errors = { example: "something went wrong" };

    $scope.buzz = function() {
        if($scope.isCordovaLoaded())
            $scope.vibrate(50);
    };

    $scope.handleUpdates = function(){
        if(blueboxService.hasWebsocketSupport()) {
            $scope.createWebsocket();
        }
        else {
            $scope.syncProgress();
            $scope.syncCurrentlyPlaying();
        }
    };

    $scope.createWebsocket = function() {
        try {
            $scope.socket = new WebSocket(blueboxService.getWebsocketUrl());
            $scope.socket.onmessage = function (event) {
                var data = JSON.parse(event.data);
                //console.log(event.data);
                if (typeof data.id === "undefined") {
                    console.error("Received a push-notification without id.")
                }
                else if (data.id === "PlaylistUpdated") {
                    $scope.playlist = data.message.playlist;
                }
                else if (data.id === "PlayModeSwitched") {
                    $scope.playMode = data.message.mode;
                }
                else if (data.id === "SongProgress") {
                    $scope.progress = data.message;
                }
                else if (data.id === "SongStarted") {
                    $scope.song = data.message.song;
                    $scope.playing = true;
                }
                else if (data.id === "SongStopped") {
                    $scope.song = data.message.song;
                    $scope.playing = false;
                }
                else if (data.id === "DirectoryUpdated") {
                    if (typeof $scope.currentDirectory === "undefined" || data.message.path == $scope.currentDirectory) {
                        $scope.loadSongs($scope.currentDirectory);
                        $scope.loadDirectories($scope.currentDirectory);
                    }
                }
                else if (data.id == "MusicDirectoryIndexed") {
                    if (typeof $scope.currentDirectory === "undefined" || data.message.directory == $scope.currentDirectory) {
                        $timeout(function () {
                            $scope.loading = false;
                            $scope.loadSongs($scope.currentDirectory);
                            $scope.loadDirectories($scope.currentDirectory);
                        }, 1500);
                    }
                }
                else if (data.id === "VolumeSet") {
                    $scope.volume = data.message.volume;
                    $scope.muted = data.message.muted;
                    console.log("Volume set to "+data.message.volume.volume);
                }
                $scope.$apply();
            };

            $scope.socket.onopen = function (event) {
                blueboxService.setOnline();
                $scope.resetErrors();
                console.log("Websocket connected to ws://" + blueboxService.getServerAndPort());
                $("#chooseServerModal").modal("hide");
            };

            $scope.socket.onclose = function (event) {
                blueboxService.setOffline();
                $scope.addError('box', "Connection with BlueBox lost");
                console.error("No connection with server, retry to connect in 5 seconds.");
                $timeout($scope.createWebsocket, 5000);

                try {
                    $scope.socket.close()
                } finally {
                    console.log("Websocket closed");
                }
            };

            window.onbeforeunload = function () {
                console.log("Closing socket");
                $scope.socket.onclose = function () {
                }; // disable onclose handler first
                $scope.socket.close()
            };
        }
        catch (ex) {
            alert(ex);
        }
    };

    $scope.updateCurrentStatusWithoutWebSockets = function() {
        if(!blueboxService.hasWebsocketSupport()) {
            $scope.stopProgressInterval();
            $scope.stopProgressPolling();
            $scope.stopCurrentlyPlayingPolling();

            $scope.currentlyPlayingPoller = $timeout($scope.syncCurrentlyPlaying, 300);
            $scope.progressPoller = $timeout($scope.syncProgress, 300);
        }
    };

    $scope.syncCurrentlyPlaying = function() {
        function setCurrentlyPlaying(active) {
            $scope.song = active.song;
            $scope.playing = active.isPlaying;
        }
        $scope.stopCurrentlyPlayingPolling();
        audioService.getCurrentlyPlaying().then(function(active) {
            setCurrentlyPlaying(active);
            $scope.loadPlaylist();
            $scope.currentlyPlayingPoller = $timeout($scope.syncCurrentlyPlaying, $scope.pollingIntervalMillis);
        })
    };


    $scope.stopCurrentlyPlayingPolling = function() {
        if($scope.currentlyPlayingPoller)
            $timeout.cancel($scope.currentlyPlayingPoller);
    };

    $scope.syncProgress = function() {
        $scope.stopProgressPolling();

        $scope.stopProgressInterval();

        audioService.getSongProgress().then(function(progress) {
            $scope.progress = progress;
            $scope.progressPoller = $timeout($scope.syncProgress, $scope.pollingIntervalMillis);
            $scope.progressInterval = $interval(function () {
                if ($scope.playing && $scope.progress.progress + 1 < $scope.progress.total)
                    $scope.progress.progress = $scope.progress.progress + 1.0;
            }, 1000, $scope.pollingIntervalMillis / 1000);
        })
    };

    $scope.stopProgressInterval = function() {
        if($scope.progressInterval)
            $interval.cancel($scope.progressInterval);
    };

    $scope.stopProgressPolling = function() {
        if($scope.progressPoller)
            $timeout.cancel($scope.progressPoller);
    };

    $scope.$watch(blueboxService.isOnline, function(online) {
        if(online) {
            $scope.init();
        }
        else {
            delete $scope.directories;
            delete $scope.songs;
            delete $scope.playlist;
            delete $scope.progress;
        }
    });

    $scope.loadSongs = function(path) {
        audioService.loadSongs(path).then(function (songs) {
            $scope.songs = songs;
            $scope.fixPageComposition();
        });
    };

    $scope.deleteSong = function(song) {
        $("#songOptionsModal").modal("hide");
        $scope.loading = true;
        audioService.deleteSong(song.path + "/" + song.fileName).then(function(){
            /// $scope.loading is disabled once the updates come through the websocket
        }, function() {
            $scope.loading = false; // handle the failure scenario
        });
    };

    $scope.startSearch = function() {
        console.info($scope.mode);
        if($scope.query)
            $scope.searchSongs($scope.query);
        $scope.buzz();
        $scope.mode = $scope.modeSearch;
        console.info($scope.mode);
    };

    $scope.cancelSearch = function() {
        $scope.buzz();
        $scope.mode = $scope.modePlayer;
        $scope.openDirectory($scope.currentDirectory)
    };

    $scope.searchSongs = function(query) {
        if(query)
            $scope.query = query;

        audioService.search($scope.query).then(function(result){
            $scope.searchResults = result;
            $scope.fixPageComposition();
        });
    };

    /**
     * Loads the directory given. If the mode is copy or move, then it sets the currentDirectory straight away.
     * @param path
     */
    $scope.loadDirectories = function(path) {
        $scope.currentDirectory = path;

        if($scope.isMobile()) {
            document.addEventListener("backbutton", function() {
                console.log("Back button pressed");
                if(blueboxService.isOnline() && typeof $scope.getParentDirectory() !== "undefined")
                    $scope.openParentDirectory();
                else {
                    $scope.buzz();
                    navigator.app.exitApp();
                }
            }, true);
        }

        audioService.getDirectories(path).then(function(directories){
            $scope.directories = directories;
            $scope.fixPageComposition();
            if(typeof $scope.currentDirectory == "undefined" &&
                typeof $scope.directories != "undefined" && typeof $scope.directories[0] != "undefined") {
                $scope.currentDirectory  = $scope.directories[0].path
                            .split("/")
                            .reverse()
                            .tail()
                            .reverse()
                            .filter(function(entry){ return entry.trim() !== ""})
                            .fold("", function(acc, entry) { return acc + "/" + entry});
            }
        });
    };

    $scope.openDirectory = function(path, mode) {
        $scope.buzz();
        if($scope.mode == $scope.modeCopy || $scope.mode == $scope.modeCopy)
            $scope.currentDirectory = path;

        delete $scope.songs;
        delete $scope.directories;
        delete $scope.searchResults;

        if(typeof mode !== "undefined")
            $scope.mode = mode;

        $scope.loadSongs(path);
        $scope.loadDirectories(path);
    };

    $scope.openParentDirectory = function() {
        $scope.buzz();
        $scope.openDirectory($scope.getParentDirectory().path)
    };

    $scope.getParentDirectory = function() {
        return $scope.directories.findFirst(function(dir){ return dir.name == ".."})
    };

    $scope.getCurrentDirectoryName = function() {
        if(typeof $scope.currentDirectory !== "undefined")
            return $scope.currentDirectory.split("/").reverse().head();
    };

    $scope.hasParentDirectory = function() {
        if(typeof $scope.directories !== "undefined" && typeof $scope.directories.exists !== "undefined" &&
            $scope.directories.exists(function(dir){ return dir.name == ".."})) {
            return true;
        }
        else return false;
    };
    $scope.createDirectory = function (newDirectory) {
        $scope.buzz();
        audioService.createDirectory(newDirectory, $scope.currentDirectory).then(function() {
            $("#createDirectoryModal").modal("hide");
        }, function(error) {
            $("#createDirectoryModal").modal("hide");
            $scope.addError('box', error.message);
            $timeout($scope.resetErrors, 4000)
        });
    };

    $scope.deleteDirectory = function(directory, force) {
        $scope.buzz();
        $scope.resetErrors();
        audioService.deleteDirectory(directory, force).then(function() {
            $("#directoryOptionsModal").modal("hide");
        }, function(data) {
            if(data.type === "java.nio.file.DirectoryNotEmptyException") {
                $scope.addError("delete", "The directory is not empty, are you sure?")
            }
        });
    };

    $scope.startRename = function(songOrDir) {
        $scope.buzz();
        $scope.songOrDir = songOrDir;

        if(songOrDir == 'song') {
            $("#songOptionsModal").modal("hide");
            $("#renameFileModal").modal("show");
            $scope.renameTo = $scope.optionSong.fileName;
        }
        else if($scope.songOrDir === 'dir') {
            $("#directoryOptionsModal").modal("hide");
            $("#renameDirectoryModal").modal("show");
            $scope.songOrDir = songOrDir;
            $scope.renameTo = $scope.optionDirectory.name;
        }
        else {
            console.warn("Unhandled "+$scope.songOrDir);
        }
    };

    $scope.cancelRename = function() {
        $scope.buzz();
        $("#renameDirectoryModal").modal("hide");
        $("#renameFileModal").modal("hide");
    };

    $scope.confirmRename = function(newName) {
        $scope.buzz();
        $("#renameFileModal").modal("hide");
        $("#renameDirectoryModal").modal("hide");
        if($scope.songOrDir === 'dir') {
            $scope.loading = true;
            var to = $scope.optionDirectory.path.replace($scope.optionDirectory.path.split("/").last(), newName);
            audioService.moveDirectory($scope.optionDirectory.path, to).then(function() {
                $scope.loading = false;
            }, function(error) {
                $scope.loading = false;
                $scope.addError('box', error.message);
                $timeout($scope.resetErrors, 4000)
            });
        }
        else if($scope.songOrDir == 'song') {
            $scope.loading = true;
            var from = $scope.optionSong.path + "/" + $scope.optionSong.fileName;
            var to = $scope.optionSong.path + "/" + newName;
            audioService.moveSong(from, to).then(function() {
                $scope.loading = false;
            }, function(error) {
                $scope.loading = false;
                $scope.addError('box', error.message);
                $timeout($scope.resetErrors, 4000)
            });
        }
        else {
            console.warning("Unhandled "+$scope.songOrDir)
        }
    };

    $scope.cancelRename = function() {
        $scope.buzz();
        $("#renameFileModal").modal("hide");
    };

    $scope.startCopy = function(songOrDir) {
        $scope.buzz();
        $("#directoryOptionsModal").modal("hide");
        $("#songOptionsModal").modal("hide");
        $scope.mode = $scope.modeCopy;
        $scope.songOrDir = songOrDir;
    };

    $scope.confirmCopy = function() {
        $scope.buzz();
        if($scope.songOrDir === 'dir') {
            $scope.mode = $scope.modePlayer;
            console.log("Copying from "+$scope.optionDirectory.path+" to "+$scope.currentDirectory);
            $scope.loading = true;
            audioService.copyDirectory($scope.optionDirectory.path, $scope.currentDirectory).then(function(){
                $scope.loading = false;
            }, function(error) {
                $scope.loading = false;
                $scope.addError('box', error.message);
                $timeout($scope.resetErrors, 4000)
            });
        }
        else if($scope.songOrDir == 'song') {
            $scope.mode = $scope.modePlayer;
            console.log("Moving from "+$scope.optionSong.path + "/" + $scope.optionSong.fileName +" to "+$scope.currentDirectory);
            $scope.loading = true;
            audioService.copySong($scope.optionSong.path + "/" + $scope.optionSong.fileName, $scope.currentDirectory).then(function(){
                /// $scope.loading is also disabled once the updates come through the websocket
            }, function(error) {
                $scope.loading = false;
                $scope.addError('box', error.message);
                $timeout($scope.resetErrors, 4000)
            });
        }
        else {
            console.warning("Unhandled "+$scope.songOrDir)
        }
    };

    $scope.cancelCopy = function() {
        $scope.buzz();
        $scope.mode = $scope.modePlayer;
    };

    $scope.startMove = function(songOrDir) {
        $scope.buzz();
        $("#directoryOptionsModal").modal("hide");
        $("#songOptionsModal").modal("hide");
        $scope.mode = $scope.modeMove;
        $scope.songOrDir = songOrDir;
    };

    $scope.confirmMove = function() {
        $scope.buzz();
        if($scope.songOrDir == 'dir') {
            $scope.mode = $scope.modePlayer;
            console.log("Moving from "+$scope.optionDirectory.path+" to "+$scope.currentDirectory);
            $scope.loading = true;
            audioService.moveDirectory($scope.optionDirectory.path, $scope.currentDirectory).then(function(){
                $scope.loading = false;
            });
        }
        else if($scope.songOrDir == 'song') {
            $scope.mode = $scope.modePlayer;
            console.log("Moving from "+$scope.optionSong.path + "/" + $scope.optionSong.fileName + " to "+$scope.currentDirectory);
            $scope.loading = true;
            audioService.moveSong($scope.optionSong.path+ "/" + $scope.optionSong.fileName, $scope.currentDirectory).then(function(){
                /// $scope.loading is disabled once the updates come through the websocket
            }, function() {
                $scope.loading = false; // handle the failure scenario
            });
        }
        else {
            console.warning("Unhandled "+$scope.songOrDir)
        }
    };

    $scope.cancelMove = function() {
        $scope.buzz();
        $scope.mode = $scope.modePlayer;
    };

    $scope.loadPlaylist = function(playlist) {
        audioService.getPlaylist(playlist).then(function(playlist) {
            $scope.playlist = playlist;
            $scope.fixPageComposition();
        });
    };


    $scope.confirmClearPlaylist = function() {
        $scope.buzz();
        $("#clearCurrentPlaylistModal").modal("show");
    };

    $scope.clearPlaylist = function() {
        audioService.deletePlaylist().then(function(){
            $("#clearCurrentPlaylistModal").modal("hide");
            $scope.slideClose();
        });
    };

    $scope.nextSongs = function(amount) {
        var next = [];
        var record = false;

        if(typeof $scope.playlist !== "undefined" && typeof $scope.song !== "undefined"
            && $scope.playlist.songs.exists(function(song) {
                return song.songHash == $scope.song.songHash;
            })) {
            for(var c = 0; c < $scope.playlist.songs.length * 2; c++)
            {
                if(record && next.length < amount
                    && next.indexOf($scope.playlist.songs[c % $scope.playlist.songs.length]) === -1)
                {
                    next.push($scope.playlist.songs[c % $scope.playlist.songs.length]);
                }
                else if(record) {
                    return next;
                }
                else if($scope.playlist.songs[c % $scope.playlist.songs.length].songHash === $scope.song.songHash)
                {
                    next.push($scope.playlist.songs[c % $scope.playlist.songs.length]);
                    record = true;
                }
            }
        }
        else if(typeof $scope.playlist !== "undefined" && typeof $scope.playlist.songs !== "undefined") {
            if(typeof $scope.song !== "undefined") {
                next.push($scope.song);
                amount = amount - 1;
            }

            for(var i = 0; i < amount; i++) {
                if(typeof $scope.playlist.songs[i] !== "undefined" &&
                    !next.exists(function(song) { return song.songHash == $scope.playlist.songs[i].songHash })) {
                    next.push($scope.playlist.songs[i % $scope.playlist.songs.length]);
                }
            }
            return next;
        }
        else {
            return next;
        }
    };

    $scope.init = function () {
        console.log("Loading AudioController");
        $scope.loadSongs();
        $scope.loadDirectories();
        $scope.loadPlaylist();
        $scope.handleUpdates();
    };

    $scope.toggleSong = function(song) {
        if($scope.isInPlaylist(song)) {
            $scope.removeSong(song);
        }
        else {
            $scope.addLast(song);
        }
    };

    $scope.setShuffleMode = function() {
        $scope.buzz();
        audioService.setMode("shuffle");
    };

    $scope.setSimpleMode = function() {
        $scope.buzz();
        audioService.setMode("simple");
    };

    $scope.addNext = function(song) {
        $scope.buzz();
        console.log("Adding next "+song.fileName);
        audioService.addSongToPlaylist(song.songHash, $scope.song.songHash).then(function() {
            if(!blueboxService.hasWebsocketSupport())
                $scope.loadPlaylist();
        });
        $("#songOptionsModal").modal("hide");
    };

    $scope.addLast = function(song) {
        $scope.buzz();
        console.log("Adding "+song.fileName + " to end of the playlist");
        if(typeof $scope.playlist.songs.last() !== "undefined")
            audioService.addSongToPlaylist(song.songHash, $scope.playlist.songs.last().songHash).then(function() {
                if(!blueboxService.hasWebsocketSupport())
                    $scope.loadPlaylist();
            });
        else
            audioService.addSongToPlaylist(song.songHash).then(function() {
                if(!blueboxService.hasWebsocketSupport())
                    $scope.loadPlaylist();
            });
    };

    $scope.removeSong = function(song) {
        $scope.buzz();
        audioService.removeSongFromPlaylist(song.songHash).then(function() {
            if(!blueboxService.hasWebsocketSupport())
                $scope.loadPlaylist();
        });
    };

    $scope.isInPlaylist = function(song) {
        if(typeof $scope.playlist !== "undefined") {
            return $scope.playlist.songs.filter(function(inPlaylist) {
                return inPlaylist.songHash == song.songHash;
            }).length > 0;
        }
        else
            return false;
    };

    $scope.resume = function() {
        $scope.buzz();
        audioService.resume();
        $scope.updateCurrentStatusWithoutWebSockets();
    };

    $scope.play = function() {
        $scope.buzz();
        audioService.play();
        $scope.updateCurrentStatusWithoutWebSockets();
    };

    $scope.pause = function() {
        audioService.pause();
        $scope.buzz();
        $scope.updateCurrentStatusWithoutWebSockets();
    };

    $scope.previous = function() {
        $scope.buzz();
        audioService.previous();
        $scope.updateCurrentStatusWithoutWebSockets();
    };

    $scope.next = function() {
        $scope.buzz();
        audioService.next();
        $scope.updateCurrentStatusWithoutWebSockets();
    };

    $scope.skipTo = function(song) {
        $scope.buzz();
        audioService.skipTo(song);
        $scope.updateCurrentStatusWithoutWebSockets();
    };

    $scope.$watch(blueboxService.isCordovaReady, function(online) {
        document.addEventListener("volumedownbutton", $scope.volumeDown, false);
        document.addEventListener("volumeupbutton",   $scope.volumeUp,   false);
    });

    $scope.volumeUp = function() {
        console.log("Volume up");
        $scope.setVolume($scope.volume.volume + 3);
    };

    $scope.volumeDown = function() {
        console.log("Volume down");
        $scope.setVolume($scope.volume.volume - 3);
    };

    var volumeIsOpen = false;
    var showVolumeDuration = 5000;

    $scope.volumeOpen = function() {
        if(!volumeIsOpen) {
            volumeIsOpen = true;
            $('#volume-control').find('.dropdown-menu').show();
            $('#volume-control').find('.dropdown-menu').dropdown('toggle');
        }
    };


    $scope.volumeClose = function() {
        if(volumeIsOpen) {
            volumeIsOpen = false;
            $('#volume-control').find('.dropdown-menu').hide();
        }
    };

    $scope.toggleVolumeSlider = function() {
        $scope.buzz();
        if($scope.hideVolumeBarTimer) {
            $timeout.cancel($scope.hideVolumeBarTimer);
            delete $scope.hideVolumeBarTimer;
        }

        console.log("Toggle volume slider");
        if(volumeIsOpen)
            $scope.volumeClose();
        else
            $scope.volumeOpen();

        $scope.hideVolumeBarTimer = $timeout(function() {
            console.log("Volume timed out, hiding");
            $scope.volumeClose();
        }, showVolumeDuration)
    };

    $scope.setVolume = function(vol) {
        if(vol < 0)     vol = 0;
        if(vol > 100)   vol = 100;

        console.log("Setting volume to "+vol);
        audioService.setVolume(vol);

        if($scope.hideVolumeBarTimer) {
            $timeout.cancel($scope.hideVolumeBarTimer);
            delete $scope.hideVolumeBarTimer;
            $scope.hideVolumeBarTimer = $timeout(function() {
                $scope.volumeClose();
            }, showVolumeDuration)
        }
    };

    $scope.slideIsOpen = false;

    $scope.slideOpen = function() {
        $scope.buzz();
        $scope.slideIsOpen = true;
    };

    $scope.slideClose = function() {
        $scope.buzz();
        $scope.slideIsOpen = false;
    };

    $scope.resetErrors = function() {
        $scope.errors = {};
    };

    $scope.addError = function(key, message) {
        $scope.errors[key] = message;
    };

    $scope.openDirectoryOptions = function(dir) {
        if(dir.name !== "..") {
            $scope.resetErrors();
            $scope.optionDirectory = dir;
            $("#directoryOptionsModal").modal("show");
        }
    };

    $scope.openSongOptions = function(song) {
        $scope.resetErrors();
        $scope.optionSong = song;
        $("#songOptionsModal").modal("show")
    };

    $scope.$watchGroup([
        function() {
            return $(document).height();
        },function() {
            return $("#audio-wrapper #audio-footer").width();
        },function() {
            return $scope.currentDirectory;
        },function() {
            return $("#audio-wrapper #audio-footer").height();
        }
    ],
    function() {
        $scope.fixPageComposition();
    });


    $scope.fixPageComposition = function() {
        function fixHeaderWidth() {
            var headerWidth = $('#audio-wrapper .audio-header').width();
            var buttonWidth = $('.audio-header-buttons').width();
            var titleLeftPx = $(".audio-header-title").css("left");
            if(typeof titleLeftPx !== "undefined") {
                var titleLeft = titleLeftPx.replace("px","");
                console.log("HEADER");
                console.log(headerWidth);
                console.log(buttonWidth);
                console.log(titleLeft);
                $(".audio-header-title").css("max-width", headerWidth - buttonWidth - titleLeft +"px")
            }
        }

        function fixContentAndFooterHeight() {
            var headerHeight = $("#audio-wrapper .audio-header").height();
            var footerHeight = $("#audio-wrapper #audio-footer").height();
            $("#audio-wrapper #audio-content table").css('margin-top', headerHeight + "px");
            $("#audio-wrapper #audio-content table").css('margin-bottom', footerHeight + headerHeight + "px");
            $(".error-notification-box").css('bottom', footerHeight + 8 + "px");
        }

        fixContentAndFooterHeight();
        fixHeaderWidth();
    };

    $scope.init();
});

audio.filter('time', function() {

    return function(seconds) {
        if(typeof seconds === "undefined" || isNaN(seconds) || seconds == "" || seconds === 0.0)
            return "00:00";

        var hours = Math.floor(seconds / 3600);
        var minutes = Math.floor((seconds % 3600) / 60);
        var seconds = Math.floor(seconds % 3600 % 60);

        if(hours < 10)
            var hoursString = "0"+hours;
        else
            var hoursString = ""+hours;

        if(minutes < 10)
            var minutesString = "0"+minutes;
        else
            var minutesString = ""+minutes;

        if(seconds < 10)
            var secondsString = "0"+seconds;
        else
            var secondsString = ""+seconds;

        if(hours == 0)
            return minutesString+":"+secondsString;
        else
            return hoursString+":"+minutesString+":"+secondsString;
    };
});


audio.filter('prettySong', function() {

    return function(song) {
        if(typeof song !== "undefined" && typeof song.clipInfo !== "undefined") {
            if(typeof song.clipInfo.title !== "undefined" && typeof song.clipInfo.artist !== "undefined")
                return song.clipInfo.artist + " - " + song.clipInfo.title;
            else if(typeof song.clipInfo.title !== "undefined" && typeof song.clipInfo.artist === "undefined")
                return song.clipInfo.title;
            else
                return song.fileName;
        }
        else {
            song;
        }
    };
});

audio.directive('audioApp', function() {
    return {
        controller: 'AudioController',
        templateUrl: 'apps/audio/audio.html'
    };
});