import sbt.Keys._
import akka.sbt.AkkaKernelPlugin._

organization := "nl.bluesoft"

name := "bluebox-server"

version := "0.0.1-SNAPSHOT"

scalaVersion := "2.11.6"

resolvers += "Local Maven Repository" at "file://"+Path.userHome.absolutePath+"/.m2/repository"

resolvers += "dnvriend at bintray" at "http://dl.bintray.com/dnvriend/maven"

credentials += Credentials(Path.userHome / ".ivy2" / ".credentials")

libraryDependencies ++=
  {
    val akkaV = "2.3.9"
    val sprayV = "1.3.1"
    val jsonV = "1.2.6"
    val streamV = "1.0"
    Seq(
      "org.scala-lang"           % "scala-library"                        % scalaVersion.value,
      "com.typesafe.akka"       %% "akka-kernel"                          % akkaV,
      "com.typesafe.akka"       %% "akka-actor"                           % akkaV,
      "com.typesafe.akka"       %% "akka-slf4j"                           % akkaV,
      "com.typesafe.akka"       %% "akka-cluster"                         % akkaV,
      "com.github.dnvriend"     %% "akka-persistence-jdbc"                % "1.1.9",
      "com.typesafe.akka"       %% "akka-contrib"                         % akkaV,
      "com.typesafe.akka"       %% "akka-persistence-experimental"        % akkaV,
      "com.typesafe.akka"       %% "akka-stream-experimental"             % streamV,
      "com.typesafe.akka"       %% "akka-http-core-experimental"          % streamV,
      "com.typesafe.akka"       %% "akka-http-experimental"               % streamV,
      "com.typesafe.akka"       %% "akka-http-spray-json-experimental"    % streamV,
      "com.typesafe.akka"       %% "akka-http-xml-experimental"           % streamV,
      "ch.qos.logback"           % "logback-classic"                      % "1.1.2",
      "org.codehaus.groovy"      % "groovy-all"                           % "1.8.2",
      "org.apache.velocity"      % "velocity"                             % "1.5",
      "com.sksamuel.elastic4s"  %% "elastic4s-core"                       % "1.6.5",
      "org.apache.commons"       % "commons-email"                        % "1.3.3",
      "com.luckycatlabs"         % "SunriseSunsetCalculator"              % "1.2",
      "com.typesafe.akka"       %% "akka-testkit"                         % akkaV   % "test",
      "io.spray"                %% "spray-testkit"                        % sprayV  % "test",
      "org.scalatest"           %% "scalatest"                            % "2.1.4" % "test",
      "org.pegdown"              % "pegdown"                              % "1.4.2" % "test",
      "com.typesafe.akka"       %% "akka-http-testkit-experimental"       % streamV % "test",
      "com.typesafe.akka"       %% "akka-stream-testkit-experimental"     % streamV % "test"
    )
  }

autoCompilerPlugins := true

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

publishMavenStyle := true

publishArtifact in Test := false

fork := true

fork in test := true

(parallelExecution in Test) := false

javaOptions ++= Seq("-Xms64M","-Xmx768M","-Dfile.encoding=ISO-8859-1")

// Akka Kernel Plugin settings
akka.sbt.AkkaKernelPlugin.distSettings

net.virtualvoid.sbt.graph.Plugin.graphSettings

distJvmOptions in Dist := "-Xms64M -Xmx1024M -DVERSION=" + version.value

distMainClass in Dist := "akka.kernel.Main nl.bluesoft.bluebox.KernelBoot"

testOptions in ThisBuild += Tests.Argument(TestFrameworks.ScalaTest, "-h", "target/test-reports")
