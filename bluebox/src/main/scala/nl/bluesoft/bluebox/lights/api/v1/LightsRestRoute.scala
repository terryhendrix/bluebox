package nl.bluesoft.bluebox.lights.api.v1

import java.nio.file.Path

import akka.actor.ActorSystem
import akka.http.scaladsl.server._
import nl.bluesoft.bluebox.audio.model._
import nl.bluesoft.bluebox.lights.LightsApp
import nl.bluesoft.bluebox.sdk.AppRoute
import nl.bluesoft.peanuts.http.routes._

import scala.concurrent.ExecutionContext

object LightsRestRoute {
  case class AudioActionRequest(action: String, song:Option[Song], mode:Option[String])
  case class RatingRequest(rating: Int)
  case class MoveRequest(from:Path, to:Path, copy:Boolean)
  case class CreateDirectoryRequest(name:String, asPartOf:Option[Path])
  case class SearchRequest(query: String)
  case class CreatePlaylistRequest(name: String)
  case class RenamePlaylistRequest(name:String)
  case class PlaylistMutationRequest(hash:String, after:Option[String])
  case class ActiveSongResponse(song:Option[Song], isPlaying:Boolean)
}

trait LightsRoute extends AppRoute with LightsRouteJsonProtocol with WebSockets
{
  def app:LightsApp

  override val name = "lights"
  override val version = "v1"

  override lazy val route: Route = {
    path("ws") {
      get {
        stringSocket(app.websocket)
      }
    } ~
    path("lights") {
      get {
        complete {
          app.lights.asJson
        }
      }
    } ~
    path("groups") {
      get {
        complete {
          app.groups.asJson
        }
      }
    }
  }
}

class LightsRestRoute(override val app:LightsApp)(implicit ec:ExecutionContext, override val system:ActorSystem) extends LightsRoute