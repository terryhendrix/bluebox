package nl.bluesoft.bluebox.lights.hue
import akka.actor.Props
import com.philips.lighting.hue.sdk.{PHAccessPoint, PHBridgeSearchManager, PHHueSDK}
import nl.bluesoft.bluebox.lights.LightRegistry.{GroupInitialized, LightInitialized}
import nl.bluesoft.bluebox.lights.hue.util.{HueSDKListener, HueActor}
import scala.concurrent.duration._
import scala.language.implicitConversions


object HueManager {
  case class AccessPointsFound(accessPoints:Seq[PHAccessPoint])

  sealed trait PrivateMessage
  case object DiscoverAccessPoints extends PrivateMessage
  def props() = Props(new HueManager())
}

class HueManager extends HueActor with HueSDKListener
{
  import HueManager._

  context.system.scheduler.schedule(10 millis, 60 seconds, self, DiscoverAccessPoints)

  override def onAccessPointsFound(accessPoints: Seq[PHAccessPoint]): Unit = {
    self ! AccessPointsFound(accessPoints)
  }

  override def receive: Receive = {
    case DiscoverAccessPoints ⇒
      log.info("Discovering access points")
      sdk.setAppName("BlueBox")
      sdk.setDeviceName("BlueBox")
      val sm = sdk.getSDKService(PHHueSDK.SEARCH_BRIDGE).asInstanceOf[PHBridgeSearchManager]
      sm.search(true, true)

    case msg@AccessPointsFound(accessPoints) ⇒
      log.debug(s"Found ${accessPoints.size} access points")
      accessPoints.foreach { ap ⇒
        log.info(ap)
        context.child(HueAccessPoint.name(ap)).getOrElse {
          log.info(s"Creating bridge ${HueAccessPoint.name(ap)}")
          context.actorOf(HueAccessPoint.props(ap), HueAccessPoint.name(ap))
        }
      }

      context.system.eventStream.publish(msg)

    case msg: LightInitialized ⇒
      context.parent forward msg

    case msg: GroupInitialized ⇒
      context.parent forward msg
  }
}
