package nl.bluesoft.bluebox.lights.hue.util
import com.philips.lighting.hue.listener.PHGroupListener
import com.philips.lighting.model.{PHHueError, PHBridgeResource, PHGroup}
import scala.collection.JavaConversions._

trait HueLightGroupListener extends PHGroupListener
{ this:HueActor ⇒


  final override def onStateUpdate(map: java.util.Map[String, String], list: java.util.List[PHHueError]): Unit = {
    onStateUpdate(map.toMap, list.toList)
  }

  final override def onReceivingAllGroups(bridgeResources: java.util.List[PHBridgeResource]): Unit = {
    onReceivingAllGroups(bridgeResources.toList)
  }

  def onReceivingAllGroups(bridgeResources: List[PHBridgeResource]): Unit = {
    log.info("Received all groups")
  }

  override def onReceivingGroupDetails(phGroup: PHGroup): Unit = {
    log.info("Received group details")
  }

  override def onCreated(phGroup: PHGroup): Unit = {
    log.info("A group is created")
  }

  def onStateUpdate(map: Map[String, String], list: List[PHHueError]): Unit = {
    log.info("State update")
  }

  override def onSuccess(): Unit = {
    log.info("Success")
  }

  override def onError(i: Int, s: String): Unit = {
    log.error(s"$i: $s")
  }
}
