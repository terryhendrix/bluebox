package nl.bluesoft.bluebox.lights.model
import nl.bluesoft.bluebox.lights.model.LightKinds.LightKind
import nl.bluesoft.peanuts.json.PeanutsJsonProtocol

object LightKinds {
  type LightKind = String
  val Hue:LightKind = "hue"
}

case class Light(id:String, kind:LightKind, name:String, version:String, model:String) {
  override def toString() = s"Light[$id]"
}

case class LightState(hue:Int, bri:Int, sat:Int,
                      on:Boolean)

case class LightGroup(id:String, name:String)

trait LightsJsonProtocol extends PeanutsJsonProtocol {
  implicit val lightStateFormat = jsonFormat4(LightState)
  implicit val lightFormat = jsonFormat5(Light)
  implicit val groupFormat = jsonFormat2(LightGroup)
}