package nl.bluesoft.bluebox.lights

import akka.actor._
import akka.stream.scaladsl._
import nl.bluesoft.bluebox.lights.LightsApp.UpdateLightState
import nl.bluesoft.bluebox.lights.api.v1.LightsRestRoute
import nl.bluesoft.bluebox.lights.model.{Light, LightGroup, LightState}
import nl.bluesoft.bluebox.sdk.{AppRoute, BlueBoxApp, SystemDispatcher}
import nl.bluesoft.peanuts.elasticsearch.util.ElasticSearchImplicits

/**
 * A factory for the AudioApp, which is then registered as ''Extension''
 */
object LightsApp extends ExtensionIdProvider with ExtensionId[LightsApp]
{
  override def createExtension(system: ExtendedActorSystem): LightsExtension = {
    system.log.info("Loading LightsApp")
    new LightsExtension(system)
  }
  override def lookup(): ExtensionId[_ <: Extension] = LightsApp

  trait GenericMessages
  case class UpdateLightState(light:Light, state:LightState) extends GenericMessages
  case class LightStateUpdated(light:Light, state:LightState) extends GenericMessages
  case class UpdateLightGroupState(group:LightGroup, state:LightState) extends GenericMessages
}

/**
 * An interface for interacting with the AudioExtension
 */
trait LightsApp extends BlueBoxApp with Extension with ElasticSearchImplicits with SystemDispatcher {
  def websocket:Source[String, Unit]

  def lights:Seq[Light]
  def setLight(id:String, state:LightState):Unit

  def groups:Seq[LightGroup]
}

trait ActorDef  {
}

trait IndexDef {
}

/**
 * An akka ''Extension'' implementation of ''AudioApp''
 * @param system    The ActorSystem to create the extension in.
 */
class LightsExtension(override val system:ExtendedActorSystem) extends LightsApp
{
  override val route: AppRoute = new LightsRestRoute(this)(system.dispatcher, system)
  val registry = system.actorOf(LightRegistry.props, "LightsRegistry")
  var registryMirror = LightRegistry.LightRegistryState()

  system.actorOf(Props(new Actor {
    override def preStart() = {
      context.system.eventStream.subscribe(self, classOf[LightRegistry.LightRegistryStateUpdate])
    }

    override def receive: Receive = {
      case LightRegistry.LightRegistryStateUpdate(st) ⇒
        registryMirror = st
    }
  }), "LightsUpdateConsumer")

  override def websocket: Source[String, Unit] = ???

  override def lights:Seq[Light] = registryMirror.lights

  override def setLight(id: String, state: LightState): Unit = {
    registryMirror.lights.find(_.id == id).foreach { light ⇒
      system.eventStream.publish(UpdateLightState(light, state))
    }
  }

  override def groups: Seq[LightGroup] = registryMirror.groups
}



