package nl.bluesoft.bluebox.lights.hue
import akka.actor.Props
import com.philips.lighting.model._
import nl.bluesoft.bluebox.lights.hue.util.{HueActor, HueLightGroupListener}
import nl.bluesoft.bluebox.lights.{LightRegistry, LightsApp}

import scala.collection.JavaConversions._
import scala.language.implicitConversions

object HueLightGroup
{
  val PrimaryGroupName = "Primary"
  val PrimaryGroupId = "primary_group_id"

  def name(group:Option[PHGroup]) = s"Group-${group.map(_.getUniqueId).getOrElse("default")}"
  def props(bridge: PHBridge, group:Option[PHGroup]) = Props(new HueLightGroup(bridge, group))
}

class HueLightGroup(bridge: PHBridge, group:Option[PHGroup]) extends HueActor with HueLightGroupListener
{
  override implicit val log = super.log

  override def preStart() = {
    super.preStart()
    context.parent ! LightRegistry.GroupInitialized(group)
    context.system.eventStream.subscribe(self, classOf[LightsApp.UpdateLightGroupState])
  }

  override def onStateUpdate(map: Map[String, String], list: List[PHHueError]) = {
    updateLightsCache(None)   // use the resource cache to get the state from
  }

  def updateLightsCache(state:Option[PHLightState]) = {
    if(group.isEmpty) {   // is primary group when empty
      bridge.getResourceCache.getAllLights.toSeq.foreach { light ⇒
        context.parent ! HueLight.SetStateCache(state.getOrElse(light.getLastKnownLightState), Some(light))
      }
    }
    else {
      bridge.getResourceCache.getAllGroups.toSeq.find(_.getUniqueId == group.get.getUniqueId).foreach { matchingGroup ⇒
        matchingGroup.getLightIdentifiers.foreach { lightIdentifier ⇒
          bridge.getResourceCache.getAllLights.find(_.getUniqueId == lightIdentifier).foreach { foundLight ⇒
            context.parent ! HueLight.SetStateCache(state.getOrElse(foundLight.getLastKnownLightState), Some(foundLight))
          }
        }
      }
    }
  }


  override def receive: Receive = {
    case LightsApp.UpdateLightGroupState(g, state) if group.isDefined && g.id == group.get.getUniqueId ⇒
      log.info(s"Request an update to $state")
      bridge.setLightStateForGroup(group.get.getName, state, this)

    case LightsApp.UpdateLightGroupState(g, state) if group.isEmpty && g.id == HueLightGroup.PrimaryGroupId ⇒
      log.info(s"Request an update to $state")
      bridge.setLightStateForDefaultGroup(state)
      updateLightsCache(Some(state))      // Since the hue api has no callback for default group, we assume everything went well..

    case msg: LightsApp.UpdateLightGroupState ⇒
      log.info(s"Ignoring command $msg cause it is not for this group. $group")
  }
}
