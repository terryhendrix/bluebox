package nl.bluesoft.bluebox.lights.api.v1

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import nl.bluesoft.bluebox.lights.model.LightsJsonProtocol
import nl.bluesoft.peanuts.json.JsonMarshalling

trait LightsRouteJsonProtocol extends LightsJsonProtocol with JsonMarshalling with SprayJsonSupport {

}
