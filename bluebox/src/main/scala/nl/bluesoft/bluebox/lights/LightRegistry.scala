package nl.bluesoft.bluebox.lights
import akka.actor.{ActorLogging, Props}
import nl.bluesoft.bluebox.lights.LightRegistry.{GroupInitialized, LightRegistryStateUpdate, LightInitialized, LightRegistryState}
import nl.bluesoft.bluebox.lights.hue.HueManager
import nl.bluesoft.bluebox.lights.model.{LightGroup, Light}
import nl.bluesoft.peanuts.actor.PeanutActor

object LightRegistry 
{
  def props = Props(new LightRegistry)

  trait Message
  case class LightInitialized(light:Light) extends Message
  case class GroupInitialized(group:LightGroup) extends Message
  case class LightRegistryStateUpdate(state:LightRegistryState) extends Message

  trait State
  case class LightRegistryState(lights:Seq[Light] = Seq.empty, groups:Seq[LightGroup] = Seq.empty) extends State
}

class LightRegistry extends PeanutActor[LightRegistryState] with ActorLogging
{
  val hue = context.actorOf(HueManager.props(),"HueManager")

  set stateTo LightRegistryState()

  override def preStart() = {
    super.preStart()
  }

  override protected def events: Receive = {
    case _ if 1 == 2 ⇒
  }

  override protected def commands: Receive = {
    case msg:LightInitialized ⇒
      withState { state ⇒
        log.info(s"Registering light ${msg.light}")
        set stateTo state.copy(lights = state.lights :+ msg.light)
      }
    case msg:GroupInitialized ⇒
      withState { state ⇒
        log.info(s"Registering group ${msg.group}")
        set stateTo state.copy(groups = state.groups :+ msg.group)
      }
    case _ if 1 == 2 ⇒
  }

  override def whenStateChanged(state:Option[LightRegistryState]) ={
    state.foreach { s ⇒
      log.debug("Publishing state update")
      context.system.eventStream.publish(LightRegistryStateUpdate(s))
    }
  }

  override def persistenceId: String = "LightRegistrar"
}
