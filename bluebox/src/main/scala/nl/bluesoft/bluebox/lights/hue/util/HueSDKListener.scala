package nl.bluesoft.bluebox.lights.hue.util

import com.philips.lighting.hue.sdk.{PHAccessPoint, PHSDKListener}
import com.philips.lighting.model._

import scala.collection.JavaConversions._

trait HueSDKListener extends PHSDKListener
{ this:HueActor ⇒

  override def onError(i: Int, s: String): Unit = {
    log.error(s"$i: $s")
  }

  final override def onCacheUpdated(codes: java.util.List[Integer], bridge: PHBridge): Unit = {
    onCacheUpdated(codes.toSeq, bridge)
  }

  def onCacheUpdated(codes: Seq[Integer], phBridge: PHBridge) = {
  }

  final override def onAccessPointsFound(accessPoints: java.util.List[PHAccessPoint]): Unit = {
    onAccessPointsFound(accessPoints.toSeq)
  }

  def onAccessPointsFound(accessPoints: Seq[PHAccessPoint]): Unit = {
    log.info(s"${accessPoints.size} access points found")
  }

  override def onConnectionLost(accessPoint: PHAccessPoint): Unit = {
    log.error("Connection lost")
  }

  override def onConnectionResumed(bridge: PHBridge): Unit = {
    log.info("Connection resumed")
  }

  override def onAuthenticationRequired(accessPoint: PHAccessPoint): Unit = {
    log.warning("Authentication required")
  }

  override def onBridgeConnected(bridge: PHBridge, s: String): Unit = {
    log.info(s"bridge connected: $s")
  }

  final override def onParsingErrors(errors: java.util.List[PHHueParsingError]): Unit = {
    onParsingErrors(errors.toSeq)
  }

  def onParsingErrors(errors: Seq[PHHueParsingError]): Unit = {
    log.info(s"parsing errors: ${errors.map(_.getMessage).mkString(", ")}")
  }

  override def preStart() = {
    log.info("Subscribing to HueSDK")
    sdk.getNotificationManager.registerSDKListener(this)
  }

  override def postStop() = {
    log.debug("Unsubscribing from HueSDK")
    sdk.getNotificationManager.unregisterSDKListener(this)
  }
}
