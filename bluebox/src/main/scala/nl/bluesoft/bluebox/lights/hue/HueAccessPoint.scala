package nl.bluesoft.bluebox.lights.hue
import akka.actor.{PoisonPill, Props}
import com.philips.lighting.hue.sdk.PHAccessPoint
import com.philips.lighting.model.PHBridge
import nl.bluesoft.bluebox.lights.LightRegistry.{GroupInitialized, LightInitialized}
import nl.bluesoft.bluebox.lights.hue.HueLight.SetStateCache
import nl.bluesoft.bluebox.lights.hue.util.{HueSDKListener, HueActor}
import scala.collection.JavaConversions._
import scala.concurrent.duration._

object HueAccessPoint
{
  def name(ap:PHAccessPoint)  = "AccessPoint-"+ap.getIpAddress
  def props(ap:PHAccessPoint) = Props(new HueAccessPoint(ap))

  case class Connected(ap:PHAccessPoint, bridge:PHBridge)
  case class Disconnected(ap:PHAccessPoint)
  case class AuthenticationRequired(ap:PHAccessPoint)

  sealed trait PrivateMessage
  case object Discover extends PrivateMessage
}

class HueAccessPoint(ap:PHAccessPoint) extends HueActor with HueSDKListener
{
  import HueAccessPoint._

  var bridge:Option[PHBridge] = None

  context.system.scheduler.schedule(10 millis, 15 seconds, self, Discover)

  override def preStart() = {
    super.preStart()
    context.system.eventStream.subscribe(self, classOf[Connected])
    context.system.eventStream.subscribe(self, classOf[Disconnected])
  }

  override def onConnectionLost(accessPoint: PHAccessPoint): Unit = {
    log.error(s"Connection lost")
    context.system.eventStream.publish(Disconnected(ap))
  }

  override def onConnectionResumed(bridge: PHBridge): Unit = {
    log.info("Connection resumed")
    context.system.eventStream.publish(Connected(ap, bridge))
  }

  override def onAuthenticationRequired(accessPoint: PHAccessPoint): Unit = {
    log.warning("Authentication required")
    context.system.eventStream.publish(AuthenticationRequired(ap))
    sdk.startPushlinkAuthentication(accessPoint)
  }

  // TODO: Store hash somewhere
  override def onBridgeConnected(bridge: PHBridge, hash: String): Unit = {
    log.info(s"Connected as $hash")
    context.system.eventStream.publish(Connected(ap, bridge))
  }

  def initAP(bridge:PHBridge) = {
    this.bridge = Some(bridge)
    initLights(bridge)
    initGroups(bridge)
  }
  
  def initLights(bridge:PHBridge) = {
    val lights = bridge.getResourceCache.getAllLights.toSeq
    log.info(s"Creating ${lights.size} lights")

    lights.foreach { light ⇒
      val ref = context.child(HueLight.name(light)).getOrElse {
        log.info(s"Creating light ${HueLight.name(light)}")
        context.actorOf(HueLight.props(bridge, light), HueLight.name(light))
      }
      ref ! HueLight.SetStateCache(light.getLastKnownLightState)
    }
  }

  def initGroups(bridge:PHBridge) = {
    def defaultGroup = None

    val groups = bridge.getResourceCache.getAllGroups.toSeq.map(Some(_)) :+ defaultGroup
    log.info(s"Creating ${groups.size} groups")

    groups.foreach { group ⇒
      context.child(HueLightGroup.name(group)).getOrElse {
        log.info(s"Creating group ${HueLightGroup.name(group)}")
        context.actorOf(HueLightGroup.props(bridge, group), HueLightGroup.name(group))
      }
    }
  }

  def terminateLights() = {
    bridge = None
    context.children.foreach(_ ! PoisonPill)
  }
  
  override def receive: Receive = {
    case Discover ⇒
      if(bridge.isEmpty) {
        ap.setUsername("2ad54eeb3814134f331b8bf329b6dcd7")    // TODO: Retrieve stored hash
        sdk.connect(ap)
      }
      else {
        bridge.foreach(initAP)
      }

    case Connected(that, connectedTo) if that.getIpAddress == this.ap.getIpAddress ⇒
      initAP(connectedTo)

    case Disconnected(that) if that.getIpAddress == this.ap.getIpAddress ⇒
      terminateLights()

    case msg: LightInitialized ⇒
      context.parent forward msg

    case msg: GroupInitialized ⇒
      context.parent forward msg

    case msg @ SetStateCache(_, Some(light)) ⇒
      val childName = HueLight.name(light)
      context.child(childName) match {
        case Some(ref) ⇒ ref forward msg
        case None      ⇒ log.error(s"Cannot route $msg. $childName could not be resolved.")
      }

    case msg @ SetStateCache(_, None) ⇒
      log.error(s"Cannot route $msg, the light is not specified")
  }
}
