package nl.bluesoft.bluebox.lights.hue
import akka.actor.Props
import akka.event.LoggingAdapter
import com.philips.lighting.model._
import nl.bluesoft.bluebox.lights.hue.util.{HueActor, HueLightListener}
import nl.bluesoft.bluebox.lights.model.{Light, LightState}
import nl.bluesoft.bluebox.lights.{LightRegistry, LightsApp}
import nl.bluesoft.bluebox.sdk.Command

import scala.collection.JavaConversions._
import scala.language.implicitConversions
object HueLight
{
  def name(light:PHLight) = s"Light-${light.getUniqueId}"
  def name(light:Light) = s"Light-${light.id}"
  def props(bridge: PHBridge, light:PHLight) = Props(new HueLight(bridge, light))

  /**
   * Sets the internal cached state of a light, this event is routed by the supervision hierarchy and NEVER published on the eventStream.
   * @param state   The new state of the light to cache
   * @param light   This argument is used for routing the request (if required). If not required None may be used.
   */
  private[hue] case class SetStateCache(state:PHLightState, light:Option[Light] = None)

  class ConnectCommand(light:PHLight, bridge:PHBridge)(override implicit val log:LoggingAdapter) extends Command
  {
    override def exec() = {
      val state = new PHLightState()
      state.setOn(true)
      state.setBrightness(20)
      state.setTransitionTime(3)
      bridge.updateLightState(light,state)

      Thread.sleep(1000)

      state.setOn(true)
      state.setBrightness(160)
      state.setTransitionTime(10)
      bridge.updateLightState(light,state)
    }
  }
}

class HueLight(bridge: PHBridge, light:PHLight) extends HueActor with HueLightListener
{
  var lastKnownState: Option[LightState] = None
  val blinkOnConnect:Boolean = false
  import HueLight._

  override implicit val log = super.log
  val connectAction = new ConnectCommand(light, bridge)


  override def onStateUpdate(map: Map[String, String], list: List[PHHueError]): Unit = {
    bridge.getResourceCache.getAllLights.toSeq.find(_.getUniqueId == light.getUniqueId).foreach { matchingLight ⇒
      self ! SetStateCache(matchingLight.getLastKnownLightState)
    }
  }

  override def preStart() = {
    super.preStart()
    if(blinkOnConnect)
      connectAction.exec()
    context.parent ! LightRegistry.LightInitialized(light)
    context.system.eventStream.subscribe(self, classOf[LightsApp.UpdateLightState])
  }

  override def receive: Receive = {
    case LightsApp.UpdateLightState(l, state) if l.id == light.getUniqueId ⇒
      log.info(s"Requesting an update to ${phLightState2LightState(state)}")
      bridge.updateLightState(light, state, this)

    case msg: LightsApp.UpdateLightState ⇒
      log.debug(s"Ignoring command $msg because the id does not match")

    case SetStateCache(state,_) if lastKnownState.isEmpty || lastKnownState.get != phLightState2LightState(state) ⇒
      if(lastKnownState.isDefined) { // Only publish when there is in fact an update, not on initialization of the state
        log.info(s"Updating the light state to ${phLightState2LightState(state)}")
        context.system.eventStream.publish(LightsApp.LightStateUpdated(light, state))
      }
      else {
        log.info(s"Initializing state to $state")

      }
      lastKnownState = Some(state)
  }
}
