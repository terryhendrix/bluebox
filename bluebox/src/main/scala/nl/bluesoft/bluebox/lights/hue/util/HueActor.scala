package nl.bluesoft.bluebox.lights.hue.util
import akka.actor.{Actor, ActorLogging}
import com.philips.lighting.hue.sdk.{PHAccessPoint, PHHueSDK}
import com.philips.lighting.model.{PHGroup, PHLight, PHLightState}
import nl.bluesoft.bluebox.lights.hue.HueLightGroup
import nl.bluesoft.bluebox.lights.model.{Light, LightGroup, LightKinds, LightState}

import scala.concurrent.ExecutionContext
import scala.language.implicitConversions

trait HueActor extends Actor with ActorLogging
{
  implicit lazy val ec:ExecutionContext = context.dispatcher

  val sdk =  PHHueSDK.getInstance()

  implicit def accessPoint2String(ap:PHAccessPoint): String = s"${ap.getUsername}@${ap.getIpAddress}, ID=${ap.getBridgeId}, MAC=${ap.getMacAddress}"

  implicit def phLight2Light(light:PHLight): Light = {
    Light(
      id   = light.getUniqueId,
      kind = LightKinds.Hue,
      name = light.getName,
      version = light.getVersionNumber,
      model = light.getModelNumber
    )
  }

//  implicit def light2phLight(light:Light)(implicit bridge: PHBridge): PHLight = {
//    require(light.kind == LightKinds.Hue, "The light kind must be hue to convert to a PHLight")
//    bridge.getResourceCache.getAllLights.toSeq.find(_.getUniqueId == light.id).get
//  }

  implicit def phLightState2LightState(state:PHLightState): LightState = {
    LightState(
      hue = state.getHue,
      bri = state.getBrightness,
      sat = state.getSaturation,
      on = state.isOn
    )
  }

  implicit def lightState2PHLightState(state:LightState): PHLightState = {
    val phState = new PHLightState()
    phState.setBrightness(state.bri)
    phState.setSaturation(state.sat)
    phState.setHue(state.hue)
    phState.setOn(state.on)
    phState
  }


  implicit def phGroup2LightGroup(group:PHGroup):LightGroup = {
    LightGroup(group.getName, group.getUniqueId)
  }

  implicit def phGroup2DefaultGroup(group:Option[PHGroup]):LightGroup = {
    group.map(phGroup2LightGroup).getOrElse{
      LightGroup(
        id   = HueLightGroup.PrimaryGroupId,
        name = HueLightGroup.PrimaryGroupName
      )
    }
  }

  override def postStop() = {
    context.system.eventStream.unsubscribe(self)
  }
}