package nl.bluesoft.bluebox.lights.extensions
import akka.actor.ActorSystem
import akka.event.LoggingAdapter
import nl.bluesoft.bluebox.lights.LightsApp
import nl.bluesoft.bluebox.lights.model.{Light, LightGroup, LightState}
import nl.bluesoft.bluebox.sdk.Command

case class LightStateCommand(light:Light, state:LightState)(implicit val system:ActorSystem) extends Command
{
  override def exec(): Unit = {
    system.eventStream.publish(LightsApp.UpdateLightState(light, state))
  }

  override def log: LoggingAdapter = system.log
}

case class LightGroupStateCommand(group:LightGroup, state:LightState)(implicit val system:ActorSystem) extends Command
{
  override def log: LoggingAdapter = system.log
  override def exec(): Unit = {
    system.eventStream.publish(LightsApp.UpdateLightGroupState(group, state))
  }
}
