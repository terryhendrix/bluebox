package nl.bluesoft.bluebox.lights.hue.util
import com.philips.lighting.hue.listener.PHLightListener
import com.philips.lighting.model.{PHHueError, PHLight, PHBridgeResource}
import scala.collection.JavaConversions._

trait HueLightListener extends PHLightListener
{ this:HueActor ⇒
  
  /**
   * Dont allow overriding of this java style method
   * @param resourceCaches
   */
  final override def onReceivingLights(resourceCaches: java.util.List[PHBridgeResource]): Unit = {
    onReceivingLights(resourceCaches.toList)
  }

  final override def onStateUpdate(map: java.util.Map[String, String], list: java.util.List[PHHueError]): Unit = {
    onStateUpdate(map.toMap, list.toList)
  }

  def onReceivingLights(list: List[PHBridgeResource]): Unit = {
    log.info("Receiving lights")
  }

  override def onSearchComplete(): Unit = {
    log.info("Search completed")
  }

  override def onReceivingLightDetails(phLight: PHLight): Unit = {
    log.info("Received light details")
  }

  def onStateUpdate(map: Map[String, String], list: List[PHHueError]) = {
  }

  override def onSuccess(): Unit = {
  }

  override def onError(i: Int, s: String): Unit = {
    log.error(s"$i: $s")
  }
}
