package nl.bluesoft.bluebox.util.files
import java.nio.file.{Files, Path, Paths}
import akka.event.LoggingAdapter
import nl.bluesoft.bluebox.util.{BlueBoxException, ErrorCodes}

import scala.concurrent.{ExecutionContext, Future}

trait FileManagement
{

  def createDirectory(path:Path)(implicit ec:ExecutionContext): Future[Unit] = {
    Future {
      if(Files.exists(path))
        throw new BlueBoxException(ErrorCodes.DirectoryAlreadyExists, s"${path.getFileName} already exists")

      Files.createDirectories(path)
    }
  }

  def copyDirectory(from: Path, dir: Path)(implicit ec:ExecutionContext): Future[Unit] = {
    Future {
      if(!Files.isDirectory(from))
        throw new BlueBoxException(ErrorCodes.NotADirectory, s"${from.getFileName} is not a directory")

      if(!Files.isDirectory(dir))
        throw new BlueBoxException(ErrorCodes.NotADirectory, s"${dir.getFileName} is not a directory")

      val destination = Paths.get(dir.toAbsolutePath.toString + "/" + from.getFileName.toString)

      Copy.copyRecursive(from, destination)
      destination
    }
    .flatMap(updated)
  }

  def moveDirectory(from:Path, dir:Path)(implicit ec:ExecutionContext, log:LoggingAdapter): Future[Unit] = {
    Future {
      if(!Files.isDirectory(from))
        throw new BlueBoxException(ErrorCodes.NotADirectory, "'from' must be a directory")

      val destination = dir match {
        case _ if Files.isDirectory(dir) ⇒
          val destination = Paths.get(dir.toAbsolutePath.toString + "/" + from.getFileName.toString)
          if(Files.exists(destination))
            throw new BlueBoxException(ErrorCodes.DirectoryAlreadyExists, s"destination '$destination' already exists")
          else
            destination
        case _ if !Files.exists(dir) ⇒
          log.info(s"Creating directories ${dir.toAbsolutePath}")
          Files.createDirectories(dir.getParent)
          dir
      }

      log.info(s"Source $from target $destination")
      Files.move(from, destination)
      destination
    }
    .flatMap(updated)
    .flatMap(_ ⇒ updated(from.getParent))
    .recover{ case ex:Throwable ⇒ ex.printStackTrace()}
  }

  def deleteDirectory(path:Path, force:Boolean)(implicit ec:ExecutionContext): Future[Unit] = {
    Future {
      if(!Files.exists(path))
        throw new BlueBoxException(ErrorCodes.NotFound, s"${path.getFileName} does not exist")

      if(!Files.isDirectory(path))
        throw new BlueBoxException(ErrorCodes.NotADirectory, s"${path.getFileName} is not a directory")

      if(force)
        Delete.deleteRecursive(path)
      else
        Files.delete(path)
    }
    .flatMap(_ ⇒ deleted(path))
  }

  def moveFile(from:Path, to:Path)(implicit ec:ExecutionContext): Future[Unit] = {
    Future {
      if(!Files.isRegularFile(from))
        throw new BlueBoxException(ErrorCodes.NotAFile, "'from' must be a regular file")

      if(Files.isDirectory(to)) {
        val destination = Paths.get(to.toAbsolutePath.toString + "/" + from.getFileName.toString)
        Files.move(from, destination)
      }
      else {
        Files.move(from, to)
      }
    }
    .flatMap(_ ⇒ updated(to))
    .flatMap(_ ⇒ updated(from.getParent))
  }

  def copyFile(from: Path, dir: Path)(implicit ec:ExecutionContext): Future[Unit] = {
    Future {
      if(!Files.isRegularFile(from))
        throw new BlueBoxException(ErrorCodes.NotAFile, s"${from.getFileName} is not a regular file")

      if(!Files.isDirectory(dir))
        throw new BlueBoxException(ErrorCodes.NotADirectory, s"${dir.getFileName} is not a directory")

      val destination = Paths.get(dir.toAbsolutePath.toString + "/" + from.getFileName.toString)
      Files.copy(from, destination)
    }
    .flatMap(_ ⇒ updated(dir))
  }

  def deleteFile(path:Path)(implicit ec:ExecutionContext): Future[Unit] = {
    Future {
      if(!Files.exists(path))
        throw new BlueBoxException(ErrorCodes.NotFound, s"${path.getFileName} does not exist")

      if(!Files.isRegularFile(path))
        throw new BlueBoxException(ErrorCodes.NotAFile, s"${path.getFileName} is not a regular file")

      Files.delete(path)
    }.flatMap(_ ⇒ deleted(path))
  }

  /**
   * This method is called for every directory or file that has been updated or created.
   * it to attach your handling.
   * @param path
   * @return
   */
  def updated(path:Path)(implicit ec:ExecutionContext):Future[Unit] = Future(Unit)

  /**
   * This method is called for every directory or file that has been deleted.
   * it to attach your handling.
   * @param path
   * @return
   */
  def deleted(path:Path)(implicit ec:ExecutionContext):Future[Unit] = Future(Unit)
}
