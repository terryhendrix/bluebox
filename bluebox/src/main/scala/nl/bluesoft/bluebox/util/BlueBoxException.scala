package nl.bluesoft.bluebox.util
import nl.bluesoft.bluebox.util.ErrorCodes.ErrorCode

case class BlueBoxException(code:ErrorCode, msg:String) extends Exception(msg) {
  override def toString() = {
    Seq(code, ": ", super.getMessage()).mkString
  }
}

case object ErrorCodes {
  type ErrorCode = Int

  // Http Like
  val NotFound                = 404
  val Conflict                = 409
  val Error                   = 500

  // Custom, will transform to HTTP 500
  val InvalidType             = 520
  val SecurityViolation       = 521

  // Directory related errors
  val DirectoryAlreadyExists  = 5000
  val DirectoryIsNotReadable  = 5001
  val DirectoryIsNotWritable  = 5002
  val NotADirectory           = 5003

  // File related errors
  val FileAlreadyExists       = 5100
  val FileIsNotReadable       = 5101
  val FileIsNotWritable       = 5102
  val NotAFile                = 5103
}