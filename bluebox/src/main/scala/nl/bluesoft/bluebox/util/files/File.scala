package nl.bluesoft.bluebox.util.files

import java.io.IOException
import java.nio.file._
import java.nio.file.attribute.BasicFileAttributes

object Copy
{
  def copyRecursive(srcDir: Path, dstDir: Path) = {
    Files.walkFileTree(srcDir, new SimpleFileVisitor[Path]()
    {
      override def visitFile(file: Path, attrs: BasicFileAttributes): FileVisitResult = {
        copy(file)
      }

      override def preVisitDirectory(dir: Path, attrs: BasicFileAttributes): FileVisitResult = {
        copy(dir)
      }
      def copy(fileOrDir: Path) : FileVisitResult = {
        Files.copy(fileOrDir, dstDir.resolve(srcDir.relativize(fileOrDir)))
        FileVisitResult.CONTINUE
      }
    })
  }
}

object Move
{
  def moveRecursive(srcDir: Path, dstDir: Path) = {
    Files.walkFileTree(srcDir, new SimpleFileVisitor[Path]()
    {
      override def visitFile(file: Path, attrs: BasicFileAttributes): FileVisitResult = {
        move(file)
      }

      override def preVisitDirectory(dir: Path, attrs: BasicFileAttributes): FileVisitResult = {
        move(dir)
      }
      def move(fileOrDir: Path) : FileVisitResult = {
        Files.move(fileOrDir, dstDir.resolve(srcDir.relativize(fileOrDir)))
        FileVisitResult.CONTINUE
      }
    })
  }
}

object Delete
{
  def deleteRecursive(path: Path) = {
    Files.walkFileTree(path, new SimpleFileVisitor[Path]()
    {
      override def visitFile(file: Path, attrs: BasicFileAttributes): FileVisitResult = {
        Files.delete(file)
        return FileVisitResult.CONTINUE
      }

      override def visitFileFailed(file: Path, exc: IOException): FileVisitResult = {
        Files.delete(file)
        return FileVisitResult.CONTINUE
      }

      override def postVisitDirectory(dir: Path, exc: IOException): FileVisitResult = {
        if (exc == null) {
          Files.delete(dir)
          return FileVisitResult.CONTINUE
        }
        else {
          throw exc
        }
      }
    })
  }
}
