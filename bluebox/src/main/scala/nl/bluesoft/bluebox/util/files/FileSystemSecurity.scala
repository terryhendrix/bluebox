package nl.bluesoft.bluebox.util.files

import java.nio.file.Path

import nl.bluesoft.bluebox.util.{BlueBoxException, ErrorCodes}

trait FileSystemSecurity {
  def rootDirectory:Path

  def isPartOfMusicRoot(path:Path): Boolean = {
    path.toAbsolutePath.toString.startsWith(rootDirectory.toAbsolutePath.toString)
  }

  def mustBePartOfRoot(path:Path) = {
    if(!isPartOfMusicRoot(path))
      throw new BlueBoxException(ErrorCodes.Conflict, s"$path is not part of the root $rootDirectory")

    if(path.toAbsolutePath.toString.contains(".."))
      throw new BlueBoxException(ErrorCodes.SecurityViolation, s"$path is not part of the root $rootDirectory")
  }
}