package nl.bluesoft.bluebox.util.files
import java.nio.file.Path

import akka.actor.{Actor, ActorLogging}
import akka.util.Timeout

import scala.concurrent.Future

object FileManager {

  trait Command

  case class MoveFile(file:Path, dir:Path) extends Command
  case class CopyFile(file:Path, dir:Path) extends Command
  case class DeleteFile(file:Path) extends Command

  case class CreateDirectory(dir:Path) extends Command
  case class MoveDirectory(from:Path, dir:Path) extends Command
  case class CopyDirectory(from:Path, dir:Path) extends Command
  case class DeleteDirectory(from:Path, force:Boolean) extends Command

  trait Event

  case class DirectoryUpdated(path:Path) extends Event
  case class Completed(msg:AnyRef, ex:Option[Throwable] = None) extends Event
  case class Busy(currentAction:AnyRef) extends Event
}

/**
 * The ''FilesManager'' manages file in a specific directory and forbids mutation outside this directory
 */
trait FileManager extends Actor with ActorLogging with FileManagement with FileSystemSecurity
{
  import FileManager._
  implicit lazy val ec = context.system.dispatcher
  implicit def timeout:Timeout
  def rootDirectory:Path
  implicit override val log = super.log
  var currentAction:Option[Command] = None
  override def receive = idle

  def idle:Receive = {
    case msg:Command if decision.isDefinedAt(msg) ⇒
      val ref = sender()
      context.become(working)
      currentAction = Some(msg)

      decision(msg).map { _ ⇒
        self ! Completed(msg)
        ref ! Completed(msg)
      }.recover { case ex:Throwable ⇒
        log.error("{}: {}", ex.getClass.getName, ex.getMessage)
        self ! Completed(msg, Some(ex))
        ref ! Completed(msg, Some(ex))
      }
    case msg ⇒
      log.warning(s"Unhandled: $msg")
  }

  def working:Receive = {
    case Completed(_,_) ⇒
      currentAction = None
      context.become(idle)
    case msg:Command if decision.isDefinedAt(msg)  ⇒
      log.info(s"Unhandled: $msg Cannot do another file-system action.")
      sender ! Busy(currentAction.get)
    case msg ⇒
      log.warning(s"Unhandled: $msg")
  }


  def decision:PartialFunction[Command, Future[Unit]] = {
    case CreateDirectory(dir) ⇒
      log.info(s"Creating directory $dir")
      createDirectory(dir).map { _ ⇒
        context.system.eventStream.publish(DirectoryUpdated(dir.getParent))
      }

    case MoveDirectory(from, dir) ⇒
      log.info(s"Moving directory $from to $dir")
      moveDirectory(from, dir)

    case CopyDirectory(from, dir) ⇒
      log.info(s"Copying directory from to $dir")
      copyDirectory(from, dir)

    case DeleteDirectory(from, force) ⇒
      log.info(s"Deleting directory $from")
      deleteDirectory(from, force)

    case MoveFile(file, dir) ⇒
      log.info(s"Moving file $file to $dir")
      moveFile(file, dir)

    case CopyFile(file, dir) ⇒
      log.info(s"Copying file $file to $dir")
      copyFile(file, dir)

    case DeleteFile(file) ⇒
      log.info(s"Deleting file $file")
      deleteFile(file)
  }
}
