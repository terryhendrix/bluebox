package nl.bluesoft.bluebox.util
import java.nio.file.{Paths, Path}

trait BlueBoxPredef {
  implicit def string2Path(s:String): Path = Paths.get(s)
}

object BlueBoxPredef extends BlueBoxPredef