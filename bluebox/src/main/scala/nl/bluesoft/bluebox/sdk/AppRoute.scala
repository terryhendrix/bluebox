package nl.bluesoft.bluebox.sdk
import akka.actor.{Actor, ActorSystem}
import akka.http.scaladsl.server.{Directives, Route}
import akka.stream.{ActorMaterializer, Materializer}

import scala.concurrent.ExecutionContext

trait BlueBoxApp {
  def route:AppRoute
}

trait AppRoute extends Directives with SystemDispatcher
{
  implicit lazy val mat:Materializer = ActorMaterializer()
  def name:String
  def version:String
  def route:Route
}

trait Dispatcher {
  protected implicit def ec:ExecutionContext
}

trait ContextDispatcher extends Dispatcher { this:Actor ⇒
  protected implicit lazy val ec:ExecutionContext = context.dispatcher
}

trait SystemDispatcher extends Dispatcher with ActorSystemMembership {
  protected implicit lazy val ec:ExecutionContext = system.dispatcher
}

trait ActorSystemMembership {
  protected implicit def system:ActorSystem
}

