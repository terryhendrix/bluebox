package nl.bluesoft.bluebox.sdk
import java.util.concurrent.TimeUnit
import akka.event.LoggingAdapter
import scala.concurrent.duration.FiniteDuration
import scala.reflect.ClassTag

trait Command {
  def log:LoggingAdapter
  def exec():Unit
}

trait Event {
  def log:LoggingAdapter
  var lastTrigger:Long = 0

  /**
   * Defines how much time there must be in between 2 'Trigger'
   * @return
   */
  def minimumTimeInBetween:FiniteDuration

  final def hasTriggered:Boolean = {
    if(lastTrigger == 0 || FiniteDuration(System.currentTimeMillis() - lastTrigger, TimeUnit.MILLISECONDS) > minimumTimeInBetween) {
      val triggered = trigger()
      if(triggered) {
        lastTrigger = System.currentTimeMillis()
        log.info(s"${this.getClass.getSimpleName} has triggered")
      }
      triggered
    }
    else {
      log.info(s"${this.getClass.getSimpleName} will be checked again in ${(minimumTimeInBetween - FiniteDuration(System.currentTimeMillis() - lastTrigger, TimeUnit.MILLISECONDS)).toMinutes} minutes")
      false
    }
  }
  
  protected def trigger():Boolean
}

trait VariableOutput[U <: ClassTag[U]] {
  this:Event ⇒
  def output:U
}

case class Schedule(trigger:Event, command:Command)