package nl.bluesoft.bluebox
import akka.actor._
import akka.event.LoggingAdapter
import akka.kernel.Bootable
import nl.bluesoft.bluebox.core.BlueBox

trait Boot extends SplashScreen
{
  def start() = {
    val system = ActorSystem("BlueBox")
    require(system.settings.config.getString("clustering.cluster.name") == "BlueBox", "The config key clustering.cluster.name must be BlueBox")

    splash(system.log)
    BlueBox(system)
  }
}

object AppBoot extends App with Boot {
  start()
}

class KernelBoot extends Bootable with Boot
{
  override def startup(): Unit = {
    start()
  }

  override def shutdown(): Unit = {

  }
}


trait SplashScreen {
  def splash(logging: LoggingAdapter): Unit = {
    logging.info(
      """
        |
        | ______         _     _ _______ ______   _____  _     _
        | |_____] |      |     | |______ |_____] |     |  \___/
        | |_____] |_____ |_____| |______ |_____] |_____| _/   \_
        |
      """.stripMargin)
  }
}


