package nl.bluesoft.bluebox.audio.core

import akka.actor.{ActorRef, Props}
import akka.contrib.pattern.ClusterSharding
import akka.contrib.pattern.ShardRegion.{EntryId, Msg}
import nl.bluesoft.bluebox.audio.es.PlaylistIndex
import nl.bluesoft.bluebox.audio.model._
import nl.bluesoft.bluebox.sdk.ActorSystemMembership
import nl.bluesoft.peanuts.actor
import nl.bluesoft.peanuts.actor.ShardActor


object Playlists extends actor.ShardActorBootstrap
{
  // public
  case class CreatePlaylist(name:String)
  case class RenamePlaylist(oldName:String, newName:String)
  case class DeletePlaylist(name:String)
  case class AddSong(name:String, song:Song, after:Option[Song] = None)
  case class MoveSong(name:String, song:Song, after:Option[Song] = None)
  case class RemoveSong(name:String, song:Song)

  // internal
  sealed case class MigrateHashes(name:String, songs:Seq[String])

  // internal event
  case object PlaylistDeleted

  override implicit def shardMessages: PartialFunction[Msg, (EntryId, Msg)] = {
    case msg @ CreatePlaylist(id)         ⇒ (id, msg)
    case msg @ RenamePlaylist(id, _)      ⇒ (id, msg)
    case msg @ DeletePlaylist(id)         ⇒ (id, msg)
    case msg @ AddSong(id,_,_)            ⇒ (id, msg)
    case msg @ MoveSong(id,_,_)           ⇒ (id, msg)
    case msg @ RemoveSong(id,_)           ⇒ (id, msg)
    case msg @ MigrateHashes(id,_)        ⇒ (id, msg)
  }

  override def shardName: String = "Playlists"

  override def props: Props = Props(new Playlists)
}

class Playlists extends ShardActor[HashPlaylist] with PlaylistsRegion
{
  import nl.bluesoft.bluebox.audio.core.Playlists._
  import nl.bluesoft.peanuts.Convert.stringToFiniteDuration

  override val system = context.system
  val index = new PlaylistIndex(system, log)
  override def persistenceId: String = shardKey
  context.setReceiveTimeout(context.system.settings.config.getString("bluebox.timeouts.playlists"))

  enable event sourcing
  make snapshots every(20 events)

  override protected def events: Receive = {
    case msg:CreatePlaylist ⇒
      withoutState {
        log.info("Creating playlist")
        set stateTo HashPlaylist(msg.name)
      }

      withState {
        case state if state.active ⇒
          log.warning("Cannot create playlist. It already exists")
        case state if !state.active ⇒
          log.info("Creating playlist")
          set stateTo HashPlaylist(msg.name)
      }

    case PlaylistDeleted ⇒
      withState { state ⇒
        log.info("Deleting playlist")
        set stateTo state.copy(active = false)
      }

    case msg:AddSong ⇒
      withState { state ⇒
        log.info(s"Adding ${msg.song.fileName} ${msg.after.map(s ⇒ s"after ${s.fileName}").getOrElse("at the beginning")}")
        set stateTo state.addSong(msg.song, msg.after)
      }

    case msg:RemoveSong ⇒
      withState { state ⇒
        log.info(s"Removing ${msg.song.fileName}")
        set stateTo state.removeSong(msg.song)
      }

    case msg:MigrateHashes ⇒
      withState { state ⇒
        log.info(s"Migrating ${msg.songs.size} hashes")
        set stateTo state.copy(hashes = msg.songs)
      }
  }

  override protected def commands: Receive = {

    case msg:RenamePlaylist ⇒
      withState { pl ⇒
        log.info(s"Renaming playlist ${msg.oldName} to ${msg.newName}")
        playlists ! CreatePlaylist(msg.newName)
        playlists ! MigrateHashes(msg.newName, pl.hashes)
        self ! PlaylistDeleted
      }

    case msg:DeletePlaylist ⇒
      self ! PlaylistDeleted
      sender ! PlaylistDeleted

    case msg:MoveSong ⇒
      log.info(s"Moving ${msg.song.fileName} ${msg.after.map(s ⇒ s"after ${s.fileName}").getOrElse("to the beginning")}")
      self ! RemoveSong(msg.name, msg.song)
      self ! AddSong(msg.name, msg.song, msg.after)

    case msg ⇒
      log.warning(s"Ignoring $msg")
  }

  override def whenStateChanged(state:Option[HashPlaylist]) = {
    whileNotRecovering {
      state.map { playlist ⇒
        log.info(s"Indexing $state")
        index.indexPlaylist(playlist).recover {
          case ex:Throwable ⇒ ex.printStackTrace()
        }
      }
    }
  }
}

trait PlaylistsRegion extends ActorSystemMembership
{
  lazy val playlists:ActorRef = ClusterSharding(system).shardRegion("Playlists")
}