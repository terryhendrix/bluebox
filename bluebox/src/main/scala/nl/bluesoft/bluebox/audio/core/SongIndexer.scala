package nl.bluesoft.bluebox.audio.core
import java.io.FileInputStream
import java.nio.file.{Files, Path, Paths}
import java.util.UUID

import akka.actor._
import akka.stream._
import akka.stream.actor.ActorPublisher
import akka.stream.scaladsl._
import nl.bluesoft.bluebox.audio.es.SongIndex
import nl.bluesoft.bluebox.audio.model.Song
import nl.bluesoft.bluebox.audio.core.SongIndexer._
import nl.bluesoft.bluebox.util.files.FileManager.DirectoryUpdated
import nl.bluesoft.peanuts.actor.streams.{FilePublisher, StreamDelay}
import nl.bluesoft.peanuts.util.{Algorithms, Hash}

import scala.concurrent.Future
import scala.concurrent.duration._

class SongPublisher(directory:Path, recursive:Boolean = true) extends FilePublisher(directory, recursive)
{
  override val extensions:Seq[FileExtension] = Seq("mp3", "flac", "wav", "m4a", "wma")
}

object SongIndexer
{
  def props() = Props(new SongIndexer)
  case class IndexMusicDirectory(directory: String, force:Boolean = false, recursive:Boolean = true)
  sealed case class Done(directory: String, sender:ActorRef, stats:Stats)
  case class MusicDirectoryIndexed(directory: String, stats:Stats)


  type Action = String
  object Actions {
    val Update:Action = "update"
    val Create:Action = "create"
    val Skip:Action   = "skip"
  }

  case class Stats(total:Int = 0, failed:Int = 0, skipped:Int = 0, created:Int = 0, updated:Int = 0) {
    override def toString = s"total=$total failed=$failed skipped=$skipped created=$created updated=$updated"
  }
}
/**
 * The song indexer reads files from a directory in a streaming based fashion.
 * When it's idle
 */
class SongIndexer extends Actor with ActorLogging with StreamDelay with SongIndexInvalidator
{
  implicit val _system = context.system
  implicit val ec = context.system.dispatcher
  implicit val mat = ActorMaterializer()
  implicit override val log = super.log
  val index = new SongIndex(context.system, log)
  context.setReceiveTimeout(1 minute)

  val filter = Flow[Path].mapAsync[Option[Path]](1) { path ⇒
    Future(new FileInputStream(path.toAbsolutePath.toString)).flatMap { s ⇒
      s.close()
      val hash = Hash.string(path.toAbsolutePath.toString, Algorithms.MD5)
      index.getByPathHash(hash).map { res ⇒
        if(res.isEmpty)
          log.info(s"Could not find indexed song ${path.toAbsolutePath.toString}")
        res.exists(_.fileName == path.getFileName.toString) match {
          case true   ⇒ None
          case false  ⇒ Some(path)
        }
      }
    }.recover { case ex:Throwable ⇒
      log.error(s"Encountered ${ex.getClass.getSimpleName}:  ${ex.getMessage}")
      None
    }
  }

  val hasher = Flow[Option[Path]].map[Option[Song]] { maybePath ⇒
    maybePath.map { path ⇒
      Song(
        path = path.getParent.toAbsolutePath.toString,
        fileName = path.getFileName.toString,
        songHash = Hash.file(path, Algorithms.MD5),
        pathHash = Hash.string(path.toAbsolutePath.toString, Algorithms.MD5)
      )
    }
  }

  val decider = Flow[Option[Song]].mapAsync(1) { maybeSong ⇒
    maybeSong.map { song ⇒
      index.getSong(song.songHash).map {
        case indexed if indexed.nonEmpty && indexed.exists(_.filePath == song.filePath) ⇒
          (Some(song), indexed, SongIndexer.Actions.Skip)
        case indexed if indexed.nonEmpty ⇒
          (Some(song), indexed, SongIndexer.Actions.Update)
        case indexed ⇒
          (Some(song), Seq.empty, SongIndexer.Actions.Create)
      }
    }.getOrElse(Future((None, Seq.empty, SongIndexer.Actions.Skip)))
  }

  val indexer = Flow[(Option[Song], Seq[Song], SongIndexer.Action)].mapAsync(1) {
    case (Some(song), _, action @ Actions.Create) ⇒
      log.info(s"Creating ${song.filePath}")
        index.indexSong(song.copy(clipInfo = Some(MPlayer.clipInfo(song)))).map(_ ⇒ (Some(song), action, None))
    case (Some(song), indexed, action @ Actions.Update) ⇒
      log.info(s"Updating ${song.filePath}")
      index.indexSong(
        indexed.find(_.clipInfo.isDefined)
          .map(indexedWithClipInfo ⇒ song.copy(clipInfo = indexedWithClipInfo.clipInfo))
          .getOrElse(song.copy(clipInfo = Some(MPlayer.clipInfo(song)))))
        .flatMap { _ ⇒
          Future.sequence(
            indexed.map { indexedSong ⇒
              if(Files.exists(Paths.get(indexedSong.filePath)))
                Future(Unit)
              else {
                log.info(s"Removing ${indexedSong.filePath} from the index as it no longer appears on the hard drive")
                index.indexSong(indexedSong.copy(active = false))
              }
            }
          ).map { _ ⇒
            (Some(song), action, None)
          }
        }
    case (song, _, action @ Actions.Skip) ⇒
      Future((song, action, None))
    case any ⇒
      log.warning(s"Unhandled $any in phase indexer")
      Future((None, Actions.Skip, None))
  }

  val counter = Sink.fold[Stats, (Option[Song], SongIndexer.Action, Option[Throwable])](Stats()) { (stats, b) ⇒
    val statsWithAction = b match {
      case (_, _, Some(ex)) ⇒
        stats.copy(failed = stats.failed + 1)
      case (_, Actions.Create, _) ⇒
        stats.copy(created = stats.created + 1)
      case (_, Actions.Update, _) ⇒
        stats.copy(updated = stats.updated + 1)
      case (_, Actions.Skip, _) ⇒
        stats.copy(skipped = stats.skipped + 1)
      case _ ⇒ stats
    }

    statsWithAction.copy(total = stats.total + 1)
  }

  def indexPipeline(directory: String, recursive:Boolean) = {
    val publisher = ActorPublisher[Path](context.system.actorOf(Props(new SongPublisher(Paths.get(directory), recursive)), s"${UUID.randomUUID()}"))
    Source[Path](publisher)
      .via(filter)
      .via(hasher)
      .via(decider)
      .via(indexer)
  }


  override def receive: Receive = idle

  def idle: Receive = {
    case msg @ IndexMusicDirectory(directory, _, recursive) ⇒
      log.info(s"Indexing $directory")
      context.become(busy)
      val ref = sender()
      context.setReceiveTimeout(Duration.Inf)
      context.system.eventStream.publish(msg)
      indexPipeline(directory, recursive).runWith(counter).map { stats ⇒
        log.info(s"Completed indexing of $directory. Result: $stats")
        removeInvalidEntries().map { _ ⇒
          self ! Done(directory, ref, stats)
          log.info(s"Completed invalidating songs that no longer exist.")
        }
      }.recover {
        case ex: Throwable ⇒
          self ! Done(directory, ref, Stats())
          ex.printStackTrace()
      }
    case ReceiveTimeout ⇒
      self ! PoisonPill
    case any ⇒
      log.debug(s"Unhandled: $any")
  }

  def busy: Receive = {
    case Done(directory, ref, stats) ⇒
      ref ! MusicDirectoryIndexed(directory, stats)
      context.become(idle)
      context.setReceiveTimeout(1 minute)
      context.system.eventStream.publish(MusicDirectoryIndexed(directory, stats))
      context.system.eventStream.publish(DirectoryUpdated(Paths.get(directory).getParent.toAbsolutePath))
    case msg @ IndexMusicDirectory(directory, true, _) ⇒
      log.info(s"Rescheduling indexing of $directory")
      context.system.scheduler.scheduleOnce(1 second, self, msg)
    case any ⇒
      log.warning(s"Unhandled: $any")
  }
}
