package nl.bluesoft.bluebox.audio.view

import akka.actor._
import nl.bluesoft.bluebox.audio.core.AudioGroup._
import nl.bluesoft.bluebox.audio.core.AudioPlayer.{SongStarted, SongStopped}
import nl.bluesoft.bluebox.audio.core.SongIndexer.{IndexMusicDirectory, MusicDirectoryIndexed}
import nl.bluesoft.bluebox.audio.core.VolumeManager.VolumeSet
import nl.bluesoft.bluebox.audio.model.{Song, SongProgress}
import nl.bluesoft.bluebox.audio.view.AudioView.{GetCurrentlyPlaying, GetSongProgress, IsPlaying}
import nl.bluesoft.bluebox.util.files.FileManager.DirectoryUpdated

object AudioView extends AudioUpdatesJsonProtocol
{
  def props = Props(new AudioView)
  case object GetSongProgress
  case object GetCurrentlyPlaying
  case object IsPlaying
  case object NotPlaying
}

class AudioView() extends Actor with AudioEventConsumer
{
  var currentlyPlaying:Option[Song] = None
  var isPlaying = false
  var songProgress:SongProgress = SongProgress(0,0)

  def receive = {
    case GetSongProgress ⇒
      sender ! songProgress

    case GetCurrentlyPlaying ⇒
      sender ! currentlyPlaying

    case IsPlaying ⇒
      sender ! isPlaying

    case msg:SongProgress ⇒
      songProgress = msg
      
    case msg:SongStarted ⇒
      isPlaying = true
      currentlyPlaying = Some(msg.song)
      
    case msg:SongStopped ⇒
      isPlaying = false

    case msg:VolumeSet ⇒
    case msg:DirectoryUpdated ⇒
    case msg:IndexMusicDirectory ⇒
    case msg:MusicDirectoryIndexed ⇒
    case msg:PlayModeSwitched ⇒
    case msg:PlaylistStarted ⇒
    case msg:PlaylistUpdated ⇒
    case msg:PlaylistResumed ⇒
    case msg:PlaylistPaused ⇒

    case msg ⇒
      log.warning(s"Unhandled event $msg, not publishing.")
  }
}