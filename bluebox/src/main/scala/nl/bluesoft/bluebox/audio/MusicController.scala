package nl.bluesoft.bluebox.audio
import java.nio.file.{DirectoryStream, Files, Path}

import akka.pattern.ask
import akka.stream.actor.ActorPublisher
import akka.stream.scaladsl.Source
import akka.util.Timeout
import nl.bluesoft.bluebox.audio.core.AudioGroup
import nl.bluesoft.bluebox.audio.core.AudioGroup.PlayModes._
import nl.bluesoft.bluebox.audio.model.{Directory, Song, SongProgress}
import nl.bluesoft.bluebox.audio.view.{AudioView, AudioWebsocket}
import nl.bluesoft.bluebox.sdk.SystemDispatcher
import nl.bluesoft.bluebox.util.{BlueBoxException, ErrorCodes}

import scala.collection.JavaConversions._
import scala.concurrent.Future

trait MusicController extends IndexDef with ActorDef with SystemDispatcher
{ this: AudioApp ⇒

  implicit def timeout:Timeout

  override def getSongs(directory: Path): Future[Seq[Song]] = songIndex.getFromDirectory(directory)

  override def getSongs: Future[Seq[Song]] = songIndex.getSongs

  override def getSong(hash: String): Future[Song] = {
    songIndex.getSong(hash).map {
      case songs if songs.nonEmpty ⇒ songs.head
      case songs if songs.isEmpty ⇒ throw new BlueBoxException(ErrorCodes.NotFound, s"Cannot find song for hash ${hashCode()}")}
  }

  override def getDirectories(directory: Path): Future[Seq[Directory]] = {
    def addNameToDirectory(dir:Directory): Directory = {
      dir.copy(name = Some(dir.path.replace(directory.toAbsolutePath.toString, "").replaceAll("[\\/$|^\\/]","")))
    }

    Future {
      val dirs = Files.newDirectoryStream(directory, new DirectoryStream.Filter[Path]() {
        override def accept(path: Path): Boolean = { Files.isDirectory(path) }
      }).iterator().toSeq.map { path ⇒ Directory(path.toAbsolutePath.toString)}.map(addNameToDirectory) match {
        case mapped if directory == musicRootDirectory ⇒ mapped
        case mapped ⇒  Directory(directory.toAbsolutePath.toString.split("\\/").dropRight(1).mkString("/"), Some("..")) +: mapped
      }

      dirs.sortBy(_.name)
    }
  }

  override def play(): Future[Unit] = {
    (audioManager ? AudioGroup.StartPlaylist).map {
      case AudioGroup.PlaylistStarted(_) ⇒
    }
  }

  override def pause(): Future[Unit] = {
    (audioManager ? AudioGroup.PausePlaylist).map {
      case AudioGroup.PlaylistPaused(_) ⇒
    }
  }

  override def resume(): Future[Unit] = {
    (audioManager ? AudioGroup.ResumePlaylist).map {
      case AudioGroup.PlaylistResumed(_) ⇒
    }
  }

  override def goto(song:Song):Future[Unit] = {
    (audioManager ? AudioGroup.SkipToSong(song)).map {
      case AudioGroup.SkippedToSong(_,_) ⇒
    }
  }

  override def previous():Future[Unit] = {
    (audioManager ? AudioGroup.PlayPreviousSong).map {
      case AudioGroup.SkippedToSong(_,_) ⇒
    }
  }

  override def next():Future[Unit] = {
    (audioManager ? AudioGroup.PlayNextSong).map {
      case AudioGroup.SkippedToSong(_,_) ⇒
    }
  }

  override def setPlayMode(mode: PlayMode): Future[Unit] = {
    (audioManager ? AudioGroup.SwitchPlayMode(mode)).map {
      case AudioGroup.PlayModeSwitched(`mode`) ⇒
      case AudioGroup.PlayModeSwitched(other) ⇒
        throw new Exception(s"Trying switching to $mode but is $other")
    }
  }

  override def getSongProgress(): Future[SongProgress] = {
    (view ? AudioView.GetSongProgress).mapTo[SongProgress]
  }

  override def activeSong(): Future[Option[Song]] = {
    (view ? AudioView.GetCurrentlyPlaying).mapTo[Option[Song]]
  }

  override def isPlaying(): Future[Boolean] = {
    (view ? AudioView.IsPlaying).mapTo[Boolean]
  }

  override def searchSongs(query: String): Future[AudioApp.SearchResults] = {
    val queries = query.replaceAll("\\s+", " ").trim.split(" ")
    Future.sequence(queries.toSeq.map { q ⇒
      songIndex.searchSongs(q).map(songs ⇒ (q, songs))
    }).map(_.toMap).map { results ⇒
      results.foldLeft(Seq.empty[Song]) {
        case (acc, partialResult) if acc.isEmpty ⇒
          partialResult._2
        case (acc, partialResult) if acc.nonEmpty ⇒
          acc.intersect(partialResult._2)
      }
    }.flatMap { songs ⇒
      songIndex.getCompletions(query).map { completion ⇒
        AudioApp.SearchResults(
          songs = songs.groupBy(s ⇒ s.clipInfo.flatMap(_.artist).getOrElse("Unknown")),
          completions = completion.map { res ⇒
            res.title → res.suggestions.flatMap(_.options).map(_.text).filter(_.trim != query.trim)
          }.toMap)
      }
    }
  }

  override def audioPublisher:Source[String, Unit] = {
    val webSocket = system.actorOf(AudioWebsocket.props)
    audioManager ! AudioGroup.InitClient(webSocket)
    volumeManager ! AudioGroup.InitClient(webSocket)
    Source(ActorPublisher[String](webSocket))
  }
}
