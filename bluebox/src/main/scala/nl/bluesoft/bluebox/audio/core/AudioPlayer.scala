package nl.bluesoft.bluebox.audio.core
import java.io.File
import java.nio.file._

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import nl.bluesoft.bluebox.audio.core.AudioGroup.RegisterPlayer
import nl.bluesoft.bluebox.audio.core.AudioPlayer._
import nl.bluesoft.bluebox.audio.model.{ManagedPlayer, Song, SongProgress}
import nl.bluesoft.bluebox.core.BlueBox
import nl.bluesoft.bluebox.util.files.Delete

import scala.sys.process._
import scala.util.Try

object AudioPlayer {
  def props(manager:ActorRef, name:String) = Props(new AudioPlayer(manager, name))

  case class StartSong(song:Song, progress:Double)
  case class SongStarted(song:Song)
  
  case object StopSong
  case class SongStopped(song:Song)
  case object NotPlaying

  case class SongFinished(song:Song)
  case class CannotPlaySong(song:Song, currentlyPlaying:Song, progress:Double)
  case class SongProgressed(song:Song, progress:SongProgress)

  case class UploadSong(song:Song, bytes:Array[Byte])
  case class SongUploaded(song:Song)

  case class DoesSongExist(song:Song)
  case class SongExists(song:Song, exists:Boolean)

  type Status = String
  object Status {
    val playing = "playing"
    val idle = "idle"
  }
}

class AudioPlayer(manager:ActorRef, name:String) extends Actor with ActorLogging
{
  override implicit val log = super.log
  implicit val ec = context.system.dispatcher
  lazy val mPlayerCacheDir = BlueBox(context.system).settings.directory("mplayer-cache")
  var currentSong:Option[Song] = None
  val maxCache = context.system.settings.config.getInt("bluebox.directories.mplayer-cache-size-in-megabyte")

  manager ! RegisterPlayer(ManagedPlayer(name, self, None, None, Status.idle))

  override def preStart() = {
    log.info(s"Flushing the cache at $mPlayerCacheDir")
    Try(Delete.deleteRecursive(mPlayerCacheDir)).recover { case ex ⇒
      log.error(s"{} while flusing cache at $mPlayerCacheDir: {}", ex.getClass.getSimpleName, ex.getMessage)
    }
  }

  override def receive: Receive = {
    case AudioPlayer.StartSong(song, progress) ⇒
      currentSong match {
        case None ⇒
          currentSong = Some(song)
          sender ! SongStarted(song)
          log.info(s"Attempting to play file from storage: ${storedFileName(song)}")
          MPlayer.playFile(storedFileName(song), progress) { progress ⇒
            manager ! SongProgressed(song, progress)
          }.map { _ ⇒
            self ! SongFinished(song)
            manager ! SongFinished(song)
          }

        case Some(current) ⇒
          sender ! CannotPlaySong(song, current, progress)
      }

    case AudioPlayer.StopSong ⇒
      currentSong match  {
        case Some(active) ⇒
          MPlayer.stop
          currentSong = None
          shrinkCache()
          sender ! SongStopped(active)
        case None ⇒
          sender ! NotPlaying
      }

    case AudioPlayer.SongFinished(song) ⇒
      currentSong = None
      shrinkCache()

    case UploadSong(song, bytes) ⇒
      cacheSong(song, bytes)
      sender ! SongUploaded(song)

    case DoesSongExist(song) ⇒
      log.info(s"Checking if ${song.fileName} exists in the mplayer cache.")
      sender ! SongExists(song, Files.exists(Paths.get(storedFileName(song))))
  }

  def storedFileName(song:Song):String = {
    val ext = song.fileName.split("\\.").toSeq.last
    mPlayerCacheDir + "/" + song.songHash + "." + ext
  }

  /**
   * A mutable java implementation to get the files in the cache sorted by their date
   * @return
   */
  def recursiveListFiles(f: File): Array[File] = {
    val these = f.listFiles
    these ++ these.filter(_.isDirectory).flatMap(recursiveListFiles)
  }

  def shrinkCache():Unit = {
    val sizeUsedInMegs = s"du -h $mPlayerCacheDir".!!
      .replace(s"$mPlayerCacheDir", "")
      .trim
      .replaceAll("G","000M")
      .replaceAll("M","")
      .replaceAll("\"","")
      .toInt
    log.info(s"Cache is using ${sizeUsedInMegs}MB")
    if(sizeUsedInMegs > maxCache) {
      val files = recursiveListFiles(new File(mPlayerCacheDir.toAbsolutePath.toString)).sortBy(_.lastModified)
      if(files.length > 3) {  // Don't remove the last files, we need it for playing!
        files.headOption.foreach { file ⇒
          log.info(s"Shrinking cache, removing ${file.getAbsolutePath}.")
          Files.delete(Paths.get(file.getAbsolutePath))
          shrinkCache()
        }
      }
    }
  }
  
  def cacheSong(song:Song, bytes:Array[Byte]) = {
    Files.createDirectories(mPlayerCacheDir)

    val location = Paths.get(storedFileName(song))

    if(!Files.exists(location)) {
      log.info(s"Writing song to $location")
      Files.write(location, bytes)
    }
    else {
      log.info(s"Not writing song to $location, file exists in the cache.")
    }
  }
}
