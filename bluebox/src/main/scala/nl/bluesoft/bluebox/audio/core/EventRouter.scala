package nl.bluesoft.bluebox.audio.core
import akka.actor.{Props, ActorSystem, Actor, ActorLogging}
import nl.bluesoft.bluebox.audio.core.AudioGroup.SkippedToSong
import nl.bluesoft.bluebox.audio.core.AudioPlayer.{SongFinished, SongStarted}
import nl.bluesoft.bluebox.audio.core.SongStats.SkippedSong

object EventRouter {
  def props = Props(new EventRouter)
}

class EventRouter extends Actor with ActorLogging with SongStatsRegion
{
  override def system: ActorSystem = context.system

  override def preStart() = {
    context.system.eventStream.subscribe(self, classOf[SongStarted])
    context.system.eventStream.subscribe(self, classOf[SongFinished])
    context.system.eventStream.subscribe(self, classOf[SkippedToSong])
  }

  override def postStop = {
    context.system.eventStream.unsubscribe(self)
  }

  override def receive: Receive = {
    case msg:SongStarted ⇒
      songStats ! msg
    case msg:SongFinished ⇒
      songStats ! msg
    case msg:SkippedToSong ⇒
      songStats ! msg
      msg.wasPlaying.foreach(song ⇒ songStats ! SkippedSong(song))
  }
}
