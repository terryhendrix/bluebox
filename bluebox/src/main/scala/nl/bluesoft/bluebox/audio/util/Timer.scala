package nl.bluesoft.bluebox.audio.util
import java.util.concurrent.TimeUnit

import scala.concurrent.duration.Duration

case class Timer(description:String, logger: String ⇒ Unit = println)
{
  val start = System.currentTimeMillis()
  def print = {
    val d = Duration(System.currentTimeMillis() - start, TimeUnit.MILLISECONDS)
    logger(s"$description took $d")
  }
}
