package nl.bluesoft.bluebox.audio.core
import akka.actor.{Actor, ActorLogging, Props}
import nl.bluesoft.bluebox.audio.core.AudioGroup.InitClient
import nl.bluesoft.bluebox.audio.core.VolumeManager._
import nl.bluesoft.bluebox.audio.model.Volume
import nl.bluesoft.peanuts.util.OperatingSystem

object VolumeManager {
  def props = Props(new VolumeManager)

  case object GetVolume
  case class SetVolume(volume:Volume)
  case object ResetVolume
  case object Mute
  case object Unmute

  case class VolumeSet(volume:Volume, muted:Boolean = false)
}

class VolumeManager extends Actor with ActorLogging
{
  var volume = Volume(70)
  val os = OperatingSystem()

  os.setVolume(volume.volume)
  context.system.eventStream.publish(VolumeSet(volume))

  override def receive: Receive = {
    case ResetVolume ⇒
      log.info(s"Setting volume to ${volume.volume}")
      os.setVolume(volume.volume)
      context.system.eventStream.publish(VolumeSet(volume))

    case SetVolume(vol) ⇒
      log.info(s"Setting volume to ${vol.volume}")
      volume = vol
      os.setVolume(volume.volume)
      context.system.eventStream.publish(VolumeSet(volume))
      sender ! VolumeSet(volume)

    case Mute ⇒
      log.info(s"Muting sound")
      os.setVolume(0)
      context.system.eventStream.publish(VolumeSet(volume, muted = true))
      sender ! VolumeSet(volume, muted = true)

    case GetVolume ⇒
      sender ! volume

    case Unmute ⇒
      log.info(s"Unmuting sound. Setting volume to ${volume.volume}")
      os.setVolume(volume.volume)
      context.system.eventStream.publish(VolumeSet(volume))
      sender ! VolumeSet(volume)

    case InitClient(client) ⇒
      log.info("Initializing client")
      client ! VolumeSet(volume)
  }
}
