package nl.bluesoft.bluebox.audio
import java.nio.file.Path

import akka.pattern.ask
import akka.util.Timeout
import nl.bluesoft.bluebox.core.BlueBox
import nl.bluesoft.bluebox.sdk.SystemDispatcher
import nl.bluesoft.bluebox.util.files.FileManager
import nl.bluesoft.bluebox.util.{BlueBoxException, ErrorCodes}

import scala.concurrent.Future

trait FilesController extends ActorDef with SystemDispatcher
{ this:AudioApp ⇒

  implicit def timeout:Timeout

  def settings:BlueBox.Settings


  implicit class QuestionHandling[U](f:Future[U]) {
    def waitForCompleted:Future[Unit] = {
      f.map {
        case FileManager.Completed(_, None) ⇒
        case FileManager.Completed(_, Some(ex)) ⇒ throw ex
        case FileManager.Busy(msg)   ⇒ throw new BlueBoxException(ErrorCodes.Conflict, s"$msg is still in progress.")
      }
    }
  }

  override def createDirectory(dir: Path): Future[Unit] = {
    implicit val timeout = Timeout(settings.timeout("file-transfer"))
    (files ? FileManager.CreateDirectory(dir)).waitForCompleted
  }

  override def copyDirectory(from: Path, dir: Path): Future[Unit] = {
    implicit val timeout = Timeout(settings.timeout("file-transfer"))
    (files ? FileManager.CopyDirectory(from, dir)).waitForCompleted
  }

  override def deleteDirectory(dir: Path, force: Boolean): Future[Unit] = {
    implicit val timeout = Timeout(settings.timeout("file-transfer"))
    (files ? FileManager.DeleteDirectory(dir, force)).waitForCompleted
  }

  override def moveDirectory(from: Path, dir: Path): Future[Unit] = {
    implicit val timeout = Timeout(settings.timeout("file-transfer"))
    (files ? FileManager.MoveDirectory(from, dir)).waitForCompleted
  }

  override def copySong(from: Path, dir: Path): Future[Unit] = {
    implicit val timeout = Timeout(settings.timeout("file-transfer"))
    (files ? FileManager.CopyFile(from, dir)).waitForCompleted
  }

  override def moveSong(from: Path, dir: Path): Future[Unit] = {
    implicit val timeout = Timeout(settings.timeout("file-transfer"))
    (files ? FileManager.MoveFile(from, dir)).waitForCompleted
  }

  override def deleteSong(path: Path): Future[Unit] = {
    implicit val timeout = Timeout(settings.timeout("file-transfer"))
    (files ? FileManager.DeleteFile(path)).waitForCompleted
  }
}
