package nl.bluesoft.bluebox.audio.view

import akka.actor.{Actor, ActorLogging, PoisonPill}
import nl.bluesoft.bluebox.audio.core.AudioGroup._
import nl.bluesoft.bluebox.audio.core.AudioPlayer.{SongStarted, SongStopped}
import nl.bluesoft.bluebox.audio.core.SongIndexer.{IndexMusicDirectory, MusicDirectoryIndexed}
import nl.bluesoft.bluebox.audio.core.VolumeManager.VolumeSet
import nl.bluesoft.bluebox.audio.model.SongProgress
import nl.bluesoft.bluebox.audio.util.AudioJsonProtocol
import nl.bluesoft.bluebox.audio.view.AudioEventConsumer.UpdateEvent
import nl.bluesoft.bluebox.util.files.FileManager.DirectoryUpdated
import nl.bluesoft.peanuts.json.JsonMarshalling

trait AudioUpdatesJsonProtocol extends AudioJsonProtocol {
  implicit val playlistStarted        = jsonFormat1(PlaylistStarted)
  implicit val playlistPaused         = jsonFormat1(PlaylistPaused)
  implicit val playlistUpdated        = jsonFormat1(PlaylistUpdated)
  implicit val playlistResumed        = jsonFormat1(PlaylistResumed)
  implicit val songStarted            = jsonFormat1(SongStarted)
  implicit val songStopped            = jsonFormat1(SongStopped)
  implicit val volumeSet              = jsonFormat2(VolumeSet)
  implicit val directoryUpdated       = jsonFormat1(DirectoryUpdated)
  implicit val musicDirectoryIndexed  = jsonFormat3(IndexMusicDirectory)
  implicit val indexMusicDirectory    = jsonFormat2(MusicDirectoryIndexed)
  implicit val playModeSwitched       = jsonFormat1(PlayModeSwitched)

  implicit val playlistStartedEvent   = jsonFormat2(UpdateEvent[PlaylistStarted])
  implicit val playlistPausedEvent    = jsonFormat2(UpdateEvent[PlaylistPaused])
  implicit val playlistUpdatedEvent   = jsonFormat2(UpdateEvent[PlaylistUpdated])
  implicit val playlistResumedEvent   = jsonFormat2(UpdateEvent[PlaylistResumed])
  implicit val progressEvent          = jsonFormat2(UpdateEvent[SongProgress])
  implicit val songStartedEvent       = jsonFormat2(UpdateEvent[SongStarted])
  implicit val songStoppedEvent       = jsonFormat2(UpdateEvent[SongStopped])
  implicit val volumeSetEvent         = jsonFormat2(UpdateEvent[VolumeSet])
  implicit val dirUpdatedEvent        = jsonFormat2(UpdateEvent[DirectoryUpdated])
  implicit val musicDirIndexedEvent   = jsonFormat2(UpdateEvent[IndexMusicDirectory])
  implicit val indexMusicDirEvent     = jsonFormat2(UpdateEvent[MusicDirectoryIndexed])
  implicit val playModeSwitchedEvent  = jsonFormat2(UpdateEvent[PlayModeSwitched])
}

object AudioEventConsumer {
  case class UpdateEvent[M](id:String, message:M)
}

trait AudioEventConsumer extends ActorLogging with JsonMarshalling with AudioUpdatesJsonProtocol
{
  this:Actor ⇒

  implicit val ec = context.system.dispatcher

  override def preStart():Unit = {
    log.info("Creating websocket")
    context.system.eventStream.subscribe(self, classOf[PlaylistStarted])
    context.system.eventStream.subscribe(self, classOf[PlaylistUpdated])
    context.system.eventStream.subscribe(self, classOf[PlaylistResumed])
    context.system.eventStream.subscribe(self, classOf[PlaylistPaused])
    context.system.eventStream.subscribe(self, classOf[SongProgress])
    context.system.eventStream.subscribe(self, classOf[SongStarted])
    context.system.eventStream.subscribe(self, classOf[SongStopped])
    context.system.eventStream.subscribe(self, classOf[VolumeSet])
    context.system.eventStream.subscribe(self, classOf[DirectoryUpdated])
    context.system.eventStream.subscribe(self, classOf[IndexMusicDirectory])
    context.system.eventStream.subscribe(self, classOf[MusicDirectoryIndexed])
    context.system.eventStream.subscribe(self, classOf[PlayModeSwitched])
  }

  override def postStop():Unit = {
    log.info("Closed websocket")
    context.system.eventStream.unsubscribe(self)
    self ! PoisonPill
  }
}
