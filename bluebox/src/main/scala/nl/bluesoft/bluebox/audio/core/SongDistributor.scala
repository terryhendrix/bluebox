package nl.bluesoft.bluebox.audio.core
import java.nio.file.{Files, Paths}

import akka.actor.{Actor, ActorLogging, ActorRef, PoisonPill}
import akka.pattern.ask
import akka.util.Timeout
import nl.bluesoft.bluebox.audio.core.AudioPlayer.{SongExists, SongUploaded}
import nl.bluesoft.bluebox.audio.core.SongDistributor.{Distribute, Distributed, Done}
import nl.bluesoft.bluebox.audio.model.{ManagedPlayer, Song}
import nl.bluesoft.bluebox.audio.util.Timer
import nl.bluesoft.bluebox.sdk.ContextDispatcher

import scala.concurrent.Future
import scala.concurrent.duration._

object SongDistributor {
  sealed case class Done(song:Song)
  case class Distribute(song:Song, players:Seq[ManagedPlayer])
  case class Distributed(song:Song)
}

class SongDistributor extends Actor with ActorLogging with ContextDispatcher
{
  var replyTo:Seq[ActorRef] = Seq.empty
  implicit val timeout = Timeout(30 seconds)

  override def postStop() = {
    log.info("Terminating distributor")
  }

  override def receive: Receive = idle

  def idle:Receive = {
    case Distribute(song, players) ⇒
      val distributeTimer = Timer(s"Distributing ${song.filePath}", log.info)
      replyTo = replyTo :+ sender()
      context.become(working)
      log.info(s"Distributing ${song.fileName} as ${song.songHash}")

      Future.sequence(players.map { player ⇒
        player.ref.ask(AudioPlayer.DoesSongExist(song)).flatMap {
          case SongExists(_, true)  ⇒
            Future(log.info(s"Song ${song.fileName} already exists"))

          case SongExists(_, false) ⇒
            val readTimer = Timer(s"Reading ${song.fileName}", log.info)
            val bytes = Files.readAllBytes(Paths.get(song.filePath))
            readTimer.print

            val uploadTimer = Timer(s"Uploading ${song.fileName}", log.info)
            player.ref.ask(AudioPlayer.UploadSong(song, bytes)).map {
              case SongUploaded(s) ⇒
                uploadTimer.print
            }
        }
      }).map { _ ⇒
        distributeTimer.print
        self ! Done(song)
      }.recover { case ex ⇒
        log.error("{} while uploading song: {}", ex.getClass.getSimpleName, ex.getMessage)
        distributeTimer.print
        self ! Done(song)
      }
  }

  def working:Receive = {
    case msg: Done ⇒
      replyTo.foreach(_ ! Distributed(msg.song))
      context.become(idle)
      log.info("Done distributing")
      self ! PoisonPill

    case msg: Distribute ⇒
      log.info(s"Already handling the distribution of ${msg.song.fileName}, subscribing ${sender()}.")
      replyTo = (replyTo :+ sender()).distinct
  }
}
