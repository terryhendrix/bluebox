package nl.bluesoft.bluebox.audio.es

import akka.actor.ActorSystem
import akka.event.LoggingAdapter
import com.sksamuel.elastic4s.ElasticDsl._
import com.sksamuel.elastic4s.mappings.FieldType._
import com.sksamuel.elastic4s.mappings.TypedFieldDefinition
import nl.bluesoft.bluebox.audio.model.{HashPlaylist, Song}
import nl.bluesoft.peanuts.elasticsearch.ElasticSearch.{IndexName, SearchLimit, SearchOffset}
import nl.bluesoft.peanuts.elasticsearch.ElasticSearchIndex
import nl.bluesoft.peanuts.elasticsearch.util.ElasticSearchImplicits
import org.elasticsearch.search.sort.SortOrder
import spray.json.DefaultJsonProtocol

import scala.concurrent.ExecutionContext

class PlaylistIndex(override val system: ActorSystem, override implicit val log:LoggingAdapter)
  extends ElasticSearchIndex with DefaultJsonProtocol with ElasticSearchImplicits
{
  implicit val playlistFormat = jsonFormat3(HashPlaylist)
  override val indexName: IndexName = "playlists"
  override implicit def ec: ExecutionContext = system.dispatcher

  override def indexDefinition: Seq[TypedFieldDefinition] = Seq(
    "_id" typed StringType index NotAnalyzed,
    "name" typed StringType,
    "active" typed BooleanType,
    "hashes" typed StringType
  )

  def getPlaylists(implicit offset:SearchOffset = 0, limit:SearchLimit = 1000) = {
    query(sortBy = "name", order = SortOrder.ASC) {
      bool {
        must {
          termQuery("active", true)
        }
      }
    }.mapResults[HashPlaylist]
  }

  def getPlaylist(name: String) = {
    query(sortBy = "name", order = SortOrder.ASC) {
      bool {
        must (
          termQuery("name", name.toLowerCase)
        )
      }
    }(0,1).mapSingle[HashPlaylist]
  }

  def indexPlaylist(playlist: HashPlaylist) = {
    doIndex(id = playlist.name) (
      "name" → playlist.name,
      "active" → playlist.active,
      "hashes" → playlist.hashes
    )
  }

  def getPlaylistsWithSong(song:Song)(implicit offset:SearchOffset = 0, limit:SearchLimit = 1000) = {
    query(sortBy = "name", order = SortOrder.ASC) {
      bool {
        must {
          termQuery("hashes", song.songHash)
        }
      }
    }.mapResults[HashPlaylist]
  }
}
