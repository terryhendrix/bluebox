package nl.bluesoft.bluebox.audio.api.v1

import java.nio.file.{Path, Paths}

import akka.actor.ActorSystem
import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.model._
import akka.http.scaladsl.server._
import nl.bluesoft.bluebox.audio.AudioApp
import nl.bluesoft.bluebox.audio.model._
import nl.bluesoft.bluebox.core.api.v1.BlueBoxRestJsonProtocol
import nl.bluesoft.bluebox.sdk.AppRoute
import nl.bluesoft.peanuts.http.routes._

import scala.concurrent.ExecutionContext

object AudioRestRoute {
  case class AudioActionRequest(action: String, song:Option[Song], mode:Option[String])
  case class RatingRequest(rating: Int)
  case class MoveRequest(from:Path, to:Path, copy:Boolean)
  case class CreateDirectoryRequest(name:String, asPartOf:Option[Path])
  case class SearchRequest(query: String)
  case class CreatePlaylistRequest(name: String)
  case class RenamePlaylistRequest(name:String)
  case class PlaylistMutationRequest(hash:String, after:Option[String])
  case class ActiveSongResponse(song:Option[Song], isPlaying:Boolean)
}

trait AudioRoute extends AppRoute with AudioRouteJsonProtocol with WebSockets
{
  import AudioRestRoute._
  def audio:AudioApp

  override val name = "audio"
  override val version = "v1"

  override lazy val route: Route = {
    path("ws") {
      get {
        stringSocket(audio.audioPublisher)
      }
    } ~
    path("progress") {
      get {
        complete {
          audio.getSongProgress().mapTo[SongProgress]
        }
      }
    } ~
    path("search") {
      get {
        parameters("q".as[String]) { query ⇒
          complete {
            audio.searchSongs(query).map { res ⇒
              res.asJson
            }
          }
        }
      }
    } ~
    path("volume") {
      get {
        complete {
          audio.getVolume().map { vol ⇒
            vol.asJson
          }
        }
      } ~
      put {
        entity(as[Volume]) { volume ⇒
          complete {
            audio.setVolume(volume.volume).map { _ ⇒
              StatusCodes.Accepted
            }
          }
        }
      }
    } ~
    path("actions") {
      put {
        entity(as[AudioActionRequest]) { request ⇒
          complete {
            request match {
              case AudioActionRequest("mute", _, _)           ⇒ audio.mute()
              case AudioActionRequest("unmute", _, _)         ⇒ audio.unmute()
              case AudioActionRequest("mode", _, Some(mode))  ⇒ audio.setPlayMode(mode)
              case AudioActionRequest("play", _, _)           ⇒ audio.play()
              case AudioActionRequest("resume", _, _)         ⇒ audio.resume()
              case AudioActionRequest("pause", _, _)          ⇒ audio.pause()
              case AudioActionRequest("next", _, _)           ⇒ audio.next()
              case AudioActionRequest("previous", _, _)       ⇒ audio.previous()
              case AudioActionRequest("goto", Some(song), _)  ⇒ audio.goto(song)
              case AudioActionRequest("goto", None, _)        ⇒
                throw new Exception("When specififying the 'goto' action a 'song' must be supplied")
              case AudioActionRequest("mode", _, None)  ⇒ audio.unmute()
                throw new Exception("When specififying the 'mode' action a 'mode' must be supplied")
              case AudioActionRequest(other, _, _) ⇒
                throw new Exception(s"Unknown command '$other'")
            }
            StatusCodes.Accepted
          }
        }
      }
    } ~
    path("playlist") {
      get {
        complete {
          StatusCodes.OK -> "{ get playlist }"
        }
      } ~
      put {
        entity(as[CreatePlaylistRequest]) { request ⇒
          complete {
            audio.loadPlaylist(request.name).map { _ ⇒
              StatusCodes.NoContent
            }
          }
        }
      } ~
      delete {
        complete {
          audio.unloadPlaylist.map { _ ⇒
            StatusCodes.NoContent
          }
        }
      }
    } ~
    path("ratings") {
      get {
        complete {
          ??? // TODO: Implement
          StatusCodes.OK -> ""
        }
      }
    } ~
    path("ratings" / Segment) { hash =>
      get {
        complete {
          ??? // TODO: Implement
          StatusCodes.OK -> ""
        }
      } ~
      put {
        entity(as[RatingRequest]) { request =>
          complete {
            ??? // TODO: Implement
            StatusCodes.Accepted
          }
        }
      } ~
      post {
        entity(as[RatingRequest]) { request =>
          complete {
            ??? // TODO: Implement
            StatusCodes.Created
          }
        }
      } ~
      delete {
        complete {
          ??? // TODO: Implement
          StatusCodes.NoContent
        }
      }
    } ~
    path("directories") {
      get {
        parameters("path".as[Path] ? audio.musicRootDirectory) { path ⇒
          complete {
            audio.getDirectories(path).map { dir ⇒
              StatusCodes.OK → dir.asJson
            }
          }
        }
      } ~
      post {
        entity(as[CreateDirectoryRequest]) { request ⇒
          complete {
            val toCreate = Paths.get(request.asPartOf.getOrElse(audio.musicRootDirectory.toAbsolutePath).toString + "/" + request.name)
            audio.createDirectory(toCreate).map { _ ⇒
              StatusCodes.NoContent
            }
          }
        }
      } ~
      put {
        entity(as[MoveRequest]) { request ⇒
          complete {
            if(request.copy) {
              audio.copyDirectory(request.from, request.to).map { _ ⇒
                StatusCodes.NoContent
              }
            } else {
              audio.moveDirectory(request.from, request.to).map { _ ⇒
                StatusCodes.NoContent
              }
            }
          }
        }
      } ~
      delete {
        parameters("path".as[Path], "force".as[Boolean] ? false) { (path, force) ⇒
          complete {
            audio.deleteDirectory(path, force).map { _ ⇒
              StatusCodes.NoContent
            }
          }
        }
      }
    } ~
    path("directories" / "root") {
      get {
        complete {
          Directory(audio.musicRootDirectory.toAbsolutePath.toString).asJson
        }
      }
    } ~
    path("songs") {
      get {
        parameters("path".as[Path] ? audio.musicRootDirectory, "all".as[Boolean] ? false) { (path,all) ⇒
          complete {
            if(all) {
              audio.getSongs.map { songs ⇒
                StatusCodes.OK → songs.asJson
              }
            }
            else {
              audio.getSongs(path).map { songs ⇒
                StatusCodes.OK → songs.asJson
              }
            }
          }
        }
      } ~
      post {
        complete {
          ???   // TODO: Upload a song
        }
      } ~
      put {
        entity(as[MoveRequest]) { request ⇒
          complete {
            if(request.copy) {
              audio.copySong(request.from, request.to).map { _ ⇒
                StatusCodes.NoContent
              }
            } else {
              audio.moveSong(request.from, request.to).map { _ ⇒
                StatusCodes.NoContent
              }
            }
          }
        }
      } ~
      delete {
        parameters("path".as[Path]) { path ⇒
          complete {
            audio.deleteSong(path).map { _ ⇒
              StatusCodes.NoContent
            }
          }
        }
      }
    } ~
    path("songs" / "active") {
      get {
        complete {
          audio.activeSong().flatMap { s ⇒
            audio.isPlaying().map { p =>
              (s, p)
            }
          }.map[ToResponseMarshallable] { case (s,p) ⇒
            ActiveSongResponse(s, p).asJson
          }
        }
      }
    } ~
    path("songs" / Segment) { hash =>
      get {
        complete {
          audio.getSong(hash).map { song ⇒
            StatusCodes.OK → song.asJson
          }
        }
      } ~
        delete {
          complete {
            ??? // TODO: Implement
            StatusCodes.NoContent
          }
        }
    } ~
    path("playlists") {
      get {
        complete {
          audio.getPlaylists.map { playlists ⇒
            StatusCodes.OK → playlists.asJson
          }
        }
      } ~
        post {
          entity(as[CreatePlaylistRequest]) { request =>
            complete {
              audio.createPlaylist(request.name).map { _ ⇒
                StatusCodes.Accepted
              }
            }
          }
        }
    } ~
    path("playlists" / Segment) { name =>
      get {
        complete {
          audio.getPlaylist(name).map { playlist ⇒
            playlist.asJson
          }
        }
      } ~
      put {
        entity(as[RenamePlaylistRequest]) { request =>
          complete {
            audio.renamePlaylist(name, request.name).map { _ ⇒
              StatusCodes.Accepted
            }
          }
        }
      } ~
      delete {
        complete {
          audio.deletePlaylist(name).map { _ ⇒
            StatusCodes.Accepted
          }
        }
      }
    } ~
    path("playlists" / Segment / "songs") { name =>
      get {
        complete {
          audio.getPlaylist(name).map { playlist ⇒
            StatusCodes.OK -> playlist.songs.asJson
          }
        }
      } ~
      post {
        entity(as[PlaylistMutationRequest]) { request =>
          complete {
            audio.addSongToPlaylist(name, request.hash, request.after).map { _ ⇒
              StatusCodes.Accepted
            }
          }
        }
      } ~
      put {
        entity(as[PlaylistMutationRequest]) { request =>
          complete {
            audio.moveSongInPlaylist(name, request.hash, request.after).map { _ ⇒
              StatusCodes.Accepted
            }
          }
        }
      }
    } ~
    path("playlists" / Segment / "songs" / Segment) { (name, hash) =>
      delete {
        complete {
          audio.removeSongFromPlaylist(name, hash).map { _ ⇒
            StatusCodes.NoContent
          }
        }
      }
    }
  }
}

class AudioRestRoute(override val audio:AudioApp)(implicit ec:ExecutionContext, override val system:ActorSystem) extends AudioRoute