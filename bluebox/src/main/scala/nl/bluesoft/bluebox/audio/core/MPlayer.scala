package nl.bluesoft.bluebox.audio.core

import akka.event.LoggingAdapter
import nl.bluesoft.bluebox.audio.model.{Song, SongProgress}

import scala.concurrent.{ExecutionContext, Future}
import scala.sys.process._
import scala.util.Try

object MPlayer extends nl.bluesoft.peanuts.util.Process
{
  case class ClipInfo(title:Option[String] = None, artist:Option[String] = None, album:Option[String] = None,
                      year:Option[Int] = None, track:Option[Int] = None, channels:Option[Int] = None,
                      genre:Option[String] = None, duration:Option[Double] = None, bitrate:Option[Int]= None,
                      hertz:Option[Int]= None, format:Option[String] = None, seekable:Option[Boolean] = None)

  object Markers {
    val ClipInfo = "Clip info"
    val Beginning = "0"
    val NoSound = "0"
    val NoSoundCard = "null"

    val Title = "title"
    val Artist = "artist"
    val Album = "album"
    val Year = "year"
    val Track = "track"
    val Comment = "comment"
    val Genre = "genre"

    val Duration = "id_length"
    val Hertz = "id_audio_rate"
    val BitRate = "id_audio_bitrate"
    val Channels = "id_audio_nch"
    val Seekable = "id_seekable"
    val Format = "id_audio_format"
  }

  object Options {
    val ResumeAt = "-ss"
    val StopAt = "-endpos"
    val Volume = "-volume"
    val AudioCard = "-ao"
    val Identify = "-identify"
    val Frames = "-frames"
  }

  override val processCommand: String = "mplayer"

  def progressReader(line:String, progressHandler:SongProgress ⇒ Unit):Unit = {
    Try(line.replaceAll("\\(.*\\).*of|\\(.*\\).*", "").replaceAll("  ", " ").split("\\:")(1).trim().split(" ")).map { base ⇒
      val progress = base(0).toDouble
      val total = base(1).toDouble
      progressHandler(SongProgress(progress, total))
    }
  }

  def nullLogger(line:String): Unit = {}
  def errorLogger(line:String)(implicit log:LoggingAdapter): Unit = if(!line.trim.isEmpty) log.debug("ERROR "+line)
  def debugLogger(line:String)(implicit log:LoggingAdapter): Unit = if(!line.trim.isEmpty) log.debug(line)

  def play(song:Song, progress:Double = 0.0)(progressHandler:SongProgress ⇒ Unit)(implicit log:LoggingAdapter, ec:ExecutionContext): Future[Unit] =
    playFile(song.filePath)(progressHandler)

  def playFile(filePath:String, progress:Double = 0.0)(progressHandler:SongProgress ⇒ Unit)(implicit log:LoggingAdapter, ec:ExecutionContext): Future[Unit] =  Future {
    log.info(s"Running mplayer on $operatingSystem")
    val logger = ProcessLogger(line ⇒ progressReader(line,progressHandler), s ⇒ errorLogger(s))
    val exitCode = Seq(processCommand, Options.ResumeAt, progress.toString, filePath) ! logger
    handleFailure(exitCode)
  }

  def play(filePath:String)(implicit ec:ExecutionContext): Future[Unit] =  Future {
    val exitCode = Seq(processCommand, filePath).!(ProcessLogger(nullLogger, nullLogger))
    handleFailure(exitCode)
  }

  def stop(implicit log:LoggingAdapter) = stopProcess

  def clipInfo(song:Song)(implicit log:LoggingAdapter): ClipInfo = {
    implicit def stringToStringOption(s:String):Option[String] = {
      s.trim match {
        case "" ⇒ None
        case _ ⇒ Some(s)
      }
    }

    implicit def stringToDoubleOption(s:String):Option[Double] = {
      s.trim match {
        case "" ⇒ None
        case _ ⇒ Try(s.toDouble).toOption
      }
    }

    implicit def stringToBooleanOption(s:String):Option[Boolean] = {
      s.trim match {
        case "0"     ⇒ Some(false)
        case "1"     ⇒ Some(true)
        case "true"  ⇒  Some(true)
        case "false" ⇒  Some(true)
      }
    }

    implicit def stringToIntOption(s:String):Option[Int] = {
      Try(s.toInt).toOption
    }

    Seq(processCommand, Options.AudioCard, Markers.NoSoundCard, Options.Identify, Options.Frames, "0", song.filePath).!!
      .split("\n").filter(l ⇒ l.contains("=") || l.contains(":")).map { line ⇒
        val keyValue = line.split("[=|:]").map(_.trim)
        Array(Try(keyValue(0).toLowerCase).getOrElse("failure"), Try(keyValue(1)).getOrElse("failure"))
      }.foldLeft(ClipInfo()) {
        case (info, Array(Markers.Title, title))        ⇒ info.copy(title = title)
        case (info, Array(Markers.Artist, artist))      ⇒ info.copy(artist = artist)
        case (info, Array(Markers.Album, album))        ⇒ info.copy(album = album)
        case (info, Array(Markers.Year, year))          ⇒ info.copy(year = year)
        case (info, Array(Markers.Track, track))        ⇒ info.copy(track = track)
        case (info, Array(Markers.Genre, genre))        ⇒ info.copy(genre = genre)
        case (info, Array(Markers.Duration, duration))  ⇒ info.copy(duration = duration)
        case (info, Array(Markers.Hertz, hertz))        ⇒ info.copy(hertz = hertz)
        case (info, Array(Markers.BitRate, bitrate))    ⇒ info.copy(bitrate = bitrate)
        case (info, Array(Markers.Seekable, seekable))  ⇒ info.copy(seekable = seekable)
        case (info, Array(Markers.Channels, channels))  ⇒ info.copy(channels = channels)
        case (info, Array(Markers.Format, format))      ⇒ info.copy(format = format)
        case other                                      ⇒ other._1
    }
  }

  def handleFailure(exitCode:Int) = {
    if(exitCode != 0)
      throw new Exception(s"MPlayer has exit code $exitCode")
  }
}
