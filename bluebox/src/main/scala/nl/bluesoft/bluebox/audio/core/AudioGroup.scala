package nl.bluesoft.bluebox.audio.core

import akka.actor._
import akka.pattern.ask
import akka.util.Timeout
import nl.bluesoft.bluebox.audio.AudioApp
import nl.bluesoft.bluebox.audio.core.AudioGroup._
import nl.bluesoft.bluebox.audio.core.AudioPlayer._
import nl.bluesoft.bluebox.audio.core.Playlists.{AddSong, MoveSong, RemoveSong}
import nl.bluesoft.bluebox.audio.es.SongIndex
import nl.bluesoft.bluebox.audio.model._
import nl.bluesoft.peanuts.actor.PeanutActor

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object AudioGroup {
  val DefaultEmptyPlaylist = HashPlaylist(AudioApp.currentPlaylist)
  def props(volume:ActorRef) = Props(new AudioGroup(volume)(None))
  def props(volume:ActorRef, songResolver:String ⇒ Future[Song]) = Props(new AudioGroup(volume)(Some(songResolver)))

  case class PlaylistUpdated(playlist:Playlist)
  case class CurrentSongUpdated(song:Song)

  case class LoadPlaylist(playlist:HashPlaylist)
  case class PlaylistLoaded(playlist:HashPlaylist)

  case object UnloadPlaylist
  case class PlaylistUnloaded(playlist:HashPlaylist)

  case object StartPlaylist
  case class PlaylistStarted(playlist:HashPlaylist)

  case object PausePlaylist
  case class PlaylistPaused(playlist:HashPlaylist)

  case object ResumePlaylist
  case class PlaylistResumed(playlist:HashPlaylist)

  case object PlayNextSong
  case object PlayPreviousSong
  case class SkipToSong(song:Song)
  case class SkippedToSong(song:Song, wasPlaying:Option[Song])

  object PlayModes {
    type PlayMode = String
    val Simple:PlayMode = "simple"
    val Shuffle:PlayMode = "shuffle"
    val ValidModes = Seq(Simple, Shuffle)
  }

  case class SwitchPlayMode(mode:PlayModes.PlayMode) {
    require(PlayModes.ValidModes.contains(mode), s"PlayMode '$mode' is invalid.")
  }

  case class PlayModeSwitched(mode:PlayModes.PlayMode)

  case object GetCurrentPlaylist
  case class CurrentPlaylist(playlist:HashPlaylist)

  case class PlayerProgressed(player:ManagedPlayer, progress:SongProgress)
  case class PlayerStopped(player:ManagedPlayer)

  case class RegisterPlayer(player:ManagedPlayer)

  case class InitClient(client:ActorRef)

  sealed case class State(playlist:HashPlaylist, currentSong:Option[Song] = None)
}

class AudioGroup(volume:ActorRef)(songResolver:Option[String ⇒ Future[Song]] = None) extends PeanutActor[State] with ActorLogging with SongSelectionStrategy
{
  implicit def toPlaylist(hashPlaylist:HashPlaylist): Playlist = {
    Await.result(Future.sequence(hashPlaylist.hashes.map(h ⇒ songIndex.getSong(h).map(_.headOption))).map { songs ⇒
      Playlist(hashPlaylist.name, songs.flatten)
    }, 15 seconds)
  }

  /**
   * Persistent ID: AudioManager
   * DO NOT CHANGE.
   */
  override val persistenceId: String = "AudioGroup"
  override implicit val timeout = Timeout(30 seconds)
  implicit def indexResolver(hash:String): Future[Song] = songResolver.map(f ⇒ f(hash)).getOrElse[Future[Song]](songIndex.getSong(hash).map(songs ⇒ songs.head))

  set stateTo State(AudioGroup.DefaultEmptyPlaylist)

  override def playlist = currentState.map(_.playlist).getOrElse(AudioGroup.DefaultEmptyPlaylist)

  enable event sourcing
  enable snapshot creation
  make snapshots every(20 events)

  var songIndex = new SongIndex(context.system, log)
  var songProgress = SongProgress(0,0)
  var managedPlayers = Seq.empty[ManagedPlayer]

  def distributor(song:Song):ActorRef = {
    context.child("Distributor-"+song.songHash).getOrElse(context.actorOf(Props(new SongDistributor), "Distributor-"+song.songHash))
  }

  var mode:String = PlayModes.Simple
  switchTo(simple)

  override def events: Receive = {
    case msg: PlayModeSwitched  ⇒  playModeSwitched(msg)
    case msg: LoadPlaylist      ⇒  loadPlaylist(msg)
    case UnloadPlaylist         ⇒  unloadPlaylist()
    case msg:AddSong            ⇒  addSong(msg)
    case msg:RemoveSong         ⇒  removeSong(msg)
    case msg:SongStarted        ⇒  songStartedHandling(msg)
  }

  override def commands: Receive = {
    case PlayNextSong          ⇒  playNext()
    case PlayPreviousSong      ⇒  playPrevious()
    case SkipToSong(song:Song) ⇒  skipToSong(song)
    case StartPlaylist         ⇒  startPlaylist()
    case ResumePlaylist        ⇒  resumePlaylist()
    case PausePlaylist         ⇒  pausePlaylist()
    case GetCurrentPlaylist    ⇒  withState(state ⇒ sender ! CurrentPlaylist(state.playlist))
    case msg:SwitchPlayMode    ⇒  switchPlayMode(msg.mode)
    case msg:SongStopped       ⇒  songStoppedHandling(msg)
    case msg:SongFinished      ⇒  songFinishedHandling(msg)
    case msg:MoveSong          ⇒  moveSong(msg)
    case NotPlaying            ⇒  outputNotPlaying()
    case Terminated(ref)       ⇒  removePlayer(ref)
    case PlayerStopped(player) ⇒  playerStopped(player)
    case RegisterPlayer(player)   ⇒ addPlayer(player)
    case msg @ InitClient(client) ⇒ initClient(client)
    case CannotPlaySong(song, active, progress) if song != active ⇒ forceStartSong(song, progress)
    case CannotPlaySong(song, active, progress) if song == active ⇒ outputAlreadyPlaying(song)
    case SongProgressed(song, progress)                           ⇒ songProgressed(song, progress)
    case msg @ PlayerProgressed(player, progress)                 ⇒ playerProgressed(player, progress)
  }

  def currentHash(state:State) = state.currentSong.map(_.songHash).getOrElse(state.playlist.hashes.head)

  def distributeSong(song:Song):Future[Unit] = {
    log.info(s"Requesting the distribution of ${song.fileName}.")
    distributor(song).ask(SongDistributor.Distribute(song, managedPlayers)).map {
      case SongDistributor.Distributed(_) ⇒
    }
  }

  def distributeSong(hash:String):Future[Unit] = {
    indexResolver(hash).flatMap(distributeSong)
  }

  def distributeNextSong(current:Song):Future[Unit] = {
    selection.getNext(current.songHash).flatMap { next ⇒
      distributeSong(next)
    }
  }

  def distributePreviousSong(current:Song):Future[Unit] = {
    selection.getPrevious(current.songHash).flatMap { next ⇒
      distributeSong(next)
    }
  }

  def startSong(song:Song): Future[Unit] = {
    def innerPlay() = {
      managedPlayers.foreach { player ⇒
        log.info(s"${player.name}: Starting $song")
        player.ref ! StartSong(song, 0)
      }
    }

    Future.sequence(managedPlayers.map { player ⇒
      player.ref.ask(AudioPlayer.DoesSongExist(song)).map {
        case msg:AudioPlayer.SongExists ⇒ msg
      }
    }).flatMap[Unit] { result ⇒
      if(result.forall(_.exists)) {
        log.info(s"No distribution required")
        Future(innerPlay())
      }
      else {
        log.warning(s"The song ${song.fileName} not pre-distributed, recovering...")
        distributeSong(song).map { _ ⇒
          innerPlay()
        }
      }
    }.flatMap[Unit] { _ ⇒
      distributeNextSong(song).flatMap { _ ⇒
        distributePreviousSong(song)
      }
    }.recover { case ex ⇒
      log.error(s"{} occurred while attempting to start song ${song.fileName}: {}", ex.getClass.getSimpleName, ex.getMessage)
      ex.printStackTrace()
    }
  }

  def playModeSwitched(msg: PlayModeSwitched) = {
    mode = msg.mode
  }

  def loadPlaylist(msg: LoadPlaylist) = {
    withState { state ⇒
      set stateTo state.copy(playlist = msg.playlist)
      whileNotRecovering {
        sender ! PlaylistLoaded(msg.playlist)
        context.system.eventStream.publish(PlaylistUpdated(msg.playlist))
      }
      distributeSong(msg.playlist.hashes.head)
    }
  }

  def unloadPlaylist() = {
    withState { state ⇒
      whileNotRecovering {
        sender ! PlaylistUnloaded(state.playlist)
        context.system.eventStream.publish(PlaylistUpdated(HashPlaylist(AudioApp.currentPlaylist)))
      }
      set stateTo state.copy(playlist = HashPlaylist(AudioApp.currentPlaylist))
    }
  }

  def addSong(msg:AddSong) = {
    withState { state ⇒
      log.info(s"Adding ${msg.song.fileName} ${msg.after.map(s ⇒ s"after ${s.fileName}").getOrElse("at the beginning")}")
      set stateTo state.copy(playlist = state.playlist.addSong(msg.song, msg.after))
      withState { stateWithSong ⇒
        whileNotRecovering {
          context.system.eventStream.publish(PlaylistUpdated(stateWithSong.playlist))
        }
      }
    }
  }

  def removeSong(msg:RemoveSong) = {
    withState { state ⇒
      log.info(s"Removing ${msg.song.fileName}")
      set stateTo state.copy(playlist = state.playlist.removeSong(msg.song))
      withState { stateWithoutSong ⇒
        whileNotRecovering(context.system.eventStream.publish(PlaylistUpdated(stateWithoutSong.playlist)))
      }
    }
  }

  def songStoppedHandling(msg:SongStopped) = {
    withPlayer(sender()).foreach { player ⇒
      log.info(s"${player.name}: Stopped song ${msg.song.fileName}")
      changePlayer(player)(_.copy(song = None, status = AudioPlayer.Status.idle))
    }

    if(managedPlayers.forall(_.song.isEmpty)) {
      log.info(s"All players stopped playing")
      context.system.eventStream.publish(msg)
    }
  }

  def songStartedHandling(msg:SongStarted)  = {
    withPlayer(sender()).foreach { player ⇒
      log.info(s"${player.name}: Confirmed starting of ${msg.song.fileName}")
      volume ! VolumeManager.ResetVolume  // TODO: A Volume manager has only 1 instance now, this is not correct for clustering
      changePlayer(player)(_.copy(song = Some(msg.song), status = AudioPlayer.Status.playing))
    }

    if(managedPlayers.forall(_.song.contains(msg.song))) {
      withState { state ⇒
        set stateTo state.copy(currentSong = msg.song)
        log.info(s"All players are playing ${msg.song.fileName}")
        whileNotRecovering(context.system.eventStream.publish(msg))
      }
    }
  }

  def songFinishedHandling(msg:SongFinished) = {
    withState { state ⇒
      log.info(s"$msg.song finished")
      withPlayer(sender()).foreach { player ⇒
        self ! PlayerStopped(player)
        volume ! VolumeManager.ResetVolume
        selection.getNext(msg.song.songHash).map { start ⇒
          log.info(s"Starting song ${start.fileName}")
          player.ref ! StartSong(start, 0)
          distributeNextSong(start)
        }
      }

      if(managedPlayers.forall(_.progress.exists(_.isFinished))) {
        whileNotRecovering(context.system.eventStream.publish(msg))
      }
    }
  }

  def playNext() = {
    withState { state ⇒
      val ref = sender()
      selection.getNext(currentHash(state)).recoverWith { case ex ⇒
        log.warning("Could not get next song. Recovering with first song in playlist")
        songIndex.getSong(playlist.hashes.head).map(_.head)
      }.foreach { song ⇒
        log.info(s"Playing next song: ${song.fileName}")
        ref ! SkippedToSong(song, state.currentSong)
        startSong(song)
      }
    }
  }


  def playPrevious() = {
    withState { state ⇒
      val ref = sender()
      selection.getPrevious(currentHash(state)).recoverWith { case ex ⇒
        log.warning("Could not get previous song. Recovering with first song in playlist")
        songIndex.getSong(playlist.hashes.head).map(_.head)
      }.foreach { song ⇒
        log.info(s"Playing previous song: ${song.fileName}")
        ref ! SkippedToSong(song, state.currentSong)
        startSong(song)
      }
    }
  }

  def skipToSong(song:Song) = {
    val msg = currentState.map(_.currentSong).map(SkippedToSong(song, _)).getOrElse(SkippedToSong(song, None))
    sender ! msg
    context.system.eventStream.publish(msg)
    startSong(song)
  }

  def startPlaylist() = {
    withState { state ⇒
      selection.getFirst.foreach { song ⇒
        startSong(song)
        context.system.eventStream.publish(PlaylistStarted(state.playlist))
      }
    }
  }

  def resumePlaylist() = {
    withState { state ⇒
      state.currentSong.map(_.songHash).orElse(state.playlist.hashes.headOption) match {
        case Some(hash) ⇒
          indexResolver(hash).foreach { song ⇒
            context.system.eventStream.publish(PlaylistResumed(state.playlist))
            val progress = managedPlayers.headOption.flatMap(_.progress).getOrElse(SongProgress(0,0))
            log.info(s"Resuming $song at $progress")
            startSong(song)
          }
        case None ⇒
          log.info("Cannot resume playlist, there are no songs in the playlist")
      }
    }
  }

  def pausePlaylist() = {
    withState { state ⇒
      context.system.eventStream.publish(PlaylistPaused(state.playlist))
      sender ! PlaylistPaused(state.playlist)
      managedPlayers.foreach { player ⇒
        player.ref ! StopSong
      }
    }
  }

  def switchPlayMode(mode:AudioGroup.PlayModes.PlayMode) = {
    switchTo(shuffle)
    context.system.eventStream.publish(PlayModeSwitched(mode))
    sender ! PlayModeSwitched(mode)
    self ! PlayModeSwitched(mode)
  }

  def forceStartSong(song:Song, progress:Double) = {
    sender ! StopSong
    sender ! StartSong(song, progress)
  }

  def outputAlreadyPlaying(song:Song) = {
    withPlayer(sender()).foreach { player ⇒
      log.warning(s"${player.name}: Already playing $song. It seems that an unneeded request was made to this player.")
    }
  }

  def outputNotPlaying() = {
    withPlayer(sender()).foreach { player ⇒
      log.info(s"${player.name}: Not playing")
    }
  }

  def songProgressed(song:Song, progress:SongProgress) = {
    withPlayer(sender()).foreach { player ⇒
      player.progress match {
        case Some(current) ⇒
          if(progress.progress - current.progress >= 1.0 || progress.progress < current.progress) {
            self ! PlayerProgressed(player, progress)
          }
        case None ⇒
          self ! PlayerProgressed(player, progress)
      }
    }
  }

  def playerProgressed(player:ManagedPlayer, progress:SongProgress) = {
    withState { state ⇒
      log.info(s"${player.name}: $progress")
      changePlayer(player)(p ⇒ p.copy(progress = Some(progress)))
      context.system.eventStream.publish(progress)

      managedPlayers
        .map(_.progress.getOrElse(SongProgress(0,0)))
        .sortBy(_.progress)
        .headOption.foreach { progress ⇒
        songProgress = progress
      }
    }
  }

  def playerStopped(player:ManagedPlayer) = {
    log.info(s"${player.name}: Song stopped")
    changePlayer(player)(p ⇒ p.copy(progress = None))
  }

  def moveSong(msg:MoveSong) = {
    log.info(s"Moving ${msg.song.fileName} ${msg.after.map(s ⇒ s"after ${s.fileName}").getOrElse("to the beginning")}")
    self ! RemoveSong(msg.name, msg.song)
    self ! AddSong(msg.name, msg.song, msg.after)
  }

  def initClient(client:ActorRef) = {
    log.info("Initializing new client")
    withState { state ⇒
      client ! songProgress
      client ! PlayModeSwitched(mode)
      state.currentSong.foreach { song ⇒
        managedPlayers.headOption.map(_.status).foreach {
          case AudioPlayer.Status.idle ⇒
            client ! SongStopped(song)
          case AudioPlayer.Status.playing ⇒
            client ! SongStarted(song)
        }
      }
    }
  }

  def removePlayer(ref:ActorRef) = {
    managedPlayers = managedPlayers.filter(_.ref != ref)
  }

  def addPlayer(player:ManagedPlayer) = {
    managedPlayers = managedPlayers :+ player
    log.info(s"Registering player ${player.name}")
    context.watch(player.ref)
  }

  def withPlayer(ref:ActorRef) = {
    managedPlayers.find(_.ref == sender())
  }

  def changePlayer(player:ManagedPlayer)(f:ManagedPlayer ⇒ ManagedPlayer) = {
    managedPlayers = managedPlayers.map(p ⇒ if(p.ref == player.ref) f(p) else p)
  }
}
