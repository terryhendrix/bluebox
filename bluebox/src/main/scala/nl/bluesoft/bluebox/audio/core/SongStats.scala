package nl.bluesoft.bluebox.audio.core

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.contrib.pattern.ClusterSharding
import akka.contrib.pattern.ShardRegion.{EntryId, Msg}
import nl.bluesoft.bluebox.audio.core.AudioGroup.SkippedToSong
import nl.bluesoft.bluebox.audio.core.AudioPlayer.{SongFinished, SongStarted}
import nl.bluesoft.bluebox.audio.core.SongStats.{SkippedSong, WrongRating, RateSong, SongRated}
import nl.bluesoft.bluebox.audio.es.SongStatsIndex
import nl.bluesoft.bluebox.audio.model._
import nl.bluesoft.bluebox.sdk.ActorSystemMembership
import nl.bluesoft.peanuts.actor._
/**
 * Created by Erik Poldervaart on 4-10-2015.
 */
object SongStats extends ShardActorBootstrap
{
  case class RateSong(user:AbstractUser, songHash:String, rating:Int)
  case class SongRated(user:AbstractUser, songHash:String, rating:Int)
  case class WrongRating(songHash:String, error:String)
  case class SkippedSong(wasPlaying:Song)

  override implicit def shardMessages: PartialFunction[Msg, (EntryId, Msg)] = {
    case msg @ RateSong(_, songHash,rating)   => (songHash, msg)
    case msg @ SongStarted(song)              => (song.songHash, msg)
    case msg @ SongFinished(song)             => (song.songHash, msg)
    case msg @ SkippedToSong(song,_)          => (song.songHash, msg)
    case msg @ SkippedSong(song)              => (song.songHash, msg)
  }

  override def shardName: String = "SongStats"

  override def props: Props = Props(new SongStats)
}

class SongStats extends ShardActor[SongStatistics]
{
  val system = context.system
  val index = new SongStatsIndex(system, log)

  override protected def events: Receive = {
    case msg:SongRated if msg.user == EndUser =>
      withState { state ⇒
        val r = state.copy(userRatings = state.userRatings + (msg.user.username → msg.rating))
        set stateTo r.copy(avgUserRating = (r.userRatings.map(_._2.toDouble).sum / r.userRatings.size).toInt)
      }
      withoutState {
        set stateTo SongStatistics(msg.songHash, avgUserRating = msg.rating, 0, userRatings = Map(msg.user.username → msg.rating))
      }

    case msg:SongStarted ⇒
      set stateTo currentState.map { c ⇒
        addSystemRatings(c.copy(counts = c.counts.copy(started = c.counts.started + 1)))
      }.getOrElse[SongStatistics] {
        addSystemRatings(SongStatistics(msg.song.songHash, 0, 0, userRatings = Map.empty, counts = SongEventCounts(started = 1)))
      }

    case msg:SongFinished ⇒
      set stateTo currentState.map { c ⇒
        c.copy(counts = c.counts.copy(finished = c.counts.finished + 1))
      }.getOrElse[SongStatistics] {
        addSystemRatings(SongStatistics(msg.song.songHash, 0, 0, userRatings = Map.empty, counts = SongEventCounts(finished = 1)))
      }

    case msg:SkippedSong  ⇒
      set stateTo currentState.map { c ⇒
        addSystemRatings(c.copy(counts = c.counts.copy(skipped = c.counts.skipped + 1)))
      }.getOrElse[SongStatistics] {
        addSystemRatings(SongStatistics(msg.wasPlaying.songHash, 0, 0, userRatings = Map.empty, counts = SongEventCounts(skipped = 1)))
      }

    case msg:SkippedToSong if msg.song.songHash == shardKey ⇒
      set stateTo currentState.map { c ⇒
        addSystemRatings(c.copy(counts = c.counts.copy(skippedTo = c.counts.skippedTo + 1)))
      }.getOrElse[SongStatistics] {
        addSystemRatings(SongStatistics(msg.song.songHash, 0, 0, userRatings = Map.empty, counts = SongEventCounts(skippedTo = 1)))
      }
  }

  override protected def commands: Receive = {
    case msg:RateSong if msg.user == EndUser =>
      if(msg.rating >= 0 && msg.rating <= 5) {
        log.info("Rating song")
        self ! SongRated(msg.user, msg.songHash, msg.rating)
      }
      else {
        log.error("Rating must be between 0 and 5")
        sender ! WrongRating(msg.songHash,"Rating must be between 0 and 5")
      }
  }

  override def persistenceId: String = shardKey

  override def whenStateChanged(state:Option[SongStatistics]) = {
    whileNotRecovering {
      state.map { rating =>
        index.indexRating(rating).recover {
          case ex: Throwable => ex.printStackTrace()
        }
      }
    }
  }

  def addSystemRatings(state:SongStatistics):SongStatistics = {
    import state.counts._

    val diff = skippedTo - skipped
    val total = skippedTo + skipped
    val deviation = BigDecimal(diff) / BigDecimal(total)
    val rating = 3 + deviation * 2
    val round = Math.round(rating.toDouble).toInt
    log.info(s"Determined system rating $round ($rating) or deviation $deviation")
    state.copy(systemRating = round)
  }
}

trait SongStatsRegion extends ActorSystemMembership
{
  lazy val songStats:ActorRef = ClusterSharding(system).shardRegion(SongStats.shardName)
}