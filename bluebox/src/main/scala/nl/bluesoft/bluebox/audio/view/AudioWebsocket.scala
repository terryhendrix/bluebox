package nl.bluesoft.bluebox.audio.view

import akka.actor._
import akka.stream.actor.ActorPublisher
import akka.stream.actor.ActorPublisherMessage.{Cancel, Request}
import nl.bluesoft.bluebox.audio.core.AudioGroup._
import nl.bluesoft.bluebox.audio.core.AudioPlayer.{SongStarted, SongStopped}
import AudioEventConsumer.UpdateEvent
import nl.bluesoft.bluebox.audio.core.SongIndexer.{IndexMusicDirectory, MusicDirectoryIndexed}
import nl.bluesoft.bluebox.audio.core.VolumeManager.VolumeSet
import nl.bluesoft.bluebox.audio.model.SongProgress
import nl.bluesoft.bluebox.util.files.FileManager.DirectoryUpdated
import spray.json.JsonWriter

import scala.concurrent.duration._
import scala.reflect.ClassTag

object AudioWebsocket extends AudioUpdatesJsonProtocol {
  def props = Props(new AudioWebsocket)
  case class Rescheduled[M](msg:M, count:Int)
  case object StopWhenNoDemand
}

class AudioWebsocket() extends ActorPublisher[String] with AudioEventConsumer
{
  import AudioWebsocket._
  override val subscriptionTimeout =  5 second
  var timeout:Option[Cancellable] = None

  def publish[U : ClassTag](msg:U)(implicit writer: JsonWriter[UpdateEvent[U]])= {
    if(totalDemand > 0 && isActive) {
      onNext(UpdateEvent(msg.getClass.getSimpleName, msg).asJson(writer))
    }
    else if(totalDemand == 0 && isActive){
      timeout = timeout.orElse(Some(context.system.scheduler.scheduleOnce(1 second, self, StopWhenNoDemand)))
      context.system.scheduler.scheduleOnce(0.3 second, self, Rescheduled(msg, 1))
    }
  }

  def receive = {
    case msg:PlaylistStarted        ⇒   publish[PlaylistStarted](msg)
    case msg:PlaylistUpdated        ⇒   publish[PlaylistUpdated](msg)
    case msg:PlaylistResumed        ⇒   publish[PlaylistResumed](msg)
    case msg:PlaylistPaused         ⇒   publish[PlaylistPaused](msg)
    case msg:SongProgress           ⇒   publish[SongProgress](msg)
    case msg:SongStarted            ⇒   publish[SongStarted](msg)
    case msg:SongStopped            ⇒   publish[SongStopped](msg)
    case msg:VolumeSet              ⇒   publish[VolumeSet](msg)
    case msg:DirectoryUpdated       ⇒   publish[DirectoryUpdated](msg)
    case msg:IndexMusicDirectory    ⇒   publish[IndexMusicDirectory](msg)
    case msg:MusicDirectoryIndexed  ⇒   publish[MusicDirectoryIndexed](msg)
    case msg:PlayModeSwitched       ⇒   publish[PlayModeSwitched](msg)

    case StopWhenNoDemand if totalDemand == 0 ⇒
      log.info("Stopping websocket stream cause there is no demand")
      self ! Cancel
      timeout = None

    case Cancel               ⇒
      log.info("Completing websocket stream")
      onComplete()
      context.stop(self)

    case msg:Rescheduled[_] if totalDemand > 0 ⇒
      self forward msg.msg

    case msg:Rescheduled[_] if msg.count < 5 && totalDemand == 0 ⇒
      context.system.scheduler.scheduleOnce((0.3 seconds) * msg.count, self, msg.copy(count = msg.count + 1))

    case Request(cnt) ⇒   // TODO: Do we need this for totalDemand increments?

    case msg                                ⇒
      log.warning(s"Unhandled event $msg, not publishing.")
  }
}