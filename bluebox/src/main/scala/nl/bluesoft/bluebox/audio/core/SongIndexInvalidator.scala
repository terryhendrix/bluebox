package nl.bluesoft.bluebox.audio.core
import java.nio.file.{Files, Paths}

import akka.actor.{Actor, ActorLogging, Props}
import akka.stream.Materializer
import akka.stream.actor.ActorPublisher
import akka.stream.scaladsl.{Flow, Sink, Source}
import nl.bluesoft.bluebox.audio.es.SongIndex
import nl.bluesoft.bluebox.audio.model.Song
import nl.bluesoft.peanuts.actor.streams.BatchPublisher
import nl.bluesoft.peanuts.elasticsearch.util.ElasticSearchImplicits

import scala.concurrent.{ExecutionContext, Future}

object SongIndexInvalidator {
  case object RemoveInvalidEntries
}

trait SongIndexInvalidator extends ElasticSearchImplicits
{ this:Actor with ActorLogging ⇒

  def index:SongIndex
  implicit def ec:ExecutionContext
  implicit def mat:Materializer

  val retrieveSongs: (BatchPublisher.Offset, BatchPublisher.Size) ⇒ Future[Seq[Song]] = { (offset, size) ⇒
    log.debug(s"Retrieving batch. offset=$offset, size=$size")
    index.getSongs(offset, size)
  }

  val invalidator = Flow[Song].mapAsync(1) { song ⇒
    if (!Files.exists(Paths.get(song.filePath))) {
      log.info(s"Invalidating song ${song.fileName}")
      index.indexSong(song.copy(active = false)).map { _ ⇒ 1 }
    }
    else
      Future(0)
  }

  def removeInvalidEntries() = {
    log.info("Starting index cleanup")
    val actor = context.system.actorOf(Props(new BatchPublisher[Song](10)(retrieveSongs)))
    val publisher = ActorPublisher[Song](actor)
    Source(publisher).via(invalidator).runWith(Sink.fold(0) { (a,b) ⇒ a + b } ).map { cnt ⇒
      if(cnt != 0)
        log.info(s"Removed $cnt songs from the index as they no longer appeared on the hard drive.")
      else
        log.info("All index items are still valid.")
    }
  }
}
