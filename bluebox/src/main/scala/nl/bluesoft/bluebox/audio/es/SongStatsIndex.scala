package nl.bluesoft.bluebox.audio.es

import akka.actor.ActorSystem
import akka.event.LoggingAdapter
import com.sksamuel.elastic4s.ElasticDsl._
import com.sksamuel.elastic4s.mappings.FieldType._
import com.sksamuel.elastic4s.mappings.TypedFieldDefinition
import nl.bluesoft.bluebox.audio.model.SongStatistics
import nl.bluesoft.bluebox.audio.util.AudioJsonProtocol
import nl.bluesoft.peanuts.elasticsearch.ElasticSearch.{IndexName, SearchLimit, SearchOffset}
import nl.bluesoft.peanuts.elasticsearch.ElasticSearchIndex
import nl.bluesoft.peanuts.elasticsearch.util.ElasticSearchImplicits
import org.elasticsearch.search.sort.SortOrder

import scala.concurrent.ExecutionContext

class SongStatsIndex(override val system: ActorSystem, override implicit val log:LoggingAdapter)
  extends ElasticSearchIndex with AudioJsonProtocol with ElasticSearchImplicits
{
  override def indexName: IndexName = "rating"
  override implicit def ec: ExecutionContext = system.dispatcher

  override def indexDefinition: Seq[TypedFieldDefinition] = Seq(
    "_id" typed StringType index NotAnalyzed,
    "songHash" typed StringType,
    "avgUserRating" typed IntegerType,
    "systemRating" typed IntegerType,
    "userRatings" typed ObjectType,
    "counts" typed ObjectType as (
      "started" typed LongType,
      "finished" typed LongType,
      "skipped" typed LongType,
      "skippedTo" typed LongType
    )
  )

  def getRatings(implicit offset:SearchOffset = 0, limit:SearchLimit = 1000) = {
    list(sortBy = "avgUserRating", order = SortOrder.DESC).mapResults[SongStatistics]
  }

  def getRating(songHash:String) = {
    query {
      bool {
        must (termQuery("songHash", songHash))
      }
    }(0,1).mapSingle[SongStatistics]
  }

  def indexRating(rating: SongStatistics) = {
    doIndex(id = rating.songHash) (
      "songHash" -> rating.songHash,
      "userRatings"  -> rating.userRatings,
      "avgUserRating"  -> rating.avgUserRating,
      "systemRating"  -> rating.systemRating,
      "counts" → Map(
        "started" → rating.counts.started,
        "finished" → rating.counts.finished,
        "skipped" → rating.counts.skipped,
        "skippedTo" → rating.counts.skippedTo
      )
    )
  }
}
