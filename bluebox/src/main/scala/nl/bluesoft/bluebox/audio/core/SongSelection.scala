package nl.bluesoft.bluebox.audio.core
import akka.event.LoggingAdapter
import nl.bluesoft.bluebox.audio.model.{HashPlaylist, Song}

import scala.concurrent.{ExecutionContext, Future}

trait SongSelectionStrategy
{
  def playlist:HashPlaylist
  def log:LoggingAdapter
  
  def simple = new LinearSongSelection {
    override def playlistImpl: HashPlaylist = playlist
  }

  def shuffle = new ShuffleSongSelection {
    override def playlistImpl: HashPlaylist = playlist
    override def logging: LoggingAdapter = log
  }

  var selection:SongSelection = simple

  def switchTo(selector:SongSelection) = selection = selector
}

trait SongSelection
{
  protected def resolve(hash:Option[String], error:String)(implicit songResolver:String ⇒ Future[Song], ec:ExecutionContext) = {
    hash match {
      case Some(h) ⇒ songResolver(h)
      case None    ⇒ Future(throw new RuntimeException(error))
    }
  }

  def playlistImpl:HashPlaylist

  def getNext(current:String)(implicit songResolver:String⇒Future[Song], ec:ExecutionContext):Future[Song]

  def getPrevious(current:String)(implicit songResolver:String⇒Future[Song], ec:ExecutionContext):Future[Song]

  def getFirst(implicit songResolver:String ⇒ Future[Song], ec:ExecutionContext):Future[Song]
}

trait LinearSongSelection extends SongSelection
{
  override def getPrevious(current:String)(implicit songResolver:String⇒Future[Song], ec:ExecutionContext):Future[Song] = {
    resolve(playlistImpl.getHashBefore(current), "Could not previous next song")
  }

  override def getNext(current:String)(implicit songResolver:String⇒Future[Song], ec:ExecutionContext):Future[Song] = {
    resolve(playlistImpl.getHashAfter(current), "Could not get next song")
  }

  def getFirst(implicit songResolver:String ⇒ Future[Song], ec:ExecutionContext):Future[Song] = {
    resolve(playlistImpl.hashes.headOption, "No songs in playlist")
  }
}

trait ShuffleSongSelection extends SongSelection
{
  def logging:LoggingAdapter

  /**
   * The current song list
   */
  var todo:Seq[String] = Seq.empty

  /**
   * The list of previous played songs
   */
  var done:Seq[String] = Seq.empty

  override def getPrevious(current:String)(implicit songResolver:String⇒Future[Song], ec:ExecutionContext):Future[Song] = {
    val i = done.indexOf(current)
    if (i != 0) {
      val hash = done(i - 1)
      songResolver(hash)
    }
    else getFirst
  }

  def work = {
    if(todo.isEmpty) {
      todo = playlistImpl.hashes
      logging.info(s"Initializing shuffle queue: $todo")
      todo
    }
    else {
      todo
    }
  }

  implicit class TakeRandom(list:Seq[String]) {
    def takeRandom = {
      val n = (todo.length * Math.random()).toInt
      todo(n)
    }
  }

  override def getNext(current:String)(implicit songResolver:String⇒Future[Song], ec:ExecutionContext):Future[Song] = {
    val hash = work.takeRandom
    todo = todo.filter(_ != hash)
    done = done.filter(_ != hash) :+ hash
    songResolver(hash)
  }

  def getFirst(implicit songResolver:String⇒Future[Song], ec:ExecutionContext):Future[Song] = {
    getNext("")
  }
}

