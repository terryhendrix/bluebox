package nl.bluesoft.bluebox.audio
import akka.actor.ActorSystem
import akka.util.Timeout
import nl.bluesoft.bluebox.audio.core.{SongStatsRegion, SongStats}
import nl.bluesoft.bluebox.audio.model.{EndUser, SongStatistics}
import nl.bluesoft.bluebox.sdk.SystemDispatcher
import nl.bluesoft.bluebox.util.{ErrorCodes, BlueBoxException}
import nl.bluesoft.peanuts.elasticsearch.ElasticSearch.{SearchLimit, SearchOffset}
import scala.concurrent.{ExecutionContext, Future}

trait StatsController extends SongStatsRegion with IndexDef with SystemDispatcher
{ this:AudioApp ⇒

  implicit def timeout:Timeout

  override def getRatings(implicit offset:SearchOffset = 0, limit:SearchLimit = 1000): Future[Seq[SongStatistics]] = {
    ratingIndex.getRatings(offset, limit)
  }

  override def rateSong(songHash:String, rating:Int): Future[Unit] = {
    songIndex.getSong(songHash).map {
      case songs if songs.nonEmpty ⇒
        songStats ! SongStats.RateSong(EndUser, songHash, rating)
      case songs if songs.isEmpty ⇒
        throw new BlueBoxException(ErrorCodes.NotFound, s"Song '$songHash' does not exist")
    }
  }

  override def getRating(songHash: String): Future[Option[SongStatistics]] = {
    ratingIndex.getRating(songHash)
  }

  override def getRatings(songHash: String*): Future[Option[SongStatistics]] = ???
}
