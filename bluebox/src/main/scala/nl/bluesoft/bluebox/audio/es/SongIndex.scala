package nl.bluesoft.bluebox.audio.es
import java.nio.file.Path
import akka.actor.ActorSystem
import akka.event.LoggingAdapter
import com.sksamuel.elastic4s.mappings.FieldType._
import com.sksamuel.elastic4s.mappings.TypedFieldDefinition
import nl.bluesoft.bluebox.audio.model.Song
import nl.bluesoft.bluebox.audio.util.AudioJsonProtocol
import nl.bluesoft.peanuts.elasticsearch.ElasticSearch.{IndexName, SearchLimit, SearchOffset}
import nl.bluesoft.peanuts.elasticsearch.ElasticSearchIndex
import nl.bluesoft.peanuts.elasticsearch.util.ElasticSearchImplicits
import org.elasticsearch.search.sort.SortOrder
import spray.json.DefaultJsonProtocol
import scala.collection.JavaConversions._
import scala.concurrent.{ExecutionContext, Future}
import com.sksamuel.elastic4s.ElasticDsl._

// TODO: Move to Peanuts
case class SuggestionOption(text:String, score:Double)
case class Suggestion(text:String, options:Seq[SuggestionOption], offset:Int, length:Int)
case class SuggestionResult(title:String, suggestions:Seq[Suggestion])

class SongIndex(override val system: ActorSystem, override implicit val log:LoggingAdapter)
                extends ElasticSearchIndex with DefaultJsonProtocol with ElasticSearchImplicits
                with AudioJsonProtocol
{
  override def indexName: IndexName = "songs"
  override implicit def ec: ExecutionContext = system.dispatcher

  override def indexDefinition: Seq[TypedFieldDefinition] = Seq(
    "_id" typed StringType index NotAnalyzed,
    "path" typed StringType index NotAnalyzed,
    "analyzedPath" typed StringType,
    "fileName" typed StringType index NotAnalyzed,
    "analyzedFileName" typed StringType,
    "songHash" typed StringType index NotAnalyzed,
    "pathHash" typed StringType index NotAnalyzed,
    "active" typed BooleanType,
    "clipInfo" typed ObjectType as (
      "album" typed StringType,
      "artist" typed StringType,
      "artistCompletion" typed CompletionType indexAnalyzer "simple" searchAnalyzer "simple" payloads true,
      "comment" typed StringType,
      "genre" typed StringType,
      "title" typed StringType,
      "titleCompletion" typed CompletionType indexAnalyzer "simple" searchAnalyzer "simple" payloads true,
      "track" typed IntegerType,
      "year" typed IntegerType,
      "duration" typed DoubleType,
      "hertz" typed IntegerType,
      "bitrate" typed IntegerType,
      "seekable" typed BooleanType,
      "format" typed StringType
    )
  )

  def getFromDirectory(directory:Path)(implicit offset:SearchOffset = 0, limit:SearchLimit = 1000):Future[Seq[Song]] = {
    query(sortBy = "fileName", order = SortOrder.ASC) {
      bool {
        must(
          termQuery("path", directory.toAbsolutePath.toString),
          termQuery("active", true)
        )
      }
    }.mapResults[Song]
  }

  def getByPathHash(pathHash:String)(implicit offset:SearchOffset = 0, limit:SearchLimit = 1000):Future[Seq[Song]] = {
    query(sortBy = "fileName", order = SortOrder.ASC) {
      bool {
        must(
          termQuery("pathHash", pathHash),
          termQuery("active", true)
        )
      }
    }.mapResults[Song]
  }

  /**
   * Get a sequence of ''Song'' from the index.
   * @param offset    Search offset.
   * @param limit     Search limit.
   * @return
   */
  def getSongs(implicit offset:SearchOffset = 0, limit:SearchLimit = 1000): Future[Seq[Song]] = {
    query(sortBy = "fileName", order = SortOrder.ASC) {
      bool(
        must(termQuery("active", true))
      )
    }(offset, limit).mapResults[Song]
  }

  /**
   * Get a ''Song'' by hash. If the song has multiple occurences, mutliple items will be returned.
   * @param hash      The hash of the songs file-content.
   * @param offset    Search offset.
   * @param limit     Search limit.
   * @return
   */
  def getSong(hash: String)(implicit offset:SearchOffset = 0, limit:SearchLimit = 1000) : Future[Seq[Song]] = {
    query(sortBy = "fileName", order = SortOrder.ASC) {
      bool(
        must(termQuery("songHash",  hash), termQuery("active", true))
      )
    }.mapResults[Song]
  }

  def getSpellingCorrections(searchQuery:String): Unit = {
    import com.sksamuel.elastic4s.ElasticDsl._
    es.doSearch(search.in(indexName -> "view").size(0).query(
      bool(
        must(termQuery("active", true))
      )
    ).suggestions(
      termSuggestion("artist")
        .field("clipInfo.artist")
        .text(searchQuery),
      termSuggestion("title")
        .field("clipInfo.title")
        .text(searchQuery),
      termSuggestion("fileName")
        .field("analyzedFileName")
        .text(searchQuery),
      termSuggestion("path")
        .field("analyzedPath")
        .text(searchQuery)
    ))
  }

  def getCompletions(searchQuery:String): Future[Seq[SuggestionResult]] = {
    import com.sksamuel.elastic4s.ElasticDsl._
    es.doSearch(search.in(indexName -> "view").size(0).query(
      bool(
        must(termQuery("active", true))
      )
    ).suggestions(
      completionSuggestion("artist")
        .field("clipInfo.artistCompletion")
        .text(searchQuery),
      completionSuggestion("title")
        .field("clipInfo.titleCompletion")
        .text(searchQuery)
    )).map { res ⇒
      res.getSuggest.iterator().toStream.map { suggestion ⇒
        SuggestionResult(suggestion.getName, suggestion.iterator().toSeq.map { e ⇒
          Suggestion(e.getText.string(), e.getOptions.iterator().toSeq.map { o ⇒
            SuggestionOption(o.getText.string(), o.getScore)
          }, e.getOffset, e.getLength)
        })
      }
    }
  }

  def searchSongs(search: String)(implicit offset:SearchOffset = 0, limit:SearchLimit = 1000) : Future[Seq[Song]] = {
    query(sortBy = "_score", order = SortOrder.DESC) {
      bool(
        must(
          termQuery("active", true)
        ).should(
          matchQuery("analyzedPath",  search),
          matchQuery("analyzedFileName",  search),
          matchQuery("clipInfo.artist",  search),
          matchQuery("clipInfo.title",  search)
        ).minimumShouldMatch(1)
      )
    }.mapResults[Song]
  }

  /**
   * Indexes a ''Song'' using 'pathHash + songHash' as identifier.
   * @param song  The song to store in the index.
   * @return
   */
  def indexSong(song: Song) = {
    doIndex(id = song.id) (
      "path" → song.path,
      "analyzedPath" → song.path,
      "fileName" → song.fileName,
      "analyzedFileName" → song.fileName,
      "songHash" → song.songHash,
      "pathHash" → song.pathHash,
      "active" → song.active,
      "clipInfo" → song.clipInfo.map { info ⇒
        Map(
          "album" → info.album,
          "artist" → info.artist,
          "artistCompletion" → info.artist,
          "genre" → info.genre,
          "title" → info.title,
          "titleCompletion" → info.title,
          "track" → info.track,
          "year" → info.year,
          "duration" → info.duration,
          "hertz" → info.hertz,
          "bitrate" → info.bitrate,
          "seekable" → info.seekable,
          "format" → info.format
        ).filter(_._2.isDefined).map(t ⇒ (t._1, t._2.get))
      }.getOrElse(null)
    )
  }
}
