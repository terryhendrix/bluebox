package nl.bluesoft.bluebox.audio.util
import nl.bluesoft.bluebox.audio.AudioApp.SearchResults
import nl.bluesoft.bluebox.audio.core.MPlayer.ClipInfo
import nl.bluesoft.bluebox.audio.core.SongIndexer
import nl.bluesoft.bluebox.audio.es.{Suggestion, SuggestionOption, SuggestionResult}
import nl.bluesoft.bluebox.audio.model._
import nl.bluesoft.peanuts.json.PeanutsJsonProtocol
import spray.json.{JsObject, JsString, JsValue, RootJsonFormat}

trait AudioJsonProtocol extends PeanutsJsonProtocol
{
  implicit val clipInfoJsonFormat = jsonFormat12(ClipInfo)
  implicit val songJsonFormat = jsonFormat6(Song)


  implicit object AbstractUserFormat extends RootJsonFormat[AbstractUser]
  {
    def read(json: JsValue): AbstractUser = {
      val fields = json.asJsObject.fields
      fields("username").asInstanceOf[JsString].value match {
        case s if s == SystemUser.username ⇒
          SystemUser
        case s if s == EndUser.username ⇒
          EndUser
        case other ⇒
          throw new Exception(s"Username $other is not defined as valid username")
      }
    }

    override def write(obj: AbstractUser): JsValue = {
      JsObject(Map[String, JsValue](
        "username" → JsString(obj.username)
      ))
    }
  }

  implicit val statsCountFormat = jsonFormat4(SongEventCounts)
  implicit val statsJsonFormat = jsonFormat5(SongStatistics)
  implicit val playlistJsonFormat = jsonFormat2(Playlist)
  implicit val hashPlaylistJsonFormat = jsonFormat3(HashPlaylist)
  implicit val songProgressJsonFormat = jsonFormat2(SongProgress)
  implicit val dirJsonFormat = jsonFormat2(Directory)
  implicit val suggestionOptionJsonFormat = jsonFormat2(SuggestionOption)
  implicit val suggestionJsonFormat = jsonFormat4(Suggestion)
  implicit val suggestionResultJsonFormat = jsonFormat2(SuggestionResult)
  implicit val searchResultsJsonFormat = jsonFormat2(SearchResults)
  implicit val volumeJsonFormat = jsonFormat1(Volume)
  implicit val statsFormat = jsonFormat5(SongIndexer.Stats)
}

object AudioJsonProtocol extends AudioJsonProtocol
