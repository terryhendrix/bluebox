package nl.bluesoft.bluebox.audio

import java.nio.file._

import akka.actor._
import akka.stream.scaladsl._
import akka.util.Timeout
import nl.bluesoft.bluebox.audio.api.v1.AudioRestRoute
import nl.bluesoft.bluebox.audio.core.AudioGroup.PlayModes.PlayMode
import nl.bluesoft.bluebox.audio.core.SongIndexer.IndexMusicDirectory
import nl.bluesoft.bluebox.audio.core.{Playlists, _}
import nl.bluesoft.bluebox.audio.es._
import nl.bluesoft.bluebox.audio.model._
import nl.bluesoft.bluebox.audio.view.AudioView
import nl.bluesoft.bluebox.core.BlueBox
import nl.bluesoft.bluebox.sdk.{AppRoute, BlueBoxApp}
import nl.bluesoft.peanuts.elasticsearch.ElasticSearch.{SearchLimit, SearchOffset}
import nl.bluesoft.peanuts.elasticsearch.util.ElasticSearchImplicits

import scala.concurrent.Future
import scala.concurrent.duration._

/**
 * A factory for the AudioApp, which is then registered as ''Extension''
 */
object AudioApp extends ExtensionIdProvider with ExtensionId[AudioApp]
{
  override def createExtension(system: ExtendedActorSystem): AudioExtension = {
    system.log.info("Loading AudioApp")
    new AudioExtension(system)
  }

  override def lookup(): ExtensionId[_ <: Extension] = AudioApp

  val currentPlaylist = "audio-manager-playlist"

  case class SearchResults(songs:Map[String, Seq[Song]], completions:Map[String, Seq[String]] = Map.empty)
}

/**
 * An interface for interacting with the AudioExtension
 */
trait AudioApp extends BlueBoxApp with Extension with ElasticSearchImplicits {
  def musicRootDirectory: Path

  def audioPublisher:Source[String, Unit]

  def play():Future[Unit]
  def pause():Future[Unit]
  def resume():Future[Unit]
  def goto(songHash:Song):Future[Unit]
  def previous():Future[Unit]
  def next():Future[Unit]
  def setPlayMode(mode:PlayMode):Future[Unit]

  def getVolume():Future[Volume]
  def setVolume(volume:Int):Future[Unit]
  def mute():Future[Unit]
  def unmute():Future[Unit]

  def loadPlaylist(name:String):Future[Unit]
  def unloadPlaylist:Future[Unit]

  def createPlaylist(name:String):Future[Unit]
  def getPlaylists(implicit offset:SearchOffset = 0, limit:SearchLimit = 1000): Future[Seq[HashPlaylist]]
  def getPlaylist(name:String): Future[Playlist]
  def renamePlaylist(oldName:String, newName:String):Future[Unit]
  def deletePlaylist(name:String):Future[Unit]

  def addSongToPlaylist(playlistName: String, songHash: String, afterSongHash: Option[String]): Future[Unit]
  def moveSongInPlaylist(playlistName:String, songHash:String, afterSongHash:Option[String]): Future[Unit]
  def removeSongFromPlaylist(playlistName:String, songHash:String): Future[Unit]

  def getRatings(implicit offset:SearchOffset = 0, limit:SearchLimit = 1000):Future[Seq[SongStatistics]]
  def rateSong(songHash:String, rating:Int):Future[Unit]
  def getRating(songHash:String):Future[Option[SongStatistics]]
  def getRatings(songHash:String*):Future[Option[SongStatistics]]

  def getSongs(directory:Path):Future[Seq[Song]]
  def getDirectories(directory:Path):Future[Seq[Directory]]
  def getSongs:Future[Seq[Song]]
  def getSong(hash:String):Future[Song]

  def createDirectory(path:Path): Future[Unit]
  def copyDirectory(from:Path, dir:Path):Future[Unit]
  def moveDirectory(from:Path, dir:Path): Future[Unit]
  def deleteDirectory(path:Path, force:Boolean): Future[Unit]

  def moveSong(from:Path, dir:Path): Future[Unit]
  def copySong(from:Path, dir:Path):Future[Unit]
  def deleteSong(path:Path): Future[Unit]
  def searchSongs(query: String): Future[AudioApp.SearchResults]

  def getSongProgress():Future[SongProgress]
  def isPlaying():Future[Boolean]
  def activeSong():Future[Option[Song]]
}

trait ActorDef  {
  def audioManager: ActorRef
  def volumeManager: ActorRef
  def player:ActorRef
  def indexer: ActorRef
  def files: ActorRef
  def view:ActorRef
}

trait IndexDef {
  val songIndex:SongIndex
  val playlistIndex:PlaylistIndex
  val ratingIndex:SongStatsIndex
}

/**
 * An akka ''Extension'' implementation of ''AudioApp''
 * @param system    The ActorSystem to create the extension in.
 */
class AudioExtension(override val system:ExtendedActorSystem) extends AudioApp with VolumeController with StatsController
                                                              with MusicController with PlaylistController with FilesController
{
  var songProgress = Song
  override val route: AppRoute = new AudioRestRoute(this)(ec, system)
  val settings = BlueBox.settings(system.settings.config)
  override val musicRootDirectory:Path = settings.directory("music")
  implicit val timeout = Timeout(settings.timeout("audio-app"))
  implicit val log = system.log

  val volumeManager: ActorRef = system.actorOf(VolumeManager.props, "Volume")
  val audioManager: ActorRef = system.actorOf(AudioGroup.props(volumeManager), "AudioGroup")
  val player = system.actorOf(AudioPlayer.props(audioManager, "Player"), "Player")
  val indexer: ActorRef = system.actorOf(SongIndexer.props(), "Indexer")
  val files: ActorRef = system.actorOf(MusicFileManager.props(musicRootDirectory, settings), "Files")
  val view = system.actorOf(AudioView.props, "AudioView")

  Playlists.startShardRegion(system)

  val songIndex = new SongIndex(system, system.log)
  val playlistIndex = new PlaylistIndex(system, system.log)
  val ratingIndex = new SongStatsIndex(system, system.log)

  songIndex.createIndex()
  ratingIndex.createIndex()
  playlistIndex.createIndex()

  system.scheduler.schedule(
    initialDelay = 30 seconds,
    interval = settings.interval("song-indexer"),
    receiver = indexer,
    message = IndexMusicDirectory(settings.directory("music").toString, recursive = true)
  )
}


