package nl.bluesoft.bluebox.audio
import akka.pattern.ask
import akka.util.Timeout
import nl.bluesoft.bluebox.audio.core.VolumeManager._
import nl.bluesoft.bluebox.audio.model.Volume
import nl.bluesoft.bluebox.sdk.SystemDispatcher
import nl.bluesoft.bluebox.util.{BlueBoxException, ErrorCodes}

import scala.concurrent.Future

trait VolumeController extends ActorDef with SystemDispatcher
{ this:AudioApp ⇒

  implicit def timeout:Timeout

  override def setVolume(volume:Int):Future[Unit] = {
    (volumeManager ? SetVolume(Volume(volume))).map {
      case VolumeSet(Volume(`volume`), _) ⇒
      case _ ⇒
        throw new BlueBoxException(ErrorCodes.Conflict, s"The volume could not be set.")
    }
  }

  override def mute():Future[Unit] = {
    (volumeManager ? Mute).map {
      case VolumeSet(_, true) ⇒
      case _ ⇒
        throw new BlueBoxException(ErrorCodes.Conflict, s"The volume could not be muted.")
    }
  }

  override def unmute():Future[Unit] = {
    (volumeManager ? Unmute).map {
      case VolumeSet(_, true) ⇒
      case _ ⇒
        throw new BlueBoxException(ErrorCodes.Conflict, s"The volume could not be un-muted.")
    }
  }

  override def getVolume():Future[Volume] = {
    (volumeManager ? GetVolume).mapTo[Volume]
  }
}
