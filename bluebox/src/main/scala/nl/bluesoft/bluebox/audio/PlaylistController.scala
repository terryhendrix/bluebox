package nl.bluesoft.bluebox.audio
import akka.pattern.ask
import akka.util.Timeout
import nl.bluesoft.bluebox.audio.core.AudioGroup.CurrentPlaylist
import nl.bluesoft.bluebox.audio.core.Playlists.DeletePlaylist
import nl.bluesoft.bluebox.audio.core.{AudioGroup, Playlists, PlaylistsRegion}
import nl.bluesoft.bluebox.audio.model.{HashPlaylist, Playlist}
import nl.bluesoft.bluebox.sdk.SystemDispatcher
import nl.bluesoft.bluebox.util.{BlueBoxException, ErrorCodes}
import nl.bluesoft.peanuts.elasticsearch.ElasticSearch.{SearchLimit, SearchOffset}

import scala.concurrent.Future

trait PlaylistController extends PlaylistsRegion with IndexDef with ActorDef with SystemDispatcher
{ this: AudioApp ⇒

  implicit def timeout:Timeout

  def loadPlaylist(name:String):Future[Unit] = {
    playlistIndex.getPlaylist(name).flatMap {
      case Some(playlist) ⇒
        (audioManager ? AudioGroup.LoadPlaylist(playlist)).map {
          case AudioGroup.PlaylistLoaded(_) ⇒
        }
      case None ⇒
        throw new BlueBoxException(ErrorCodes.NotFound, s"Playlist '$name' does not exist")
    }
  }

  def unloadPlaylist:Future[Unit] = {
    (audioManager ? AudioGroup.UnloadPlaylist).map {
      case AudioGroup.PlaylistUnloaded(_) ⇒
    }
  }

  override def getPlaylists(implicit offset:SearchOffset = 0, limit:SearchLimit = 1000): Future[Seq[HashPlaylist]] = {
    playlistIndex.getPlaylists
  }

  override def createPlaylist(name: String): Future[Unit] = {
    playlistIndex.getPlaylist(name).map {
      case None ⇒
        playlists ! Playlists.CreatePlaylist(name)
      case Some(_) ⇒
        throw new BlueBoxException(ErrorCodes.Conflict, s"Playlist '$name' already exists")
    }
  }

  override def renamePlaylist(oldName: String, newName: String): Future[Unit] = {
    playlistIndex.getPlaylist(oldName).flatMap {
      case Some(_) ⇒
        playlistIndex.getPlaylist(newName).map {
          case None ⇒
            playlists ! Playlists.RenamePlaylist(oldName, newName)
          case Some(_) ⇒
            throw new BlueBoxException(ErrorCodes.Conflict, s"Playlist '$newName' already exists")
        }
      case None ⇒
        throw new BlueBoxException(ErrorCodes.NotFound, s"Playlist '$oldName' does not exist")
    }
  }

  override def deletePlaylist(playlistName: String): Future[Unit] = {
    playlistName match {
      case AudioApp.currentPlaylist ⇒
        unloadPlaylist
      case _ ⇒
        playlistIndex.getPlaylist(playlistName).map {
          case Some(_) ⇒
            playlists ! DeletePlaylist(playlistName)
          case None ⇒
            throw new BlueBoxException(ErrorCodes.NotFound, s"Playlist '$playlistName' does not exist")
        }
    }
  }


  override def addSongToPlaylist(playlistName: String, songHash: String, afterSongHash: Option[String]): Future[Unit] = {
    songIndex.getSong(songHash).flatMap {
      case songs if songs.nonEmpty ⇒
        afterSongHash.map(songIndex.getSong) match {
          case Some(f) ⇒
            f.map {
              case after if after.nonEmpty ⇒
                playlistName match {
                  case AudioApp.currentPlaylist ⇒
                    Future(audioManager ! Playlists.AddSong(playlistName, songs.head, after.headOption))
                  case _ ⇒
                    playlistIndex.getPlaylist(playlistName).map {
                      case Some(playlist) ⇒
                        playlists ! Playlists.AddSong(playlistName, songs.head, after.headOption)
                      case None ⇒
                        throw new BlueBoxException(ErrorCodes.NotFound, s"Playlist '$playlistName' does not exist")
                    }
                }
              case after if after.isEmpty ⇒
                throw new BlueBoxException(ErrorCodes.NotFound, s"Song '$afterSongHash' does not exist")
            }
          case None ⇒
            playlistName match {
              case AudioApp.currentPlaylist ⇒
                Future(audioManager ! Playlists.AddSong(playlistName, songs.head, None))
              case _ ⇒
                playlistIndex.getPlaylist(playlistName).map {
                  case Some(playlist) ⇒
                    playlists ! Playlists.AddSong(playlistName, songs.head, None)
                  case None ⇒
                    throw new BlueBoxException(ErrorCodes.NotFound, s"Playlist '$playlistName' does not exist")
                }
            }
        }

      case songs if songs.isEmpty ⇒
        throw new BlueBoxException(ErrorCodes.NotFound, s"Song '$songHash' does not exist")
    }
  }

  // TODO: Look at addSongInPlaylist and copy the nested matching structure
  override def moveSongInPlaylist(playlistName: String, songHash: String, afterSongHash: Option[String]): Future[Unit] = {
    playlistIndex.getPlaylist(playlistName).flatMap {
      case Some(_) ⇒
        songIndex.getSong(songHash).flatMap {
          case songs if songs.nonEmpty ⇒
            afterSongHash.map(songIndex.getSong).getOrElse(Future(Seq.empty)).map {
              case maybeAfterSong if playlistName == AudioApp.currentPlaylist ⇒
                audioManager ! Playlists.MoveSong(playlistName, songs.head, maybeAfterSong.headOption)
              case maybeAfterSong ⇒
                playlists ! Playlists.MoveSong(playlistName, songs.head, maybeAfterSong.headOption)
            }
          case songs if songs.isEmpty ⇒
            throw new BlueBoxException(ErrorCodes.NotFound, s"Song '$songHash' does not exist")
        }
      case None ⇒
        throw new BlueBoxException(ErrorCodes.NotFound, s"Playlist '$playlistName' does not exist")
    }
  }

  override def removeSongFromPlaylist(playlistName: String, songHash: String): Future[Unit] = {
    songIndex.getSong(songHash).map {
      case songs if songs.nonEmpty && playlistName == AudioApp.currentPlaylist  ⇒
        audioManager ! Playlists.RemoveSong(playlistName, songs.head)
      case songs if songs.nonEmpty  ⇒
        playlists ! Playlists.RemoveSong(playlistName, songs.head)
      case songs if songs.isEmpty ⇒
        throw new BlueBoxException(ErrorCodes.NotFound, s"Song '$songHash' does not exist")
    }
  }

  override def getPlaylist(name: String): Future[Playlist] = {
    def buildPlaylist(hashPlaylist:HashPlaylist):Future[Playlist] = {
      Future.sequence(hashPlaylist.hashes.map(h ⇒ songIndex.getSong(h).map(_.headOption))).map { songs ⇒
        Playlist(hashPlaylist.name, songs.flatten)
      }
    }

    name match {
      case AudioApp.`currentPlaylist` ⇒
        (audioManager ? AudioGroup.GetCurrentPlaylist).flatMap {
          case CurrentPlaylist(hashPlaylist) ⇒
            buildPlaylist(hashPlaylist)
        }
      case _ ⇒
        playlistIndex.getPlaylist(name).map {
          case Some(playlist) ⇒ Playlist(playlist.name, Seq.empty)  // TODO: retrieve songs from the index and add these to the playlist
          case None ⇒
            throw new BlueBoxException(ErrorCodes.NotFound, s"Playlist '$name' does not exist")
        }
    }
  }

}
