package nl.bluesoft.bluebox.audio.core
import java.nio.file.Path

import akka.actor.Props
import akka.util.Timeout
import nl.bluesoft.bluebox.audio.es.SongIndex
import nl.bluesoft.bluebox.audio.model.Song
import nl.bluesoft.bluebox.audio.core.SongIndexer.{Stats, MusicDirectoryIndexed, IndexMusicDirectory}
import nl.bluesoft.bluebox.core.BlueBox
import nl.bluesoft.bluebox.util.files.FileManager
import nl.bluesoft.bluebox.util.files.FileManager.DirectoryUpdated
import nl.bluesoft.peanuts.util.{Algorithms, Hash}

import scala.concurrent.{ExecutionContext, Future}

object MusicFileManager
{
  def props(rootDirectory:Path, settings:BlueBox.Settings) = Props(new MusicFileManager(rootDirectory, settings))
}

class MusicFileManager(override val rootDirectory:Path, settings:BlueBox.Settings) extends FileManager {
  val songIndex = new SongIndex(context.system, log)
  override implicit def timeout: Timeout = settings.timeout("file-transfer")

  override def updated(path:Path)(implicit ec:ExecutionContext = context.dispatcher): Future[Unit] = {
    Future {
      val quickIndexer = context.system.actorOf(SongIndexer.props())
      quickIndexer ! IndexMusicDirectory(path.toString, force = true, recursive = false)
    }
  }

  /**
   * After a song of directory is deleted, it sends an update about the enclosing directory.
   * Also the items are invalidated from in the index
   * @param path
   */
  override def deleted(path:Path)(implicit ec:ExecutionContext = context.dispatcher): Future[Unit] = {
    def invalidate(songs:Seq[Song]) = songs.foreach { s ⇒
      log.info(s"Invalidating ${s.filePath}")
      songIndex.indexSong(s.copy(active = false))
    }

    context.system.eventStream.publish(DirectoryUpdated(path.getParent))

    songIndex.getFromDirectory(path).map(invalidate)
      .map(_ ⇒ Hash.string(path.toAbsolutePath.toString, Algorithms.MD5))
      .flatMap(hash ⇒ songIndex.getByPathHash(hash).map(invalidate))
      .map(_ => context.system.eventStream.publish(MusicDirectoryIndexed(path.getParent.toAbsolutePath.toString, Stats())))
  }
}