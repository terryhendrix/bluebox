package nl.bluesoft.bluebox.audio.api.v1
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import nl.bluesoft.bluebox.audio.util.AudioJsonProtocol
import nl.bluesoft.peanuts.json.JsonMarshalling

trait AudioRouteJsonProtocol extends AudioJsonProtocol with JsonMarshalling with SprayJsonSupport {
  import AudioRestRoute._
  implicit val audioActionRequestJsonFormat = jsonFormat3(AudioActionRequest)
  implicit val ratingRequestJsonFormat = jsonFormat1(RatingRequest)
  implicit val moveRequest = jsonFormat(MoveRequest, "from", "to", "copy")
  implicit val createDirRequest = jsonFormat2(CreateDirectoryRequest)
  implicit val searchRequest = jsonFormat1(SearchRequest)
  implicit val playlistCreateRequestJsonFormat = jsonFormat1(CreatePlaylistRequest)
  implicit val playlistRenameRequestJsonFormat = jsonFormat1(RenamePlaylistRequest)
  implicit val playlistMutationRequestJsonFormat = jsonFormat2(PlaylistMutationRequest)
  implicit val activeSongResponseJsonFormat= jsonFormat2(ActiveSongResponse)
}
