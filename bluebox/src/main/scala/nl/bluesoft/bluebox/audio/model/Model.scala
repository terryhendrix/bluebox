package nl.bluesoft.bluebox.audio.model
import akka.actor.ActorRef
import nl.bluesoft.bluebox.audio.core.{MPlayer, AudioPlayer}
import scala.util.Try

trait FileSystemLike
{
  def path:String
}

case class Song(override val path:String, fileName:String, songHash:String, pathHash:String,
                clipInfo:Option[MPlayer.ClipInfo] = None, active:Boolean = true) extends FileSystemLike
{
  def filePath = s"$path/$fileName"
  def id = s"$songHash$pathHash"
}

case class Volume(volume:Int)

case class Directory(override val path:String, name:Option[String] = None) extends FileSystemLike


case class SongProgress(progress:Double, total:Double) {
  def percentage = progress /  total * 100
  override def toString = f"$progress / $total ($percentage%.2f%%)"

  def isFinished = total - progress <= 1.0
}

case class ManagedPlayer(name:String, ref:ActorRef, song:Option[Song], progress:Option[SongProgress],
                          status:AudioPlayer.Status)



trait AbstractUser {
  def username:String
}

case object EndUser extends AbstractUser
{
  override def username: String = "user"
}


case object SystemUser extends AbstractUser
{
  override def username: String = "system"
}

case class SongEventCounts(started:Long = 0, finished:Long = 0, skipped:Long = 0, skippedTo:Long = 0)

case class SongStatistics(songHash:String, avgUserRating:Int = 0 , systemRating:Int = 0 , userRatings:Map[String,Int] = Map.empty, counts:SongEventCounts = SongEventCounts())

trait PlaylistActions {
  def addSong(song:Song, after:Option[Song]):PlaylistActions
  def removeSong(song:Song):PlaylistActions
}

case class Playlist(name:String, songs:Seq[Song] = Seq.empty) extends PlaylistActions {
  def addSong(song:Song, after:Option[Song]):Playlist = {
    if(songs.exists(_.songHash == song.songHash))
      removeSong(song)

    copy(songs = after.map { after ⇒
      songs.flatMap { s ⇒
        if(s.songHash == after.songHash)
          Seq(s, song)
        else
          Seq(s)
      }
    }.getOrElse(song +: songs).distinct)
  }

  def removeSong(song:Song):Playlist = {
    copy(songs = songs.filter(_.songHash != song.songHash))
  }

  def getSongAfter(song:Song): Option[Song] = {
    (songs.last +: songs :+ songs.head).sliding(2).find { slide ⇒
      Try(slide.head.songHash == song.songHash).getOrElse(false)
    }.map(_.last)
  }

  def getSongBefore(song:Song): Option[Song] = {
    (songs.last +: songs :+ songs.head).sliding(2).find { slide ⇒
      Try(slide.last.songHash == song.songHash).getOrElse(false)
    }.map(_.head)
  }
}

case class HashPlaylist(name:String, active:Boolean = true, hashes:Seq[String] = Seq.empty) extends PlaylistActions
{
  def addSong(song:Song, after:Option[Song]):HashPlaylist = {
    if(hashes.contains(song.songHash))
      removeSong(song)

    copy(hashes = after.map { after ⇒
      hashes.flatMap { h ⇒
        if(h == after.songHash)
          Seq(h, song.songHash)
        else
          Seq(h)
      }
    }.getOrElse(song.songHash +: hashes).distinct)
  }

  def removeSong(song:Song):HashPlaylist = {
    copy(hashes = hashes.filter(_ != song.songHash))
  }

  def getHashAfter(song:Song): Option[String] = {
    getHashAfter(song.songHash)
  }

  def getHashAfter(hash:String): Option[String] = {
    (hashes.last +: hashes :+ hashes.head).sliding(2).find { slide ⇒
      Try(slide.head == hash).getOrElse(false)
    }.map(_.last)
  }

  def getHashBefore(song:Song): Option[String] = {
    getHashBefore(song.songHash)
  }

  def getHashBefore(hash:String): Option[String] = {
    (hashes.last +: hashes :+ hashes.head).sliding(2).find { slide ⇒
      Try(slide.last == hash).getOrElse(false)
    }.map(_.head)
  }
}


