package nl.bluesoft.bluebox.core
import java.nio.file.{Path, Paths}

import akka.actor._
import akka.event.Logging
import com.typesafe.config.Config
import nl.bluesoft.bluebox.audio.AudioApp
import nl.bluesoft.bluebox.core.api.v1.BlueBoxRest
import nl.bluesoft.bluebox.lights.LightsApp
import nl.bluesoft.peanuts.actor.SystemStats
import nl.bluesoft.peanuts.config.ExtendedTypeSafeConfig

import scala.concurrent.duration._

object BlueBox extends ExtensionIdProvider with ExtensionId[BlueBox] with ExtendedTypeSafeConfig {
  override def createExtension(system: ExtendedActorSystem): BlueBox = {
    system.log.info("Loading BlueBox")
    new BlueBoxExtension(system)
  }

  override def lookup(): ExtensionId[_ <: Extension] = BlueBox

  case class Settings(directories:Map[String, Path], intervals:Map[String, FiniteDuration], timeouts:Map[String, FiniteDuration]) {
    def directory(name:String): Path = directories(name)
    def timeout(name:String): FiniteDuration = timeouts(name)
    def interval(name:String): FiniteDuration = intervals(name)
  }

  def settings(config:Config):Settings = {
    val directories = Map(
      "music" → Paths.get(config.getString("bluebox.directories.music")).toAbsolutePath,
      "mplayer-cache" → Paths.get(config.getString("bluebox.directories.mplayer-cache")).toAbsolutePath
    )

    val timeouts = Map(
      "audio-app"           → config.getFiniteDuration("bluebox.timeouts.audio-app"),
      "song-indexer"        → config.getFiniteDuration("bluebox.timeouts.song-indexer"),
      "playlists"           → config.getFiniteDuration("bluebox.timeouts.playlists"),
      "ratings"             → config.getFiniteDuration("bluebox.timeouts.ratings"),
      "file-transfer"       → config.getFiniteDuration("bluebox.timeouts.file-transfer")
    )

    val intervals = Map(
      "song-indexer"        → config.getFiniteDuration("bluebox.intervals.song-indexer"),
      "sys-stats"           → config.getFiniteDuration("bluebox.intervals.sys-stats")
    )

    new Settings(directories, intervals, timeouts)
  }
}

trait BlueBox extends Extension {
  def settings:BlueBox.Settings
  def rest:BlueBoxRest
  def audio:AudioApp
  def lights:LightsApp
}

class BlueBoxExtension(system:ExtendedActorSystem) extends BlueBox
{
  override val settings  = BlueBox.settings(system.settings.config)
  override val audio     = AudioApp(system)
  override val lights    = LightsApp(system)
  override val rest      = new BlueBoxRest(audio.route, lights.route)(system)

  rest.initializeRestApi(system)

  val jvmStats: Cancellable = {
    val stats = system.actorOf(SystemStats.props(Logging.InfoLevel), "SystemStats")
    implicit val ec = system.dispatcher
    system.scheduler.schedule(settings.interval("sys-stats") , settings.interval("sys-stats"), stats, SystemStats.LogStats)
  }
}
