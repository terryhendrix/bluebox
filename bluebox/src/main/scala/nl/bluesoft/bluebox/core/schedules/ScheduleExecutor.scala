package nl.bluesoft.bluebox.core.schedules
import akka.actor.{Actor, ActorLogging}
import nl.bluesoft.bluebox.sdk.{Dispatcher, ContextDispatcher, Schedule}
import scala.concurrent.Future


trait ScheduleExecution extends Dispatcher {
  def execSchedule(schedule:Schedule) = Future {
    if(schedule.trigger.hasTriggered) {
      schedule.command.exec()
    }
  }
}

class ScheduleExecutor extends ScheduleExecution with Actor with ActorLogging with ContextDispatcher
{
  override def receive: Receive = idle

  def idle:Receive = {
    case s:Schedule ⇒
      context.become(executing)
      execSchedule(s).map { _ ⇒
        context.become(idle)
      }.recover { case ex ⇒
        log.error("{} while executing schedule: {}", ex.getClass.getSimpleName, ex.getMessage)
        context.become(idle)
      }
  }

  def executing:Receive = {
    case s ⇒
      log.warning(s"Dropping message $s")
  }
}
