package nl.bluesoft.bluebox.core.api.v1

import java.util.Date

import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server._
import akka.stream.{ActorMaterializer, Materializer}
import nl.bluesoft.bluebox.sdk.AppRoute
import nl.bluesoft.bluebox.util.BlueBoxException
import nl.bluesoft.peanuts.date.DateFormats
import nl.bluesoft.peanuts.http.ExtendableRestApi
import nl.bluesoft.peanuts.http.routes.{CorsSupport, ViewRoute}
import nl.bluesoft.peanuts.http.util.{JsonErrorResponse, LessCompilation}

object BlueBoxRest {
  val BlueBoxID = "BlueBox"
  val BlueBoxVersion = "v1"
  val BlueBoxName = "box"

  case class PingResponse(id:String, version:String, timestamp:Date)
}

class BlueBoxRest(routes:AppRoute*)(implicit val system: ActorSystem) extends ExtendableRestApi with ViewRoute with CorsSupport
                     with LessCompilation with JsonErrorResponse with DateFormats with BlueBoxRestJsonProtocol
{
  import BlueBoxRest._

  implicit val ec = system.dispatcher
  override val config = system.settings.config.getConfig("bluebox")
  override implicit def mat: Materializer = ActorMaterializer()

  override val extendedExceptionHandler:PartialFunction[Throwable, Route] = {
    case ex:BlueBoxException ⇒
      complete(StatusCodes.InternalServerError → ex.asJson)
  }

  routes.foreach { route ⇒
    extendRoute {
      system.log.info(s"Extending REST api with /api/${route.version}/${route.name}")
      pathPrefix("api" / s"${route.version}" / s"${route.name}") {
        route.route ~ path("ping") {
          complete {
            PingResponse(
              timestamp = new Date(),
              id        = route.name,
              version   = route.version
            ).asJson
          }
        }
      }
    }
  }

  override val extendedRoute: Route = {
    pathPrefix("api" / BlueBoxVersion / BlueBoxName) {
      path("ping") {
        complete {
          PingResponse(
            timestamp = new Date(),
            id        = BlueBoxID,
            version   = BlueBoxVersion
          ).asJson
        }
      }
    }
  }
}
