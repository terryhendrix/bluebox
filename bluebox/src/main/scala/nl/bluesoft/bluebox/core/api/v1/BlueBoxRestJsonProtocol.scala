package nl.bluesoft.bluebox.core.api.v1

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import nl.bluesoft.bluebox.audio.util.AudioJsonProtocol
import nl.bluesoft.bluebox.util.BlueBoxException
import nl.bluesoft.peanuts.json.JsonMarshalling
import spray.json._

trait BlueBoxRestJsonProtocol extends AudioJsonProtocol with SprayJsonSupport with JsonMarshalling
{
  import BlueBoxRest._
  implicit val pingFormat = jsonFormat3(PingResponse)

  implicit object BlueBoxExceptionFormat extends RootJsonFormat[BlueBoxException]
  {
    def read(json: JsValue): BlueBoxException = {
      val fields = json.asJsObject.fields
      new BlueBoxException(
        fields("code").asInstanceOf[JsNumber].value.toInt,
        fields("message").asInstanceOf[JsString].value
      )
    }

    override def write(obj: BlueBoxException): JsValue = {
      var build = collection.mutable.Map[String, JsValue](
        "type" → JsString(obj.getClass.getName),
        "code" → JsNumber(obj.code),
        "message" → JsString(obj.getMessage)
      )
      Option(obj.getCause).map(e ⇒ ThrowableFormat.write(e)).foreach(c ⇒ build += ("cause" → c))

      JsObject(build.toMap)
    }
  }
}
