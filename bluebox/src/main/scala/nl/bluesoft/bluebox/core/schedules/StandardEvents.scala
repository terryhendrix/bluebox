package nl.bluesoft.bluebox.core.schedules
import java.time.{ZoneId, Instant, DayOfWeek, LocalDateTime}
import java.util.Calendar
import akka.event.LoggingAdapter
import com.luckycatlabs.sunrisesunset.SunriseSunsetCalculator
import com.luckycatlabs.sunrisesunset.dto.Location
import nl.bluesoft.bluebox.sdk.Event
import scala.concurrent.duration._
import scala.language.postfixOps

trait DateEvent extends Event {
  def now = LocalDateTime.now()
}

trait SunEvent extends DateEvent {
  def lat:Long
  def lon:Long

  def calculator = {
    val location = new Location(lat.toString, lon.toString)
    new SunriseSunsetCalculator(location, "Europe/Amsterdam")
  }

  def sunset = LocalDateTime.ofInstant(Instant.ofEpochMilli(calculator.getOfficialSunsetCalendarForDate(Calendar.getInstance()).getTime.getTime), ZoneId.systemDefault())
  def sunrise = LocalDateTime.ofInstant(Instant.ofEpochMilli(calculator.getOfficialSunriseCalendarForDate(Calendar.getInstance()).getTime.getTime), ZoneId.systemDefault())
}

case class SunsetEvent(override val lat:Long, override val lon:Long)(implicit override val log:LoggingAdapter) extends SunEvent
{
  override protected def trigger(): Boolean = {
    now isAfter sunset
  }

  override def minimumTimeInBetween: FiniteDuration = 23.hours
}

case class SunriseEvent(override val lat:Long, override val lon:Long)(implicit override val log:LoggingAdapter) extends SunEvent
{
  override protected def trigger(): Boolean = {
    now isAfter sunrise
  }

  override def minimumTimeInBetween: FiniteDuration = 23.hours
}

object TimeEvent {
  val EveryDayOfTheWeek = "EveryDayOfTheWeek"
  val EveryDayOfTheMonth = 0
}

/**
 * An event that goes off when it's a certain time. It will only go off once a day
 * @param minute          The minute the event triggers.
 * @param hour            The hours the event triggers.
 * @param dayOfWeek       The day of the week the event triggers, using values from java.time.DayOfWeek or use TimeEvent.EveryDayOfTheWeek for every day.
 * @param dayOfMonth      The day of the month the event triggers, use a value from 1 until 31 or use TimeEvent.EveryDayOfTheMonth for every day.
 * @param log
 */
case class TimeEvent(minute:Int, hour:Int, dayOfWeek:String, dayOfMonth:Int)(implicit override val log:LoggingAdapter) extends DateEvent
{
  override def minimumTimeInBetween: FiniteDuration = 23 hours

  override protected def trigger(): Boolean = {
    now.getMinute == minute && hour == now.getHour                                                  &&
    (dayOfWeek == TimeEvent.EveryDayOfTheWeek || DayOfWeek.valueOf(dayOfWeek) == now.getDayOfWeek)  &&
    (dayOfMonth == TimeEvent.EveryDayOfTheMonth || dayOfMonth == now.getDayOfMonth)
  }
}