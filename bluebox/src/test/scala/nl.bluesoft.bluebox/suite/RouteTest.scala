package nl.bluesoft.bluebox.suite
import akka.actor.ActorSystem
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import nl.bluesoft.bluebox.core.BlueBox
import nl.bluesoft.bluebox.core.api.v1.BlueBoxRestJsonProtocol
import nl.bluesoft.peanuts.json.JsonMarshalling
import org.scalatest.Suite

trait RouteTest extends TestSpec with Suite with ScalatestRouteTest with BlueBoxRestJsonProtocol with SprayJsonSupport with JsonMarshalling
{
  override def createActorSystem() = {
    ActorSystem("TestSystem", ConfigFactory.parseString(Defaults.config()).resolve())
  }

  lazy val _system = system
  implicit val mat = ActorMaterializer()

  val rest = BlueBox(system).rest
}
