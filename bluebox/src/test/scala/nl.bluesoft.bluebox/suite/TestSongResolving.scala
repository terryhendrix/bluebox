package nl.bluesoft.bluebox.suite
import nl.bluesoft.bluebox.audio.model.Song
import scala.concurrent.Future

trait TestSongResolving
{ this:TestSuite ⇒

  implicit def testResolver(hash:String): Future[Song] = {
    Future(testSongs.find(_.songHash == hash).getOrElse(throw new Exception(s"Hash $hash is not in TestSuite.testSongs")))
  }

}
