package nl.bluesoft.bluebox.suite
import akka.actor.ActorSystem
import akka.event.LoggingAdapter
import akka.testkit.TestKit
import com.typesafe.config.ConfigFactory
import nl.bluesoft.bluebox.audio.core.{Playlists, SongStats}
import nl.bluesoft.bluebox.audio.es.{PlaylistIndex, SongIndex, SongStatsIndex}
import org.scalatest.time.{Millis, Seconds, Span}

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.implicitConversions
import scala.util.Try

abstract class TestSuite(config: String = "") extends TestKit(ActorSystem("TestSystem", ConfigFactory.parseString(Defaults.config() + config).resolve()))
                                              with TestSpec
{
  val _system = system
  implicit val log: LoggingAdapter = system.log
  implicit val ec                  = system.dispatcher
  override implicit val patienceConfig = PatienceConfig(scaled(Span(10, Seconds)), scaled(Span(100, Millis)))

  implicit def uToOption[U](u:U): Option[U] = Option(u)

  Playlists.startShardRegion(system)
  SongStats.startShardRegion(system)

  lazy val songIndex = new SongIndex(system, log)
  lazy val playlistIndex = new PlaylistIndex(system, system.log)
  lazy val ratingIndex = new SongStatsIndex(system, system.log)


  def createIndices(): Unit = Try {
    log.info("Creating indices")
    Future.sequence(Seq(
      songIndex.createIndex(),
      playlistIndex.createIndex(),
      ratingIndex.createIndex()
    )).futureValue
  }.recover {
    case ex: Throwable =>
      ex.printStackTrace()
      lapseTime(3 second)
      deleteIndices()
      createIndices()
  }

  def deleteIndices(): Unit = {
    log.info("Clearing indices")
    Future.sequence(Seq(
      songIndex.clearIndex(),
      playlistIndex.clearIndex(),
      ratingIndex.clearIndex()
    )).futureValue
  }

  override protected def beforeAll() = {
    createIndices()
  }

  override protected def afterAll() = {
    deleteIndices()
    system.shutdown()
    system.awaitTermination(15 seconds)
  }
}