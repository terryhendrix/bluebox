package nl.bluesoft.bluebox.suite

trait WithoutElasticSearch
{ this: TestSuite ⇒

  override def createIndices(): Unit = {}
  override def deleteIndices(): Unit = {}
}
