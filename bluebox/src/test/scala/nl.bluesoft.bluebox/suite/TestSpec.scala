package nl.bluesoft.bluebox.suite
import nl.bluesoft.bluebox.audio.core.PlaylistsRegion
import nl.bluesoft.bluebox.audio.model.Song
import nl.bluesoft.bluebox.util.BlueBoxPredef
import org.scalatest._
import org.scalatest.concurrent._
import scala.concurrent.duration.FiniteDuration

trait TestSpec extends FeatureSpecLike with BeforeAndAfterAll with BeforeAndAfterEach with Eventually with Matchers
with GivenWhenThen with ScalaFutures with PlaylistsRegion with BlueBoxPredef
{
  def lapseTime(duration:FiniteDuration) = Thread sleep duration.toMillis

  val heavyFuel = Song(
    path = "src/test/resources",
    fileName = "Dire Straits - Heavy Fuel.mp3",
    songHash = "song12abcd",
    pathHash = "path12abcd"
  )

  val moneyForNothing = Song(
    path = "src/test/resources",
    fileName = "Dire Straits - Money For Nothing.mp3",
    songHash = "song34abcd",
    pathHash = "path34abcd"
  )

  val mannishBoy = Song(
    path = "src/test/resources/sub",
    fileName = "01 Mannish Boy.flac",
    songHash = "song56abcd",
    pathHash = "path56abcd"
  )

  val rollingAndTumblin = Song(
    path = "src/test/resources/sub",
    fileName = "02 Rollin' & Tumblin' Pt. 1.flac",
    songHash = "song78abcd",
    pathHash = "path78abcd"
  )

  val downTheRoad = Song(
    path = "src/test/resources/sub/sub",
    fileName = "01 - C2C - Down the Road.mp3",
    songHash = "song90abcd",
    pathHash = "path90abcd"
  )

  val testSongs = Seq(heavyFuel, moneyForNothing, mannishBoy, rollingAndTumblin, downTheRoad)
}
