package nl.bluesoft.bluebox.suite
import java.nio.file.{Paths, Files}
import java.util.UUID

/**
 * Defaults to use in tests.
 */
object Defaults
{
  def config() = {
    val uuid = UUID.randomUUID().toString
    val esBaseDir = "target/es-test"
    val esDir = s"$esBaseDir/es-data-" + uuid
    val esConfigFile = s"$esBaseDir/es-config-" + uuid + ".yml"
    val clusterPort = MathUtil.randomInt(10000, 40000).getOrElse(9200)
    val tcpPort = MathUtil.randomInt(10000, 40000).getOrElse(9200)

    val esConfig = s"""
        |  node.local:                       false
        |  index.mapping.ignore_malformed:   true
        |  cluster.name:                     "$uuid"
        |  cluster.port:                     $clusterPort
        |  http.enabled:                     false
        |  http.port:                        $tcpPort
        |  path.home:                        "$esDir"
        |  store.type:                       "niofs"
        |  bootstrap.mlockall:               true
        |  transport.tcp.port:               $tcpPort
        |  http.cors.enabled:                true
        |  http.cors.allow-origin:           "*"
        |  http.cors.allow-methods:          "OPTIONS,HEAD,GET,POST,PUT,DELETE"
        |  http.cors.allow-headers:          "X-Requested-With,X-Auth-Token,Content-Type,Content-Length"
        |  es.logger.level:                  "DEBUG"
        |  gateway.expected_nodes:           1
        |  gateway.recover_after_time:       1s
     """.stripMargin

    Files.createDirectories(Paths.get(esBaseDir))
    Files.write(Paths.get(esConfigFile), esConfig.getBytes)

    s"""
       |  elasticsearch.settings = "file://"$${ BLUEBOX_HOME }"/bluebox/$esConfigFile"
    """.stripMargin
  }
}
