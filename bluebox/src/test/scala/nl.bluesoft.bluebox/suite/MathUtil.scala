package nl.bluesoft.bluebox.suite
import scala.util.{Random, Try}

object MathUtil {
  val random = new Random()

  def diffCoordinates(curLat: Double, curLong: Double, prevLat: Double, prevLong: Double) = {
    pythagorasABToC(abs(curLat - prevLat), abs(curLong - prevLong))
  }
  def pythagorasABToC(a: Double, b: Double): Double = Math.sqrt(a * a + b * b)

  def abs(x: Double): Double = if (x < 0)  x * -1  else  x

  def abs(x: Long): Long = if (x < 0)  x * -1  else  x

  def abs(x: Int): Int = if (x < 0)  x * -1  else  x

  def randomInt(min:Int, max:Int):Try[Int] = {
    require(max > min, s"The max '$max' is smaller than min '$min'")
    Try(random.nextInt(max-min) + min)
  }

  def randomDouble(min:Double, max:Double):Try[Double] = {
    require(max > min, s"The max '$max' is smaller than min '$min'")
    Try(random.nextDouble() * (max-min) + min)
  }

  def round(double:Double, precision:Int):Double = {
    BigDecimal(double).setScale(precision, BigDecimal.RoundingMode.HALF_UP).toDouble
  }
}
