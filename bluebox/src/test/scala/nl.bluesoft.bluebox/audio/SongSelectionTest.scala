package nl.bluesoft.bluebox.audio

import akka.event.LoggingAdapter
import nl.bluesoft.bluebox.audio.core.{LinearSongSelection, ShuffleSongSelection}
import nl.bluesoft.bluebox.audio.model.{HashPlaylist, Song}
import nl.bluesoft.bluebox.suite.{TestSuite, WithoutElasticSearch}

import scala.concurrent.Future

class SongSelectionTest extends TestSuite with WithoutElasticSearch
{
  val mockedSong = Song(
    path = "mock",
    fileName = "mock",
    songHash = "mock",
    pathHash = "mock"
  )

  def generateHashes(count:Int) = Range(0, count).map(_.toString)

  val mockedHashPlaylist = HashPlaylist("MockedHashes", active = true, hashes = generateHashes(25))

  feature("Song selection should be inter-exchangable") {
    require(mockedHashPlaylist.hashes.size >= 25, "The tests are designed to run with at least 25 hashes")

    implicit def songResolver(hash:String): Future[Song] = {
      Future(mockedSong.copy(songHash = hash))
    }

    scenario("1. Linear song selection ") {

      val selector = new LinearSongSelection {
        override def playlistImpl: HashPlaylist = mockedHashPlaylist
      }

      selector.getFirst.futureValue.songHash shouldBe "0"
      selector.getNext("0").futureValue.songHash shouldBe "1"
      selector.getNext("4").futureValue.songHash shouldBe "5"
      selector.getNext("23").futureValue.songHash shouldBe "24"
      intercept[RuntimeException] {
        selector.getNext(mockedHashPlaylist.hashes.size.toString).futureValue.songHash
      }
    }

    scenario("2. Shuffled song selection, get next song") {
      val selector = new ShuffleSongSelection {
        override def playlistImpl: HashPlaylist = mockedHashPlaylist
        override def logging: LoggingAdapter = system.log
      }

      val playlists = Range(0, 3).map { _ ⇒
        val randomized = mockedHashPlaylist.hashes.map { _ ⇒
          selector.getNext("").futureValue
        }.map(_.songHash)

        randomized.size shouldBe mockedHashPlaylist.hashes.size
        randomized should not be mockedHashPlaylist.hashes
        randomized.sortBy(_.toInt) shouldBe mockedHashPlaylist.hashes
        randomized
      }

      playlists.distinct.size shouldBe playlists.size
    }

    scenario("3. Shuffled song selection, get previous song") {
      val selector = new ShuffleSongSelection {
        override def playlistImpl: HashPlaylist = mockedHashPlaylist
        override def logging: LoggingAdapter = system.log
      }

      val played = Range(0,5).map { _ ⇒
        selector.getNext("").futureValue
      }.map(_.songHash)

      log.info(s"$played")
      selector.getPrevious(played(4)).futureValue.songHash shouldBe played(3)
      selector.getPrevious(played(3)).futureValue.songHash shouldBe played(2)
      selector.getPrevious(played(2)).futureValue.songHash shouldBe played(1)
      selector.getPrevious(played(1)).futureValue.songHash shouldBe played(0)
    }
  }
}
