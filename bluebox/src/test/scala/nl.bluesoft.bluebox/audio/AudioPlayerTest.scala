package nl.bluesoft.bluebox.audio

import akka.testkit.TestProbe
import nl.bluesoft.bluebox.audio.core.AudioGroup.RegisterPlayer
import nl.bluesoft.bluebox.audio.core.AudioPlayer
import nl.bluesoft.bluebox.audio.core.AudioPlayer.StopSong
import nl.bluesoft.bluebox.audio.model.ManagedPlayer
import nl.bluesoft.bluebox.suite.TestSuite

import scala.concurrent.duration._

class AudioPlayerTest extends TestSuite
{
  val testProbe = TestProbe()

  feature("An audio player must start and stop songs, having not more than 1 song playing at a given moment in time") {
    Given("The manager already exists")
    val manager = TestProbe()
    When("A player is created")
    val player = system.actorOf(AudioPlayer.props(manager.ref, "Living room"), "AudioPlayer")
    Then("The manager will get a message")
    manager.expectMsg(RegisterPlayer(ManagedPlayer("Living room", player, None, None, AudioPlayer.Status.idle)))

    scenario("1. Play") {
      testProbe.send(player, AudioPlayer.StartSong(heavyFuel, 0))
      lapseTime(3 seconds)
      testProbe.expectMsg(AudioPlayer.SongStarted(heavyFuel))
    }

    scenario("2. Play again (dont do anything)") {
      testProbe.send(player, AudioPlayer.StartSong(moneyForNothing, 30))
      testProbe.expectMsgPF(3 seconds) {
        case AudioPlayer.CannotPlaySong(`moneyForNothing`, `heavyFuel`, _) ⇒
      }
    }

    scenario("3. Stop") {
      testProbe.send(player, StopSong)
      testProbe.expectMsgPF(3 seconds) {
        case msg @ AudioPlayer.SongStopped(`heavyFuel`) ⇒
      }

      manager.receiveWhile(1 second){
        case AudioPlayer.SongProgressed(song, progress) ⇒
      }
    }

    scenario("4. Stop again") {
      testProbe.send(player, StopSong)
      testProbe.expectMsg(AudioPlayer.NotPlaying)
    }

    scenario("5. Resume song at 60 seconds") {
      testProbe.send(player, AudioPlayer.StartSong(moneyForNothing, 60))
      lapseTime(3 seconds)
      testProbe.expectMsg(AudioPlayer.SongStarted(moneyForNothing))
    }

    scenario("6. Stop") {
      testProbe.send(player, StopSong)

      testProbe.expectMsgPF(3 seconds){
        case msg @ AudioPlayer.SongStopped(`moneyForNothing`) ⇒
      }

      manager.receiveWhile(1 second) {
        case AudioPlayer.SongProgressed(song, progress) ⇒
        case other ⇒ fail(s"Unexpected: $other")
      }
    }
  }
}
