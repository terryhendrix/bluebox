package nl.bluesoft.bluebox.audio

import nl.bluesoft.bluebox.audio.core.MPlayer
import nl.bluesoft.bluebox.suite.TestSuite

class MPlayerTest extends TestSuite
{
  feature("MPlayer should get the MP3 Meta") {
    scenario("1. Get info for heavy fuel") {
     val info =  MPlayer.clipInfo(heavyFuel)
      info.title shouldBe Some("Heavy Fuel")
      info.artist shouldBe Some("Dire Straits")
      info.album shouldBe Some("Sultans Of Swing (The Very Bes")
      info.year shouldBe Some(1998)
      info.genre shouldBe Some("Unknown")
      info.duration shouldBe Some(301.0)
      info.hertz shouldBe Some(44100)
      info.bitrate shouldBe Some(128000)
      info.seekable shouldBe Some(true)
      info.format shouldBe Some("85")
    }

    scenario("2. Get info for heavy fuel") {
      val info = MPlayer.clipInfo(moneyForNothing)
      info.title shouldBe Some("Money For Nothing")
      info.artist shouldBe Some("Dire Straits")
      info.album shouldBe Some("Sultans Of Swing (The Very Bes")
      info.year shouldBe Some(1998)
      info.genre shouldBe Some("Unknown")
      info.duration shouldBe Some(249.0)
      info.hertz shouldBe Some(44100)
      info.bitrate shouldBe Some(128000)
      info.seekable shouldBe Some(true)
      info.format shouldBe Some("85")
    }

    scenario("3. Cannot get info for rollin' & tumblin'") {
      val info = MPlayer.clipInfo(rollingAndTumblin)
      info.title shouldBe None
      info.artist shouldBe None
      info.album shouldBe None
      info.year shouldBe None
      info.genre shouldBe None
      info.duration shouldBe Some(180.0)
      info.hertz shouldBe Some(44100)
      info.bitrate shouldBe Some(478664)
      info.seekable shouldBe Some(true)
      info.format shouldBe Some("fLaC")
    }

    scenario("4. Cannot get info for mannish boy") {
      val info = MPlayer.clipInfo(mannishBoy)
      info.duration shouldBe Some(177.0)
      info.hertz shouldBe Some(44100)
      info.bitrate shouldBe Some(516816)
      info.seekable shouldBe Some(true)
      info.format shouldBe Some("fLaC")
    }

    scenario("5. Get info for down the road") {
      val info = MPlayer.clipInfo(downTheRoad)
      info.title shouldBe Some("Down the Road")
      info.artist shouldBe Some("C2C")
      info.album shouldBe Some("Down the Road")
      info.year shouldBe Some(2012)
      info.track shouldBe Some(1)
      info.genre shouldBe Some("Electronic")
      info.duration shouldBe Some(205.0)
      info.hertz shouldBe Some(48000)
      info.bitrate shouldBe Some(320000)
      info.seekable shouldBe Some(true)
      info.format shouldBe Some("85")
    }
  }
}
