package nl.bluesoft.bluebox.audio

import akka.testkit.TestProbe
import nl.bluesoft.bluebox.audio.core.AudioGroup.SkippedToSong
import nl.bluesoft.bluebox.audio.core.{EventRouter, SongStatsRegion}
import nl.bluesoft.bluebox.audio.model.{SongEventCounts, SongStatistics}
import nl.bluesoft.bluebox.suite.TestSuite

import scala.concurrent.duration._

class IntelligentRatingsTest extends TestSuite with SongStatsRegion
{
  val testProbe = TestProbe()
  val router = system.actorOf(EventRouter.props, "EventRouter")

  feature("The event router should route events published on the event stream") {
    scenario("1. Average") {
      system.eventStream.publish(SkippedToSong(song = heavyFuel, wasPlaying = moneyForNothing))
      lapseTime(100 millis)
      system.eventStream.publish(SkippedToSong(song = heavyFuel, wasPlaying = moneyForNothing))
      lapseTime(100 millis)
      system.eventStream.publish(SkippedToSong(song = heavyFuel, wasPlaying = moneyForNothing))
      lapseTime(100 millis)
      system.eventStream.publish(SkippedToSong(song = heavyFuel, wasPlaying = moneyForNothing))
      lapseTime(100 millis)
      system.eventStream.publish(SkippedToSong(song = heavyFuel, wasPlaying = moneyForNothing))
      lapseTime(100 millis)

      system.eventStream.publish(SkippedToSong(song = moneyForNothing, wasPlaying = heavyFuel))
      lapseTime(100 millis)
      system.eventStream.publish(SkippedToSong(song = moneyForNothing, wasPlaying = heavyFuel))
      lapseTime(100 millis)
      system.eventStream.publish(SkippedToSong(song = moneyForNothing, wasPlaying = heavyFuel))
      lapseTime(100 millis)

      eventually {
        log.info(s"${ratingIndex.getRatings.futureValue}")
        ratingIndex.getRatings.futureValue.size shouldBe 2
        ratingIndex.getRating(heavyFuel.songHash).futureValue.size shouldBe 1
        ratingIndex.getRating(heavyFuel.songHash).futureValue.head shouldBe SongStatistics(
          songHash = heavyFuel.songHash,
          systemRating = 4,
          counts = SongEventCounts(skipped = 3, skippedTo = 5)
        )

        ratingIndex.getRating(moneyForNothing.songHash).futureValue.size shouldBe 1
        ratingIndex.getRating(moneyForNothing.songHash).futureValue.head shouldBe SongStatistics(
          systemRating = 3,
          songHash = moneyForNothing.songHash,
          counts = SongEventCounts(skipped = 5, skippedTo = 3)
        )
      }
    }

    scenario("2. Rounding flipping point") {
      system.eventStream.publish(SkippedToSong(song = heavyFuel, wasPlaying = moneyForNothing))
      lapseTime(100 millis)

      eventually {
        log.info(s"${ratingIndex.getRatings.futureValue}")
        ratingIndex.getRatings.futureValue.size shouldBe 2
        ratingIndex.getRating(heavyFuel.songHash).futureValue.size shouldBe 1
        ratingIndex.getRating(heavyFuel.songHash).futureValue.head shouldBe SongStatistics(
          songHash = heavyFuel.songHash,
          systemRating = 4,
          counts = SongEventCounts(skipped = 3, skippedTo = 6)
        )

        ratingIndex.getRating(moneyForNothing.songHash).futureValue.size shouldBe 1
        ratingIndex.getRating(moneyForNothing.songHash).futureValue.head shouldBe SongStatistics(
          systemRating = 2,
          songHash = moneyForNothing.songHash,
          counts = SongEventCounts(skipped = 6, skippedTo = 3)
        )
      }
    }

    scenario("3. Min-max") {
      system.eventStream.publish(SkippedToSong(song = mannishBoy, wasPlaying = downTheRoad))
      lapseTime(100 millis)
      system.eventStream.publish(SkippedToSong(song = mannishBoy, wasPlaying = downTheRoad))
      lapseTime(100 millis)
      system.eventStream.publish(SkippedToSong(song = mannishBoy, wasPlaying = downTheRoad))
      lapseTime(100 millis)


      eventually {
        log.info(s"${ratingIndex.getRatings.futureValue}")
        ratingIndex.getRatings.futureValue.size shouldBe 4
        ratingIndex.getRating(mannishBoy.songHash).futureValue.size shouldBe 1
        ratingIndex.getRating(mannishBoy.songHash).futureValue.head shouldBe SongStatistics(
          songHash = mannishBoy.songHash,
          systemRating = 5,
          counts = SongEventCounts(skipped = 0, skippedTo = 3)
        )

        ratingIndex.getRating(downTheRoad.songHash).futureValue.size shouldBe 1
        ratingIndex.getRating(downTheRoad.songHash).futureValue.head shouldBe SongStatistics(
          systemRating = 1,
          songHash = downTheRoad.songHash,
          counts = SongEventCounts(skipped = 3, skippedTo = 0)
        )
      }
    }
  }
}
