package nl.bluesoft.bluebox.audio

import java.nio.file.{Files, Paths}

import akka.testkit.TestProbe
import nl.bluesoft.bluebox.audio.core.SongIndexer
import nl.bluesoft.bluebox.audio.core.SongIndexer.Stats
import nl.bluesoft.bluebox.suite.TestSuite

import scala.concurrent.duration._
import scala.util.Try

class SongIndexerTest extends TestSuite
{
  val testProbe = TestProbe()

  override def beforeEach() = {
    super.beforeEach()
    Try(Files.move(Paths.get("src/test/resources/sub/sub/01 Mannish Boy.flac"), Paths.get("src/test/resources/sub/01 Mannish Boy.flac")))
  }

  override def afterAll() = {
    super.afterAll()
    Try(Files.move(Paths.get("src/test/resources/sub/sub/01 Mannish Boy.flac"), Paths.get("src/test/resources/sub/01 Mannish Boy.flac")))
  }

  feature("A directory should be indexed") {
    Given("The manager already exists")
    val indexer = system.actorOf(SongIndexer.props(),"SongIndexer")

    scenario("1. Should create items in the index from a directory") {
      testProbe.send(indexer, SongIndexer.IndexMusicDirectory("src/test/resources/sub/sub"))
      testProbe.expectMsg(15 seconds, SongIndexer.MusicDirectoryIndexed("src/test/resources/sub/sub", Stats(total = 2, created = 2)))

      eventually {
        songIndex.getSongs().futureValue.size shouldBe 2
        songIndex.getSongs().futureValue.flatMap(_.clipInfo).count(_.title.isDefined) shouldBe 2

        withClue("The directories should have proper sizes") {
          songIndex.getFromDirectory("src/test/resources/sub/sub").futureValue.size shouldBe 2
          songIndex.getFromDirectory("src/test/resources/sub").futureValue.size shouldBe 0
          songIndex.getFromDirectory("src/test/resources").futureValue.size shouldBe 0
        }
      }
    }

    scenario("2. Should create and skip items in the index from a directory") {
      testProbe.send(indexer, SongIndexer.IndexMusicDirectory("src/test/resources/sub"))
      testProbe.expectMsg(15 seconds, SongIndexer.MusicDirectoryIndexed("src/test/resources/sub", Stats(total = 8, created = 6, skipped = 2)))

      eventually {
        songIndex.getSongs().futureValue.size shouldBe 8
        songIndex.getSongs().futureValue.flatMap(_.clipInfo).count(_.title.isDefined) shouldBe 3

        withClue("The directories should have proper sizes") {
          songIndex.getFromDirectory("src/test/resources/sub/sub").futureValue.size shouldBe 2
          songIndex.getFromDirectory("src/test/resources/sub").futureValue.size shouldBe 6
          songIndex.getFromDirectory("src/test/resources").futureValue.size shouldBe 0
        }
      }
    }

    scenario("3. Should update, create and skip items") {
      When("An item is moved, it should be updated in the index")
      Files.move(Paths.get("src/test/resources/sub/01 Mannish Boy.flac"), Paths.get("src/test/resources/sub/sub/01 Mannish Boy.flac"))

      testProbe.send(indexer, SongIndexer.IndexMusicDirectory("src/test/resources"))
      testProbe.expectMsg(15 seconds, SongIndexer.MusicDirectoryIndexed("src/test/resources", Stats(total = 10, created = 1, skipped = 7, updated = 2)))

      eventually {
        val songs = songIndex.getSongs().futureValue
        songs.size shouldBe 10

        withClue("Heavy Fuel is in the directory twice, so we should be 2 clip-info's for this as well") {
          songs.count(_.fileName.contains("Heavy Fuel")) shouldBe 2
        }

        withClue("Mannish Boy is moved so it should occur once") {
          songs.count(_.fileName.contains("Mannish Boy")) shouldBe 1
        }

        withClue("We should count the available title's") {
          songs.flatMap(_.clipInfo).count(_.title.isDefined) shouldBe 5
        }

        withClue("The directories should have proper sizes") {
          songIndex.getFromDirectory("src/test/resources/sub/sub").futureValue.size shouldBe 3
          songIndex.getFromDirectory("src/test/resources/sub").futureValue.size shouldBe 5
          songIndex.getFromDirectory("src/test/resources").futureValue.size shouldBe 2
        }
      }
    }
  }
}
