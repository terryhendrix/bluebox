package nl.bluesoft.bluebox.audio

import akka.testkit.TestProbe
import nl.bluesoft.bluebox.audio.core.AudioGroup.SkippedToSong
import nl.bluesoft.bluebox.audio.core.AudioPlayer.{SongFinished, SongStarted}
import nl.bluesoft.bluebox.audio.core.SongStats.{RateSong, SkippedSong}
import nl.bluesoft.bluebox.audio.core.SongStatsRegion
import nl.bluesoft.bluebox.audio.model.{EndUser, SongEventCounts, SongStatistics}
import nl.bluesoft.bluebox.suite.TestSuite

class SongStatsTest extends TestSuite with SongStatsRegion
{
  val testProbe = TestProbe()

  feature("A rating should be added for songs and the ratings should be indexed") {
    scenario("1. Set some song ratings by end user, overwriting by username") {
      Range(1, 4).foreach { nr ⇒
        log.info(s"Rating $nr")
        testProbe.send(songStats, RateSong(EndUser, moneyForNothing.songHash, 5))
        eventually {
          log.info(s"${ratingIndex.getRatings.futureValue}")
          ratingIndex.getRating(moneyForNothing.songHash).futureValue.size shouldBe 1
          ratingIndex.getRating(moneyForNothing.songHash).futureValue.head shouldBe SongStatistics(songHash = moneyForNothing.songHash, userRatings = Map(EndUser.username → 5))
        }
      }
    }

    scenario("2. It should react to SongStarted") {
      Range(1, 5).foreach { nr ⇒
        log.info(s"Event $nr")
        testProbe.send(songStats, SongStarted(heavyFuel))
        eventually {
          log.info(s"${ratingIndex.getRatings.futureValue}")
          ratingIndex.getRating(heavyFuel.songHash).futureValue.size shouldBe 1
          ratingIndex.getRating(heavyFuel.songHash).futureValue.head shouldBe SongStatistics(songHash = heavyFuel.songHash, counts = SongEventCounts(started = nr))
        }
      }
    }

    scenario("3. It should react to SongFinished") {
      Range(1, 5).foreach { nr ⇒
        log.info(s"Event $nr")
        testProbe.send(songStats, SongFinished(mannishBoy))
        eventually {
          log.info(s"${ratingIndex.getRatings.futureValue}")
          ratingIndex.getRating(mannishBoy.songHash).futureValue.size shouldBe 1
          ratingIndex.getRating(mannishBoy.songHash).futureValue.head shouldBe SongStatistics(songHash = mannishBoy.songHash, counts = SongEventCounts(finished = nr))
        }
      }
    }

    scenario("4. It should react to SkippedToSong") {
      Range(1, 5).foreach { nr ⇒
        log.info(s"Event $nr")
        testProbe.send(songStats, SkippedToSong(song = rollingAndTumblin, wasPlaying = heavyFuel))
        eventually {
          log.info(s"${ratingIndex.getRatings.futureValue}")
          ratingIndex.getRating(rollingAndTumblin.songHash).futureValue.size shouldBe 1
          ratingIndex.getRating(rollingAndTumblin.songHash).futureValue.head shouldBe SongStatistics(rollingAndTumblin.songHash, counts = SongEventCounts(skippedTo = nr))
        }
      }
    }

    scenario("5. It should react to SkippedSong") {
      Range(1, 5).foreach { nr ⇒
        log.info(s"Event $nr")
        testProbe.send(songStats, SkippedSong(wasPlaying = downTheRoad))
        eventually {
          log.info(s"${ratingIndex.getRatings.futureValue}")
          ratingIndex.getRating(downTheRoad.songHash).futureValue.size shouldBe 1
          ratingIndex.getRating(downTheRoad.songHash).futureValue.head shouldBe SongStatistics(downTheRoad.songHash, counts = SongEventCounts(skipped = nr))
        }
      }
    }
  }
}
