package nl.bluesoft.bluebox.audio

import nl.bluesoft.bluebox.audio.model.Song
import nl.bluesoft.bluebox.suite.TestSuite

class SongTest extends TestSuite
{
  feature("A song must be able to determine its file system location") {
    scenario("1. path + file = filepath") {
      Song("/tmp", "test.mp3", "songHash", "pathHash").filePath shouldBe "/tmp/test.mp3"
    }
  }
}
