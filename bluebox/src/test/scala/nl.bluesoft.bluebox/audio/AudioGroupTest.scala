package nl.bluesoft.bluebox.audio

import akka.testkit.TestProbe
import nl.bluesoft.bluebox.audio.core.AudioGroup._
import nl.bluesoft.bluebox.audio.core.{AudioGroup, AudioPlayer}
import nl.bluesoft.bluebox.audio.model.HashPlaylist
import nl.bluesoft.bluebox.suite.{TestSongResolving, TestSuite}

import scala.concurrent.duration._

class AudioGroupTest extends TestSuite with TestSongResolving
{
  val volume = TestProbe()
  val manager = system.actorOf(AudioGroup.props(volume.ref, testResolver), "AudioManager")
  val player = system.actorOf(AudioPlayer.props(manager, "Living room"), "AudioPlayer")
  val testProbe = TestProbe()

  feature("An audio manager must load and unload playlists with only playlist loaded at one time") {
    val playlist = HashPlaylist("Test", hashes = Seq(heavyFuel.songHash,moneyForNothing.songHash))

    scenario("1. Load a playlist") {
      testProbe.send(manager, LoadPlaylist(playlist))
      testProbe.expectMsg(PlaylistLoaded(playlist))
    }

    scenario("2. Start it") {
      testProbe.send(manager, StartPlaylist)
      testProbe.expectMsg(PlaylistStarted(playlist))
      lapseTime(5 seconds)
    }

    scenario("3. Pause it") {
      testProbe.send(manager, PausePlaylist)
      testProbe.expectMsg(PlaylistPaused(playlist))
      lapseTime(2 seconds)
    }

    scenario("4. Resume it") {
      testProbe.send(manager, ResumePlaylist )
      testProbe.expectMsg(PlaylistResumed(playlist))
      lapseTime(5 seconds)
    }

    scenario("5. Pause it") {
      testProbe.send(manager, PausePlaylist)
      testProbe.expectMsg(PlaylistPaused(playlist))
      lapseTime(2 seconds)
    }

    scenario("6. Restart it") {
      testProbe.send(manager, StartPlaylist)
      testProbe.expectMsg(PlaylistStarted(playlist))
      lapseTime(5 seconds)
    }

    scenario("7. Stop it") {
      testProbe.send(manager, PausePlaylist)
      testProbe.expectMsg(PlaylistPaused(playlist))
    }

    val playlist2 = HashPlaylist("Test2", hashes = Seq(moneyForNothing.songHash))

    scenario("8. Load another playlist") {
      testProbe.send(manager, LoadPlaylist(playlist2))
      testProbe.expectMsg(PlaylistLoaded(playlist2))
    }

    scenario("9. Start it") {
      testProbe.send(manager, StartPlaylist)
      testProbe.expectMsg(PlaylistStarted(playlist2))
      lapseTime(5 seconds)
    }

    scenario("10. Stop it") {
      testProbe.send(manager, PausePlaylist)
      testProbe.expectMsg(PlaylistPaused(playlist2))
    }
  }
}
