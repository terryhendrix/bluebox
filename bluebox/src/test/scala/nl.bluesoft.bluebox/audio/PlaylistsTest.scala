package nl.bluesoft.bluebox.audio

import akka.testkit.TestProbe
import nl.bluesoft.bluebox.audio.core.Playlists._
import nl.bluesoft.bluebox.audio.model.HashPlaylist
import nl.bluesoft.bluebox.suite.TestSuite

class PlaylistsTest extends TestSuite
{
  val testProbe = TestProbe()

  feature("A playlistmanager must be able to manage playlists") {
    val name1 = "Playlist1"
    val name2 = "Playlist2"
    val name3 = "Playlist3"

    scenario("1. Create A Playlist") {
      testProbe.send(playlists, CreatePlaylist(name1))
      eventually {
        playlistIndex.getPlaylist(name1).futureValue shouldBe Some(HashPlaylist(name1))
        playlistIndex.getPlaylists().futureValue.size shouldBe 1
      }
    }

    scenario("2. Add songs to playlist") {
      testProbe.send(playlists, AddSong(name1, mannishBoy))
      eventually {
        playlistIndex.getPlaylist(name1).futureValue shouldBe Some(HashPlaylist(name1, hashes = Seq(mannishBoy.songHash)))
        playlistIndex.getPlaylists().futureValue.size shouldBe 1
      }
    }
    scenario("3. Add another song after a song") {
      testProbe.send(playlists, AddSong(name1, rollingAndTumblin, Some(mannishBoy)))
      eventually {
        val expected = Seq(mannishBoy.songHash, rollingAndTumblin.songHash)
        playlistIndex.getPlaylist(name1).futureValue shouldBe Some(HashPlaylist(name1, hashes = expected))
        playlistIndex.getPlaylists().futureValue.size shouldBe 1
      }
    }

    scenario("4. Add another song to playlist") {
      testProbe.send(playlists, AddSong(name1, moneyForNothing))
      eventually {
        val expected = Seq(moneyForNothing.songHash, mannishBoy.songHash, rollingAndTumblin.songHash)
        playlistIndex.getPlaylist(name1).futureValue shouldBe Some(HashPlaylist(name1, hashes = expected))
        playlistIndex.getPlaylists().futureValue.size shouldBe 1
      }
    }

    scenario("5. Moving a song in the playlist") {
      testProbe.send(playlists, MoveSong(name1, moneyForNothing, Some(rollingAndTumblin)))
      eventually {
        val expected = Seq(mannishBoy.songHash, rollingAndTumblin.songHash, moneyForNothing.songHash)
        playlistIndex.getPlaylist(name1).futureValue shouldBe Some(HashPlaylist(name1, hashes = expected))
        playlistIndex.getPlaylists().futureValue.size shouldBe 1
      }
    }

    scenario("6. Moving a song to the beginning of the playlist") {
      testProbe.send(playlists, MoveSong(name1, rollingAndTumblin))
      eventually {
        val expected = Seq(rollingAndTumblin.songHash, mannishBoy.songHash, moneyForNothing.songHash)
        playlistIndex.getPlaylist(name1).futureValue shouldBe Some(HashPlaylist(name1, hashes = expected))
        playlistIndex.getPlaylists().futureValue.size shouldBe 1
      }
    }

    scenario("7. Create another playlist") {
      testProbe.send(playlists, CreatePlaylist(name2))
      eventually {
        playlistIndex.getPlaylist(name2).futureValue shouldBe Some(HashPlaylist(name2))
        playlistIndex.getPlaylists().futureValue.size shouldBe 2
      }
    }

    scenario("7. Move 1 to 3") {
      testProbe.send(playlists, RenamePlaylist(name1, name3))
      eventually {
        val expected = Seq(rollingAndTumblin.songHash, mannishBoy.songHash, moneyForNothing.songHash)
        playlistIndex.getPlaylist(name2).futureValue shouldBe Some(HashPlaylist(name2))
        playlistIndex.getPlaylist(name3).futureValue shouldBe Some(HashPlaylist(name3, hashes = expected))
        playlistIndex.getPlaylist(name1).futureValue shouldBe Some(HashPlaylist(name1, active = false, hashes = expected))
        playlistIndex.getPlaylists().futureValue.size shouldBe 2
      }
    }

    scenario("7. Move 2 to 1") {
      testProbe.send(playlists, RenamePlaylist(name2, name1))
      eventually {
        val expected = Seq(rollingAndTumblin.songHash, mannishBoy.songHash, moneyForNothing.songHash)
        playlistIndex.getPlaylist(name1).futureValue shouldBe Some(HashPlaylist(name1))
        playlistIndex.getPlaylist(name3).futureValue shouldBe Some(HashPlaylist(name3, hashes = expected))
        playlistIndex.getPlaylist(name2).futureValue shouldBe Some(HashPlaylist(name2, active = false))
        playlistIndex.getPlaylists().futureValue.size shouldBe 2
      }
    }

    scenario("8. Delete all playlists") {
      testProbe.send(playlists, DeletePlaylist(name1))
      testProbe.send(playlists, DeletePlaylist(name2))
      testProbe.send(playlists, DeletePlaylist(name3))

      eventually {
        val expected = Seq(rollingAndTumblin.songHash, mannishBoy.songHash, moneyForNothing.songHash)
        playlistIndex.getPlaylist(name3).futureValue shouldBe Some(HashPlaylist(name3, hashes = expected, active = false))
        playlistIndex.getPlaylist(name1).futureValue shouldBe Some(HashPlaylist(name1, active = false))
        playlistIndex.getPlaylist(name2).futureValue shouldBe Some(HashPlaylist(name2, active = false))
        playlistIndex.getPlaylists().futureValue.size shouldBe 0
      }
    }
  }
}

