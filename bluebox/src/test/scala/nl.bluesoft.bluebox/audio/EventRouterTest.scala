package nl.bluesoft.bluebox.audio

import akka.testkit.TestProbe
import nl.bluesoft.bluebox.audio.core.AudioGroup.SkippedToSong
import nl.bluesoft.bluebox.audio.core.AudioPlayer.{SongFinished, SongStarted}
import nl.bluesoft.bluebox.audio.core.{EventRouter, SongStatsRegion}
import nl.bluesoft.bluebox.audio.model.{SongEventCounts, SongStatistics}
import nl.bluesoft.bluebox.suite.TestSuite

class EventRouterTest extends TestSuite with SongStatsRegion
{
  val testProbe = TestProbe()
  val router = system.actorOf(EventRouter.props, "EventRouter")

  feature("The event router should route events published on the event stream") {
    scenario("1. Route SongStarted message to the SongStats shard region") {
      Range(1, 5).foreach { nr ⇒
        log.info(s"Event $nr")
        system.eventStream.publish(SongStarted(heavyFuel))
        eventually {
          log.info(s"${ratingIndex.getRatings.futureValue}")
          ratingIndex.getRating(heavyFuel.songHash).futureValue.size shouldBe 1
          ratingIndex.getRating(heavyFuel.songHash).futureValue.head shouldBe SongStatistics(heavyFuel.songHash, counts = SongEventCounts(started = nr))
        }
      }
    }

    scenario("2. Route SongFinished message to the SongStats shard region") {
      Range(1, 5).foreach { nr ⇒
        log.info(s"Event $nr")
        system.eventStream.publish(SongFinished(moneyForNothing))
        eventually {
          log.info(s"${ratingIndex.getRatings.futureValue}")
          ratingIndex.getRating(moneyForNothing.songHash).futureValue.size shouldBe 1
          ratingIndex.getRating(moneyForNothing.songHash).futureValue.head shouldBe SongStatistics(moneyForNothing.songHash, counts = SongEventCounts(finished = nr))
        }
      }
    }

    scenario("3. Route SongFinished message to the SongStats shard region") {
      Range(1, 5).foreach { nr ⇒
        log.info(s"Event $nr")
        system.eventStream.publish(SkippedToSong(song = mannishBoy, wasPlaying = rollingAndTumblin))
        eventually {
          log.info(s"${ratingIndex.getRatings.futureValue}")
          ratingIndex.getRating(mannishBoy.songHash).futureValue.size shouldBe 1
          ratingIndex.getRating(mannishBoy.songHash).futureValue.head shouldBe SongStatistics(mannishBoy.songHash, counts = SongEventCounts(skippedTo = nr))
          ratingIndex.getRating(rollingAndTumblin.songHash).futureValue.size shouldBe 1
          ratingIndex.getRating(rollingAndTumblin.songHash).futureValue.head shouldBe SongStatistics(rollingAndTumblin.songHash, counts = SongEventCounts(skipped = nr))
        }
      }
    }
  }
}
