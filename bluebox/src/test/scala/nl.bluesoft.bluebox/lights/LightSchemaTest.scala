package nl.bluesoft.bluebox.lights

import akka.actor.ActorSystem
import akka.testkit.TestProbe
import nl.bluesoft.bluebox.lights.extensions.{LightGroupStateCommand, LightStateCommand}
import nl.bluesoft.bluebox.lights.hue.HueAccessPoint
import nl.bluesoft.bluebox.lights.model.LightState
import nl.bluesoft.peanuts.util.MathUtil
import org.scalatest._
import org.scalatest.concurrent.{Eventually, ScalaFutures}
import org.scalatest.time.{Second, Seconds, Span}

import scala.concurrent.duration._
import scala.language.postfixOps

class LightSchemaTest extends FeatureSpecLike with Matchers with GivenWhenThen with Eventually with ScalaFutures with BeforeAndAfterAll with BeforeAndAfterEach
{
  implicit val system = ActorSystem("TestSystem")
  implicit val log    = system.log
  implicit override val patienceConfig = PatienceConfig(scaled(Span(30, Seconds)), scaled(Span(1, Second)))

  val app = LightsApp(system)
  val eventStream = TestProbe()
  system.eventStream.subscribe(eventStream.ref, classOf[HueAccessPoint.Connected])
  system.eventStream.subscribe(eventStream.ref, classOf[LightsApp.LightStateUpdated])

  feature("The light app registers some Events and Commands") {
    scenario("1. LightStateCommand must set the state of a light") {
      When("The lights initialize")

      eventually {
        eventStream.expectMsgPF(1 seconds) {
          case HueAccessPoint.Connected(ap, bridge) ⇒ log.info("Connected. Continue test")
        }
      }

      eventually {
        app.lights.size should not be 0
      }

      val state = LightState(MathUtil.randomInt(0, 65000), MathUtil.randomInt(30, 254), MathUtil.randomInt(30, 254), on = true)
      val command = LightStateCommand(app.lights.head, state)
      command.exec()

      eventStream.expectMsg(LightsApp.LightStateUpdated(app.lights.head, state))
    }

    scenario("2. LightGroupStateCommand must set the state of the default group") {
      eventStream.receiveWhile(1 seconds) { case emptyTheStream ⇒  }

      eventually {
        app.groups.size should not be 0
        app.lights.size should not be 0
      }

      val state = LightState(MathUtil.randomInt(0, 65000), MathUtil.randomInt(30, 254), MathUtil.randomInt(30, 254), on = true)
      val command = LightGroupStateCommand(app.groups.head, state)
      command.exec()

      val events = eventStream.receiveWhile(3 seconds){ case e:LightsApp.LightStateUpdated ⇒ e }
      assert(events.size > 3) // The hue bridge doesn't always update
      events.foreach { event ⇒
        app.lights should contain(event.light)
      }

      scenario("3. A TimeEvent must be linked to a LightStateCommand") {
        eventStream.receiveWhile(1 seconds) { case emptyTheStream ⇒ }

        eventually {
          app.groups.size should not be 0
          app.lights.size should not be 0
        }

        // TODO: IMPLEMENT
      }
    }
  }

  override protected def afterAll() = {
    eventStream.receiveWhile(1 seconds) { case a ⇒ log.warning("EventStream received but the test has ended " + a.toString) }
    system.shutdown()
    system.awaitTermination(15 seconds)
  }

  override def beforeEach() = {
    Thread sleep 3000 // required somehow for test to succeed when "testing all"
  }
}
