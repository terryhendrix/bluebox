package nl.bluesoft.bluebox.lights
import akka.actor.ActorSystem
import akka.testkit.TestProbe
import nl.bluesoft.bluebox.lights.hue.{HueAccessPoint, HueManager}
import org.scalatest.{BeforeAndAfterAll, Matchers, GivenWhenThen, FeatureSpecLike}

import scala.concurrent.duration._

class HueConnectionTest extends FeatureSpecLike with GivenWhenThen with Matchers with BeforeAndAfterAll
{
  implicit val system = ActorSystem("TestSystem")
  val log = system.log
  val registrar = system.actorOf(LightRegistry.props, "LightRegistrar")
  val eventStream = TestProbe()
  
  system.eventStream.subscribe(eventStream.ref, classOf[LightRegistry.LightInitialized])
  system.eventStream.subscribe(eventStream.ref, classOf[HueManager.AccessPointsFound])
  system.eventStream.subscribe(eventStream.ref, classOf[HueAccessPoint.Connected])
  system.eventStream.subscribe(eventStream.ref, classOf[HueAccessPoint.Disconnected])

  override protected def afterAll() = {
    system.shutdown()
    system.awaitTermination(15 seconds)
  }

  feature("Philips Hue") {
    info("This test requires exactly one Philips Hue Access point to be in the local network")
    scenario("1. Connect and discover") {
      When("The LightRegistry initializes")
      val events = eventStream.receiveWhile(3 seconds) {
        case a ⇒
          log.info("EventStream received " + a.toString)
          a
      }

      Then("We should get a connection to a HueAccessPoint")
      events.exists {
        case _: HueAccessPoint.Connected ⇒ true
        case _ ⇒ false
      } shouldBe true
    }
  }
}
