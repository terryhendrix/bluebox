package nl.bluesoft.bluebox.stories
import nl.bluesoft.bluebox.audio.model.SongProgress
import nl.bluesoft.bluebox.suite.RouteTest

class StoryBB001Test extends RouteTest
{

  feature("Story: BB-001 The user loads the application") {
    scenario("1. The Bluebox should identify itself") {
      val url = "/api/v1/box/ping"
      Get(url) ~> rest.route ~> check {
        handled shouldBe true
        val res = responseAs[String]
        system.log.info(s"$url returned $res")
      }
    }

    scenario("2. Progress is retrieved") {
      val url = "/api/v1/audio/progress"
      Get(url) ~> rest.route ~> check {
        handled shouldBe true
        val res = responseAs[String]
        system.log.info(s"$url returned $res")
        res.asObject[SongProgress] shouldBe SongProgress(0,0)
      }
    }
  }
}
