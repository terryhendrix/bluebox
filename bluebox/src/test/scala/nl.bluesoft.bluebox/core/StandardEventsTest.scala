package nl.bluesoft.bluebox.core

import java.time.LocalDateTime
import akka.actor.ActorSystem
import nl.bluesoft.bluebox.core.schedules.{TimeEvent, SunriseEvent, SunsetEvent}
import org.scalatest._
import org.scalatest.concurrent.{ScalaFutures, Eventually}

class StandardEventsTest extends FeatureSpecLike with Matchers with GivenWhenThen with Eventually with ScalaFutures
{
 implicit val system = ActorSystem("TestSystem")
 implicit val log = system.log

 feature("Standard events should fire") {
   scenario("1. SunriseEvent") {
     val event = SunriseEvent(52, 4)

     if(event.now isAfter event.sunrise) {
       event.hasTriggered shouldBe true
       event.hasTriggered shouldBe false
     }
     else {
       event.hasTriggered shouldBe false
       event.hasTriggered shouldBe false
     }
   }

   scenario("2. SunsetEvent") {
     val event = SunsetEvent(52, 4)
     if(event.now isAfter event.sunset) {
       event.hasTriggered shouldBe true
       event.hasTriggered shouldBe false
     }
     else {
       event.hasTriggered shouldBe false
       event.hasTriggered shouldBe false
     }
   }

   scenario("3. TimeEvent for current time") {
     val minute = LocalDateTime.now().getMinute
     val hour = LocalDateTime.now().getHour
     val event = TimeEvent(minute = minute, hour = hour, TimeEvent.EveryDayOfTheWeek, TimeEvent.EveryDayOfTheMonth)
     event.hasTriggered shouldBe true
     event.hasTriggered shouldBe false
   }

   scenario("4. TimeEvent for current time") {
     val minute = LocalDateTime.now().getMinute match {
       case 0 ⇒ 59
       case min ⇒ min - 1
     }

     val hour = LocalDateTime.now().getMinute match {
       case 0 ⇒ LocalDateTime.now().getHour match {
         case 0 ⇒ 23
         case _ ⇒ LocalDateTime.now().getHour - 1
       }
       case min ⇒ LocalDateTime.now().getHour
     }

     val event = TimeEvent(minute = minute, hour = hour, TimeEvent.EveryDayOfTheWeek, TimeEvent.EveryDayOfTheMonth)
     event.hasTriggered shouldBe false
     event.hasTriggered shouldBe false
   }
 }
}
