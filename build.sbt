import sbt._
import Keys._

organization := "nl.bluesoft"

version := "1.0.0-SNAPSHOT"

name := "bluebox-project"

scalaVersion in ThisBuild := "2.11.7"

lazy val root = Project(id = "bluebox-project", base = file("."))
  .aggregate(blueboxServer, upgradeServer)

lazy val blueboxServer = Project(id = "bluebox-server", base = file("bluebox"))

lazy val upgradeServer = Project(id = "version-server", base = file("version-server"))

