# BlueBox

An overly complicated music player. ;)

## 1. Installation

### 1.1 Installation for mac


1. Get brew
2. Install mplayer using brew (get brew if you dont have it yet)
3. Install pidof using brew
4. Put BLUEBOX_HOME in your bash profile and point it to the directory containing the build.sbt

`brew install mplayer`

`brew install pidof`

`nano ~/.profile` or `nano ~/.bash_profile`

Add the line:

`BLUEBOX_HOME=<path_to_project>`

### 1.2 Installation on Windows

Todo

## 2. Getting the latest version

### 2.1. Making sure you are on the right branch

Go to the project directory and make sure you are on the correct branch. This is most likely development. Execute: 

`git status`

The output will say something about the branch you are on: 

`On branch development`

To change your branch, execute:

`git checkout <name>` 

Replace &lt;name&gt; with your branch-name.

Then you may retrieve the newest version from the server:

### 2.2. Pulling the branch

`git pull` 

If that doesn't work, try:

`git pull origin <name>` Again, replace &lt;name&gt; with the actual branch-name.


## 3. Running BlueBox server

On the terminal, go to the project directory. 
Then go to the `scripts` directory, wich is within the project directory.

Type `./start-bluebox-server.sh`

The program should be running on port 25080. 
It should be available [here](http://localhost:25080/).

The first time the bluebox runs it should show a [welcome message](http://localhost:25080/#/welcome).
Elastic Search is [running on port](http://localhost:25200/songs/view/_search?pretty) 250200 (HTTP) and 250300 (Native)

## 4. Preparing a device

First run `write-sd.sh <image> <disk>` in the install/prepare directory

Using `diskutil list` you may check your disks. Warning: Don't enter the wrong disk, as it will lead to data loss.

Then the ansible script may be run on the device.

This raspbian image is used [Raspbian image](https://drive.google.com/file/d/0BzoTh3Vdt47fQzd0V1lFYUQtS0k/view), its the Raspberry HDMI version from [banana-pi.org](http://www.banana-pi.org/download.html)

* [NGinx websocket support](https://www.nginx.com/blog/nginx-nodejs-websockets-socketio/)
* [Raspbian image](https://drive.google.com/file/d/0BzoTh3Vdt47fQzd0V1lFYUQtS0k/view)
* [Sunrise/sunset calendar](http://stackoverflow.com/questions/15241392/get-sunrise-and-sunset-time)
* [Philips Hue Java SDK](http://www.developers.meethue.com/documentation/java-sdk-getting-started)