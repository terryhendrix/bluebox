#!/bin/sh
exec scala -deprecation "$0" "$@"
!#
import scala.sys.process._
val dir = args(0)
val v = Seq("cat", s"$dir/build.sbt").!!.mkString.split("\n").filter(_.matches("^\\s*version.*")).mkString
println(v.replaceAll("version", "").replace(":=", "").replaceAll(" ", "").replaceAll("\"",""))