#!/bin/bash

OLD_DIR=$(pwd)
cd $BLUEBOX_HOME
cd install/ansible

# Setup ansible modules
echo "Setting up ansible modules..."
brew install http://git.io/sshpass.rb
echo "Ansible modules installed."

if [ ! -f ~/.ssh/id_bluebox ]; then
    echo
    echo "Installing private key..."
    echo

    cp roles/bluebox/templates/id_bluebox ~/.ssh/id_bluebox

    cat ~/.ssh/id_bluebox

    echo
    echo "Installed private key to ~/.ssh/id_bluebox"
    echo
fi;

ssh -q deploy@bluebox.local exit

if [ $? -ne 0  ]; then
    echo "Installing deployment configuration..."   && \
    ./run-playbook-initial.sh                       && \
    echo "Installed deployment configuration."      && \
    echo
fi;

echo "Installing bluebox..."                    && \
./run-playbook-bluebox.sh                       && \
echo "Installed bluebox."                       && \
echo                                            && \
echo "Installation complete."
