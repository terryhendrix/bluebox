#!/bin/bash


if [ -z ${BLUEBOX_HOME} ]; then
    echo "ERROR: The BLUEBOX_HOME environment variable is not set."
    exit -1
fi;

OLD_DIR=$(pwd)
cd ${BLUEBOX_HOME}
cd scripts

RELEASE_DIR="${BLUEBOX_HOME}/.release"
RELEASE_VERSION=$(./get-version.scala ${BLUEBOX_HOME}/bluebox)

# Create the release directory
echo "Releasing version $RELEASE_VERSION"
rm -rf $RELEASE_DIR;
mkdir ${RELEASE_DIR}
echo "${RELEASE_VERSION}" > "${RELEASE_DIR}/.version"


# Release bluebox server
BLUEBOX_DIR="${RELEASE_DIR}/bluebox-server"
echo "Releasing bluebox-server"
cd ../bluebox && sbt dist:clean dist
cp -r target/dist ${BLUEBOX_DIR}
echo "Building bluebox-server complete"

echo "Zipping the release"
cd $BLUEBOX_HOME
zip -r release.zip .release/*
mkdir -p install/roles/release/files
mv -v release.zip install/roles/release/files/

cd install
./install-bluebox.sh release

echo "Building android app"
cd ../app
phonegap --verbose remote build android
echo "Building android app complete"

rm -rf $RELEASE_DIR;
echo "Release complete"
cd ${OLD_DIR}

