#!/bin/bash

sudo echo "Running with sudo"

if [[ -z $1 ]]; then
    echo "No device name given, enter disk such as disk2. Please make sure that this is the correct disk! Otherwise data may be lost!"
    echo "You have been warned."
    exit -1
else
    if [[ -z $2 ]]; then
        echo "No image name given, enter an .img as first argument."
        exit -1
    else
	    diskutil unmountDisk /dev/${1}                      && \
        echo "Writing /dev/${1} to ${2}"                && \
        sudo pv -tpreb /dev/r${1} | sudo dd of=${2} bs=4m   && \
        echo "/dev/${1} is written to ${2}"                 && \
	    diskutil list                                       && \
        sudo diskutil eject /dev/${1}                       && \
        echo "Disk /dev/${1} is ejected, you may remove the hardware"
    fi;
fi;
